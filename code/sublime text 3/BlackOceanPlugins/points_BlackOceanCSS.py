import sublime
import sublime_plugin

def isStyle  (cad,pos    ):
	return(
		cad[pos  ]=='-' and
		cad[pos+1]=='s' and
		cad[pos+2]=='t' and
		cad[pos+3]=='y' and
		cad[pos+4]=='l' and
		cad[pos+5]=='e'
	)
def isSpecial(cad,pos    ):
	return (
		cad[pos]==' '  or
		cad[pos]=='('  or
		cad[pos]==')'  or
		cad[pos]=='['  or
		cad[pos]==']'  or
		cad[pos]=='{'  or
		cad[pos]=='}'  or
		cad[pos]=='<'  or
		cad[pos]=='>'  or
		cad[pos]=='='  or
		cad[pos]=='.'  or
		cad[pos]==':'  or
		cad[pos]==';'  or
		cad[pos]==','  or
		cad[pos]=='-'  or
		cad[pos]=='+'  or
		cad[pos]=='*'  or
		cad[pos]=='/'  or
		cad[pos]=='?'  or
		cad[pos]=='#'  or
		cad[pos]=='&'  or
		cad[pos]=='!'  or
		cad[pos]=='|'  or
		cad[pos]=='"'  or
		cad[pos]=="'"  or
		cad[pos]=='\n' or
		cad[pos]=='\r' or
		cad[pos]=='\t'
	)
def isSpaces (cad,pos    ):
	return (
		cad[pos]==' '  or
		cad[pos]=='\n' or
		cad[pos]=='\r' or
		cad[pos]=='\t'
	 )
def isSpacest(cad,pos    ):
	return (
		cad[pos]==' '  or
		cad[pos]=='\t'
	 )
def isLetter (cad,pos    ):
	return (
		(cad[pos]>='a' and cad[pos]<='z') or
		(cad[pos]>='A' and cad[pos]<='Z')
	 )
def isNumber (cad,pos    ):
	return (
		cad[pos]>='0' and
		cad[pos]<='9'
	 )
def isBCTag  (cad,pos,let):
	if (
		cad[pos]=='.' or
		cad[pos]=='#' or
		cad[pos]=='>' or
		cad[pos]==':' or
		cad[pos]=='*' or
		cad[pos]=='@' or
		cad[pos]=='&' or
		cad[pos]=='$'
	): return True;

	if isLetter(cad,pos) or cad[pos]=='-':
		num=0

		while pos<let:
			if   cad[pos]=='\n' or cad[pos]==';' or cad[pos]==','             : break;
			elif cad[pos]=='['  or cad[pos]=='>'                              : return True;
			elif num==0 and (cad[pos]==':' or cad[pos]==' ' or cad[pos]=='\t'): num+=1;
			elif num==1 and not isSpaces(cad,pos)                             : num+=1;

			pos+=1

		return num==1 or num==0
	elif isNumber(cad,pos):
		while pos<let and (isNumber(cad,pos) or cad[pos]=='.'): pos+= 1;
		while pos<let and isSpacest(cad,pos): pos+=1;

		return cad[pos]=='%' or cad[pos]=='\n' or cad[pos]==';'

	return False;
def isBCProp (cad,pos,let):
	if isLetter(cad,pos) or cad[pos]=='-' or cad[pos]=='/':
		num=0;

		while pos<let:
			if   cad[pos]=='\n' or cad[pos]==';'                              : break
			elif cad[pos]=='/' and cad[pos+1]=='*'                            : return True
			elif num==0 and (cad[pos]==':' or cad[pos]==' ' or cad[pos]=='\t'): num+=1
			elif num==1 and not isSpaces(cad,pos)                             : num+=1

			pos+=1

		return num==2;

	return False
def isComment(cad,pos,let):
	return cad[pos]=='/' and pos+1<let and (cad[pos+1]=='/' or cad[pos+1]=='*')

def GetCountTabs(cad,pos,let):
	niv=0
	key=0

	while pos<let:
		if   cad[pos]=='{'                  : key+=1;
		elif cad[pos]=='}' and key>0        : key-=1;
		elif cad[pos]=='}'                  : break;
		elif cad[pos]=='\n'                 : niv =0;
		elif cad[pos]=='\t' or cad[pos]==' ': niv+=1;
		elif not isSpecial(cad,pos) or cad[pos]==':' or cad[pos]=='>' or cad[pos]=='&' or cad[pos]=='$': break;

		pos+=1

	return niv
def GetAtts     (cad,pos,let):
	psa=pos
	key=0

	while pos<let:
		if   cad[pos]=='{'          : key+=1;
		elif cad[pos]=='}' and key>0: key-=1;
		elif cad[pos]=='}'          : break;
		elif cad[pos]=='['          : break;
		elif cad[pos]==';'          : break;
		elif cad[pos]=='\n'         : break;
		pos+=1

	if cad[pos]=='[':
		while pos<let and cad[pos]!=']':pos+=1;
		return pos

	return psa
def GetYa       (cad,pos,let):
	key=0

	while pos<let:
		if   cad[pos]=='{'          : key+=1;
		elif cad[pos]=='}' and key>0: key-=1;
		elif cad[pos]=='}'          : break;
		elif cad[pos]=='['          : break;
		elif cad[pos]=='\n'         : break;
		pos+=1

	if cad[pos]=='[':
		while pos<let and cad[pos]!=']':pos+=1;

	return pos-1
def GetComment  (cad,pos,let):
	if cad[pos+1]=='/':
		while pos<let and cad[pos]!='\n' and cad[pos]!='}': pos+=1;
		return pos-1

	elif cad[pos+1]=='*':
		while pos<let and (cad[pos]!='*' or cad[pos+1]!='/'): pos+=1;
		return pos+1;

	return pos

def ReplacePoint(view,edit  ):
	pos=0; bni=0; tab=0; nun=0; key=0; let=view.size(); cad=view.substr(sublime.Region(0,let))

	while pos<let:
		if isStyle(cad,pos):
			while pos<let and cad[pos]!='{':pos+=1

			pos+=1;
			bni =GetCountTabs(cad,pos,let)

			while pos<let:
				if   cad[pos]=='}'                  : break
				# elif cad[pos]=='#'                  : 1
				elif cad[pos]=='\n'                 : tab =0
				elif cad[pos]=='\t' or cad[pos]==' ': tab+=1
				elif cad[pos]=='⋅'                  : pos=GetYa(cad,pos,let)
				elif not isSpaces(cad,pos)          :
					if isBCTag(cad,pos,let) and tab>bni:
						view.insert(edit, nun+pos-tab+2, '⋅')
						pos =GetAtts(cad,pos,let)
						nun+=1
					elif isBCProp(cad,pos,let) and tab>bni+1:
						view.insert(edit, nun+pos-tab+3, '⋅')
						nun+=1
					while pos<let:
						if   cad[pos]=='{'                  : key+=1;
						elif cad[pos]=='}' and key>0        : key-=1;
						elif cad[pos]=='\n' or cad[pos]=='}': break;
						elif isComment(cad,pos,let)         : pos=GetComment(cad,pos,let);
						pos+=1

					if   cad[pos]=='}' : break;
					elif cad[pos]=='\n': pos-=1;
				pos+=1
		pos+=1
def ClearPoint  (view,edit  ):
	pos=0; nun=0; key=0; let=view.size(); cad=view.substr(sublime.Region(0,let))

	while pos<let:
		if isStyle(cad,pos):
			while pos<let and cad[pos]!='{':pos+=1;

			pos+=1

			while pos<let:
				if   cad[pos]=='{'          : key+=1;
				elif cad[pos]=='}' and key>0: key-=1;
				elif cad[pos]=='}'          : break;
				elif isComment(cad,pos,let) : pos=GetComment(cad,pos,let);
				elif cad[pos]=='⋅'          :
					view.erase(edit, sublime.Region(pos-nun,pos-nun+1) )
					while pos<let and cad[pos]!='\n':pos+=1
					nun+=1
				pos+=1
		pos+=1

class pointsbocssCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		ReplacePoint(self.view, edit)
class clearsbocssCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		ClearPoint(self.view, edit)

