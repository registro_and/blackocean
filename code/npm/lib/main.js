/*Globales*/
global.VALUE_FILE = 'file',
	global.VALUE_FOLDER    = 'folder',
	global.VALUE_PATH      = 'path',
	global.VALUE_INSERT    = 'insert',
	global.VALUE_UPDATE    = 'update',
	global.VALUE_DELETE    = 'delete',
	global.VALUE_GETTER    = 'getter',
	global.VALUE_FUNCTION  = 'function',
	global.VALUE_OBJECT    = 'object',
	global.VALUE_ARRAY     = 'array',
	global.VALUE_STRING    = 'string',
	global.VALUE_NUMBER    = 'number',
	global.VALUE_FLOAT     = 'float',
	global.VALUE_BOOL      = 'bool',
	global.VALUE_INT       = 'int',
	global.VALUE_PASSWORD  = 'password',
	global.VALUE_SPECIAL   = 'special',
	global.VALUE_HTML      = 'html',
	global.VALUE_CSS       = 'css',
	global.VALUE_JS        = 'js',
	global.VALUE_TAXONOMY  = 'taxonomy',
	global.VALUE_JSON      = 'json',
	global.VALUE_TEXT      = 'text',
	global.VALUE_BIG       = 'big',
	global.VALUE_MED       = 'med',
	global.VALUE_MIN       = 'min',
	global.VALUE_SYNC      = 'sync',
	global.VALUE_ASYNC     = 'async',
	global.VALUE_NULL      = 'null',
	global.VALUE_WARN      = 'warn',
	global.VALUE_ERROR     = 'error',
	global.VALUE_VALID     = 'valid',
	global.VALUE_QUERY     = 'query',
	global.VALUE_ACCEPT    = 'accept',
	global.VALUE_NOTHING   = 'nothing',
	global.VALUE_API       = 'api',
	global.VALUE_COMPILER  = 'compiler',
	global.VALUE_RESOURCES = 'resources',
	global.VALUE_APP       = 'Application',
	global.VALUE_COM       = 'Component',
	global.VALUE_SPA       = 'SPA';

/*Funciones*/
tipode=( type )=>Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase(), /* TIPO DE OBJETO */
	Hash      =data=>{
		if ( !data || !data.charCodeAt ) return 0;

		let hash=5381, num=(data||'').length;

		while ( num ) hash=(hash*33) ^ data.charCodeAt(--num);

		return hash>>>0;
	 }
	Mime      =type=>{
		if ( tipode(type)==VALUE_STRING ) {
			switch (type.toLowerCase()) {
				case '.js'  : case 'js'  : return 'application/javascript; charset=UTF-8';
				case '.bh'  : case 'bh'  : return 'text/html; charset=UTF-8';
				case '.bj'  : case 'bj'  : return 'application/javascript; charset=UTF-8';
				case '.css' : case 'css' : return 'text/css; charset=UTF-8';
				case '.ico' : case 'ico' : return 'image/x-icon';
				case '.jpg' : case 'jpg' : return 'image/jpeg';
				case '.png' : case 'png' : return 'image/png';
				case '.svg' : case 'svg' : return 'image/svg+xml';
				case '.txt' : case 'txt' : return 'text/plain';
				case '.woff': case 'woff': return 'font/woff';
				case '.html': case 'html': return 'text/html; charset=UTF-8';
				case '.json': case 'json': return 'application/javascript; charset=UTF-8';
				case '.pdf' : case 'pdf' : return 'application/pdf';
			}
		}

		return 'text/txt; null';
	 }
	ExcelToInt=text=>{
		let res=0, pos=0, len=text.length;

		while ( pos<len )
			res = (text[pos]>='a'?text[pos++]-32:text[pos++]) + (res*26) - 64;

		return res;
	 }
	IntToExcel=( numb, lower=true )=>{
		let res='', cas=lower?97:65;

		while ( parseInt(numb)>0 ) {
			res  = String.fromCharCode( ((numb-1) % 26) + cas ) + res;
			numb = (numb-1) / 26;
		}

		return res;
 	 }

	isLetter     =( cad, pos )=>{
		return (
			(cad[pos]>='a' && cad[pos]<='z') ||
			(cad[pos]>='A' && cad[pos]<='Z')
		 );
	 }
	isNumber     =( cad, pos )=>{
		return (
			cad[pos]>='0' &&
			cad[pos]<='9'
		);
	 }
	isOperator   =( cad, pos )=>{
		return (
			cad[pos]=='<' ||
			cad[pos]=='>' ||
			cad[pos]=='=' ||
			cad[pos]=='-' ||
			cad[pos]=='+' ||
			cad[pos]=='*' ||
			cad[pos]=='/' ||
			cad[pos]=='%' ||
			cad[pos]=='!' ||
			cad[pos]=='?'
		 );
	 }
	isSpaces     =( cad, pos )=>{
		return (
			cad[pos]==' '  ||
			cad[pos]=='\n' ||
			cad[pos]=='\r' ||
			cad[pos]=='\t'
		 );
	 }
	isSpacesTabs =( cad, pos )=>{
		return (
			cad[pos]=='\t' ||
			cad[pos]==' '  ||
			cad[pos]=='⋅'
		 );
	 }
	isSpecial    =( cad, pos )=>{
		return (
			cad[pos]==' '  ||
			cad[pos]==' '  ||
			cad[pos]=='⋅'  ||
			cad[pos]=='('  ||
			cad[pos]==')'  ||
			cad[pos]=='['  ||
			cad[pos]==']'  ||
			cad[pos]=='{'  ||
			cad[pos]=='}'  ||
			cad[pos]=='<'  ||
			cad[pos]=='>'  ||
			cad[pos]=='='  ||
			cad[pos]=='.'  ||
			cad[pos]==':'  ||
			cad[pos]==';'  ||
			cad[pos]==','  ||
			cad[pos]=='-'  ||
			cad[pos]=='+'  ||
			cad[pos]=='*'  ||
			cad[pos]=='/'  ||
			cad[pos]=='?'  ||
			cad[pos]=='#'  ||
			cad[pos]=='&'  ||
			cad[pos]=='!'  ||
			cad[pos]=='|'  ||
			cad[pos]=='"'  ||
			cad[pos]=="'"  ||
			cad[pos]=='\n' ||
			cad[pos]=='\r' ||
			cad[pos]=='\t'
		);
	 }
	isReplace    =( cad, pos )=>{
		return(
			cad[pos]=='[' &&
			(
				(
					cad[pos+2]==']' &&
					(
						cad[pos+1]=='.' ||
						cad[pos+1]==',' ||
						cad[pos+1]==':' ||
						cad[pos+1]==';'
					)
				)
				||
				( cad[pos+1]=='#' && cad[pos+2]==':' && isStringJS(cad,pos+3) )
				||
				( cad[pos+1]=='_' && cad[pos+2]=='_' )
			)
		);
	 }
	isStringJS   =( cad, pos )=>{
		return (
			cad[pos]=="'" ||
			cad[pos]=='"' ||
			cad[pos]=='`'
		);
	 }
	isStringCSS  =( cad, pos )=>{
		return (
			cad[pos]=="'" ||
			cad[pos]=='"'
		);
	 }
	isStringHTML =( cad, pos )=>{
		return (
			cad[pos]=="'" ||
			cad[pos]=='"'
		);
	 }
	isCommentJS  =( cad, pos )=>{
		return (
			(cad[pos]=='/' && cad[pos+1]=='/') ||
			(cad[pos]=='/' && cad[pos+1]=='*')
		);
	 }
	isCommentCSS =( cad, pos )=>{
		return (
			cad[pos  ]=='/' &&
			cad[pos+1]=='*'
		);
	 }
	isCommentHTML=( cad, pos )=>{
		return (
			cad[pos  ]=='<' &&
			cad[pos+1]=='!' &&
			cad[pos+2]=='-' &&
			cad[pos+3]=='-'
		);
	 }
	isReturn     =( cad, pos )=>{
		return (
			cad[pos  ]=='r' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='t' &&
			cad[pos+3]=='u' &&
			cad[pos+4]=='r' &&
			cad[pos+5]=='n' &&
			isSpecial(cad,pos+6) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	isDefine     =( cad, pos )=>{
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='d' &&
			cad[pos+2]=='e' &&
			cad[pos+3]=='f' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='n' &&
			cad[pos+6]=='e' &&
			isSpacesTabs(cad,pos+7) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	isGDefine    =( cad, pos )=>{
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='g' &&
			cad[pos+2]=='d' &&
			cad[pos+3]=='e' &&
			cad[pos+4]=='f' &&
			cad[pos+5]=='i' &&
			cad[pos+6]=='n' &&
			cad[pos+7]=='e' &&
			isSpacesTabs(cad,pos+8) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	isRInclude   =( cad, pos )=>{
		return(
			cad[pos  ]=='/' &&
			cad[pos+1]=='*' &&
			cad[pos+2]=='I' &&
			cad[pos+3]=='N' &&
			cad[pos+4]=='C' &&
			cad[pos+5]=='L' &&
			cad[pos+6]=='U' &&
			cad[pos+7]=='D' &&
			cad[pos+8]=='E'
 		);
	 };
	isEndRInclude=( cad, pos )=>{
		return(
			cad[pos   ]=='/' &&
			cad[pos+1 ]=='*' &&
			cad[pos+2 ]=='E' &&
			cad[pos+3 ]=='N' &&
			cad[pos+4 ]=='D' &&
			cad[pos+5 ]==' ' &&
			cad[pos+6 ]=='I' &&
			cad[pos+7 ]=='N' &&
			cad[pos+8 ]=='C' &&
			cad[pos+9 ]=='L' &&
			cad[pos+10]=='U' &&
			cad[pos+11]=='D' &&
			cad[pos+12]=='E'
 		);
	 };

	GetSpacesJS  =( res, cad, pos, par                            )=>{
		if ( !par.zip ) return[res+cad[pos], pos];

		let len=cad.length, ler=res.length;

		if (
			pos+1<len &&
			--ler>=0 &&
			(isLetter(cad,pos+1) || isNumber(cad,pos+1) || cad[pos+1]=='_') &&
			(isLetter(res,ler  ) || isNumber(res,ler  ) || res[ler]=='"' || res[ler]=='_')
		) { res+= ' '; }

		return[res, pos];
	 }
	GetSpacesCSS =( res, cad, pos, par                            )=>{
		if ( !par.zip ) return[res+cad[pos], pos];

		let len=cad.length, ler=res.length;

		if (
			pos+1<len &&
			--ler>=0 &&
			(
				isLetter(cad,pos+1) ||
				isNumber(cad,pos+1) ||
				cad[pos+1]=='.' ||
				cad[pos+1]=='#' ||
				cad[pos+1]=='[' ||
				cad[pos+1]=='*' ||
				cad[pos+1]=='"' ||
				(isOperator(cad,pos+1) && cad[pos+1]!='>' && cad[pos+1]!='=')
			)
			&&
			(
				isLetter(res,ler) ||
				isNumber(res,ler) ||
				 res[ler]=='-' ||
				 res[ler]=='+' ||
				 res[ler]=='*' ||
				 res[ler]=='"' ||
				 res[ler]=='/'
			)
		){ res+=' '; }

		return[res,pos];
	 }
	GetSpacesHTML=( res, cad, pos, par                            )=>{
		if ( !par.zip ) return[res+cad[pos], pos];

		let len=cad.length, ler=res.length;

		if (
			pos+1<len &&
			--ler>=0 &&
			(
				isLetter(res,ler) ||
				isNumber(res,ler) ||
				res[ler]=='-'  ||
				res[ler]=='.'  ||
				res[ler]==','  ||
				res[ler]=='_'  ||
				res[ler]=='"'  ||
				res[ler]==':'  ||
				res[ler]==';'  ||
				res[ler]=='\''
			)
			&&
			(
				isLetter(cad,pos+1) ||
				isNumber(cad,pos+1) ||
				cad[pos+1]=='_' ||
				cad[pos+1]=='-' ||
				cad[pos+1]=='.' ||
				cad[pos+1]=='('
			)
		) { res+= " "; }

		return[res,pos];
	 }
	GetComment   =( res, cad, pos, par, inccom=true               )=>{
		let len=cad.length, ret='';

		switch ( cad[pos+1] ) {
			case '*':
				for ( pos+=2; pos<len && (cad[pos]!='*' || cad[pos+1]!='/'); ret+=cad[pos++] );
				if ( inccom ) ret = '/*'+ ret +'*/';
				pos++;
				break;
			case '/':
				for ( pos+=2; pos<len && cad[pos]!='\n'; ret+=cad[pos++] );
				if ( inccom ) ret = '//' + ret;
				pos--;
				break;
			case '!':
				for ( pos+=4; pos<len && !(cad[pos]=='-' && cad[pos+1]=='-' && cad[pos+2]=='>'); ret+=cad[pos++] );
				if ( inccom ) ret = '<!--' + ret + '-->';
				pos+= 2;
				break;
		}

		if ( par.zip ) ret='';

		return[res+ret,pos];
	 }
	GetReplace   =( res, cad, pos, par                            )=>{
		if ( cad[pos+4]==']' ) {
			let tem='', len=cad.length;

			for (pos++; pos<len && cad[pos]!=']'; tem+=cad[pos++]);

			res+=par.variables[tem]||'not found variable';
		}
		else if ( cad[pos+1]=='#' && cad[pos+2]==':' && isStringJS(cad,pos+3) && !par.no_compile_hash ) {
			let tem, len=cad.length;
			[tem,pos] = GetString('',cad,pos+3,par,false,false);
			tem       = sCompileALL(tem,Object.assign({ no_compile_hash:true },par))||{};
			res      += tem.hash||0;
			for(;pos<len && cad[pos]!=']'; pos++);
		}
		else if ( cad[pos+1]=='_' && cad[pos+2]=='_' ) {
			let tem='', len=cad.length;
			for(pos+=3; pos<len && cad[pos]!=']'; tem+=cad[pos++]);
			res+=(par[tem]||'');
			for(;pos<len && cad[pos]!=']'; pos++);
		}
		else res+=cad[pos];

		return[res,pos];
	 },
	GetString    =( res, cad, pos, par, rembjs=false, inccom=true )=>{
		let len=cad.length, com=cad[pos];

		inccom && (res+=com);

		for ( pos++; pos<len; pos++ ) {
			if      ( cad[pos]=='$' && cad[pos+1]=='{' )[res,pos]=sCompileJS(res,cad,pos,par);
			else if ( rembjs && cad[pos]=='{'          )[res,pos]=sCompileBJ(res,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\'                   ) res+=cad[pos], res+=cad[++pos];
			else if ( cad[pos]=='['                    )[res,pos]=GetReplace(res,cad,pos,par);
			else if ( cad[pos]==com                    ) break;
			else                                         res+=cad[pos];
		 }

		inccom && (res+=com);

		return[res,pos];
	 }
	GetAttrs     =( res, cad, pos, par, inccom=true               )=>{
		let len=cad.length;
		let spa=par.cleanSpaces===undefined || par.cleanSpaces; delete par.cleanSpaces;
		let rem=par.replacekeys===undefined || par.replacekeys; delete par.replacekeys;

		for ( ;pos<len && cad[pos]!='['; res+=cad[pos++] );

		if ( inccom ) res+=cad[pos];

		pos++;

		for ( let key=0; pos<len; pos++ ) {
			if      ( cad[pos]=='['               ) res+=cad[pos], key++;
			else if ( cad[pos]==']' && key--==0   ) break;
			else if ( cad[pos]=='{'               )[res,pos]=sCompileBJ  (res,cad,pos,Object.assign(par,{includeTemp:rem}));
			else if ( isCommentJS(cad,pos)        )[res,pos]=GetComment  (res,cad,pos,par);
			else if ( isSpaces   (cad,pos) && spa )[res,pos]=GetSpacesCSS(res,cad,pos,par);
			else if ( cad[pos]=='='               ){
				let ret='';
				res+=cad[pos++];
				for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );
				for ( ;pos<len; pos++ ) {
					if      ( isStringCSS(cad,pos)        ){[ret,pos]=GetString(ret,cad,pos,par,true,false); break}
					else if ( isSpaces   (cad,pos) && spa ){pos--; break}
					else if ( isCommentJS(cad,pos)        )[ret,pos]=GetComment(ret,cad,pos,par);
					else if ( cad[pos]=='{'               )[ret,pos]=sCompileBJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
					else if ( cad[pos]==']'               ){pos--; break}
					else                                    ret+=cad[pos];
				}

				res+=`"${ret}"`;
			}
			else res+=cad[pos];
		}

		if ( inccom ) res+=cad[pos];

		return[res,pos];
	 }
	GetRegExp    =( res, cad, pos                                 )=>{
		let len=cad.length, ler=res.length;

		for ( ler--; ler>=0 && isSpaces(res,ler); ler-- );

		if (
			isLetter(res,ler) ||
			isNumber(res,ler) ||
			 res[ler]=='.' ||
			 res[ler]=='_' ||
			 res[ler]==')'
		) { return[res+cad[pos],pos]; }

		/*Leer Exprecion*/
		for (res+=cad[pos++]; pos<len; pos++) {
			if      ( cad[pos]=='\\'                  ) res+=cad[pos], res+=cad[++pos];
			else if ( cad[pos]=='['                   ){for(;pos<len&&cad[pos]!='\n'&&cad[pos]!=']'; res+=cad[pos++]); res+=cad[pos];}
			else if ( cad[pos]=='\n' || cad[pos]=='/' ){res+=cad[pos]; break;}
			else                                        res+=cad[pos];
		}

		return[res,pos];
	 },

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , {enumerable:false, value:function( func, result ) {
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sForEach', {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++)==false )
			break;
	}
 }});
Object.defineProperty(Object.prototype, 'sSome'   , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});
String.prototype.cmd = function( all=true ) {
	return this
	.replace(/%r/g,  all?'\x1b[0m':'') // Reset

	.replace(/%cb/g, all?'\x1b[1m':'') // CBright
	.replace(/%cd/g, all?'\x1b[2m':'') // CDim
	.replace(/%cu/g, all?'\x1b[4m':'') // CUnderscore
	.replace(/%cl/g, all?'\x1b[5m':'') // CBlink
	.replace(/%cv/g, all?'\x1b[7m':'') // CReverse
	.replace(/%ch/g, all?'\x1b[8m':'') // CHidden

	.replace(/%fb/g, all?'\x1b[30m':'') // FgBlack
	.replace(/%fr/g, all?'\x1b[31m':'') // FgRed
	.replace(/%fg/g, all?'\x1b[32m':'') // FgGreen
	.replace(/%fy/g, all?'\x1b[33m':'') // FgYellow
	.replace(/%fu/g, all?'\x1b[34m':'') // FgBlue
	.replace(/%fm/g, all?'\x1b[35m':'') // FgMagenta
	.replace(/%fc/g, all?'\x1b[36m':'') // FgCyan
	.replace(/%fw/g, all?'\x1b[37m':'') // FgWhite

	.replace(/%bb/g, all?'\x1b[40m':'') // BgBlack
	.replace(/%br/g, all?'\x1b[41m':'') // BgRed
	.replace(/%bg/g, all?'\x1b[42m':'') // BgGreen
	.replace(/%by/g, all?'\x1b[43m':'') // BgYellow
	.replace(/%bu/g, all?'\x1b[44m':'') // BgBlue
	.replace(/%bm/g, all?'\x1b[45m':'') // BgMagenta
	.replace(/%bc/g, all?'\x1b[46m':'') // BgCyan
	.replace(/%bw/g, all?'\x1b[47m':'') // BgWhite
	;
 }
String.prototype.inf = function( title='' ) {
	let gui='';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%bb%fy ${title} %r
		%fy${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }
String.prototype.war = function( title='' ) {
	let gui='';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%fy%cv ${title} %r
		%fy${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }
String.prototype.err = function( title='' ) {
	let gui = '';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%br%fw ${title} %r
		%fr${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }

/*Clases*/
global.sCompileJS   = function( result, data, position, params ) {
	/*Funciones*/
	function Compall( cad, par={}           ) {
		let len=cad.length, pos=0, res='';

		for (;pos<len; pos++) {
			if      ( isStringJS (cad,pos) )[res,pos]=GetString  (res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )[res,pos]=GetComment (res,cad,pos,par);
			else if ( isSpaces   (cad,pos) )[res,pos]=GetSpacesJS(res,cad,pos,par);
			else if ( cad[pos]=='/'        )[res,pos]=GetRegExp  (res,cad,pos);
			else                             res+=cad[pos];
		}

		return res;
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			par.data=Compall(cad,par);

			run(par);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='';
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos]=GetRegExp (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else                                  ret+=cad[pos];
		}

		if ( inc ) ret=ini + ret + cad[pos];

		return[res+ret, pos];
	 }

	/*Inicio*/
	if      ( typeof result==VALUE_NUMBER && result<0                   ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileJSON = function( result, data, position, params ) {
	/*Funciones*/
	function isComaJSON( cad, pos ) {
		if ( cad[pos++]==',' ) {
			for ( let len=cad.length; pos<len; pos++ ) {
				if      (  isCommentJS(cad,pos) ) pos=GetComment('',cad,pos,{})[1];
				else if ( !isSpaces   (cad,pos) ) break;
			}

			return cad[pos]=='}' || cad[pos]==']';
		}

		return false;
	 }

	/*Funciones*/
	function Compall( cad, par={}           ) {
		let len=cad.length, pos=0, res='';

		for (;pos<len; pos++) {
			if      ( isStringJS (cad,pos) )[res,pos]=GetString  (res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )pos      =GetComment (res,cad,pos,par)[1];
			else if ( isSpaces   (cad,pos) )[res,pos]=GetSpacesJS(res,cad,pos,par);
			else if ( isComaJSON (cad,pos) );
			else                             res+=cad[pos];
		}

		return res;
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			par.data=Compall(cad,par);

			run(par);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='';
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos]=GetRegExp (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else                                  ret+=cad[pos];
		}

		if ( inc ) ret=ini + ret + cad[pos];

		return[res+ret, pos];
	 }

	/*Inicio*/
	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileCSS  = function( result, data, position, params ) {
	/*Is*/
	const isCalc=( cad, pos )=>{
		return(
			cad[pos  ]=='c' &&
			cad[pos+1]=='a' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='c' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }

	/*Funciones*/
	const GetCalc=( res, cad, pos, par )=>{
		let len=cad.length;

		for ( ;pos<len && isLetter(cad,pos); res+=cad[pos++] );
		for ( ;pos<len && (!isOperator(0) || cap(0)=='%'); pos++ ) {
			if      ( isOperator(cad,pos) && cad[pos]!='%' ) break;
			else if ( isSpaces  (cad,pos)                  );
			else                                             res+=cad[pos];
		}

		res+= ' ' + cad[pos++] + ' ';

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]==')'     ){res+=cad[pos]; break}
			else if ( isSpaces(cad,pos) );
			else                          res+=cad[pos];
		}

		return[res,pos];
	 }

	/*Funciones*/
	function Compall( cad, par={}           ) {
		let len=cad.length, pos=0, res='';

		for ( ;pos<len; pos++ ) {
			if      ( isStringCSS (cad,pos) )[res,pos]=GetString   (res,cad,pos,par);
			else if ( isCommentCSS(cad,pos) )[res,pos]=GetComment  (res,cad,pos,par);
			else if ( isSpaces    (cad,pos) )[res,pos]=GetSpacesCSS(res,cad,pos,par);
			else if ( isReplace   (cad,pos) )[res,pos]=GetReplace  (res,cad,pos,par);
			else if ( isCalc      (cad,pos) )[res,pos]=GetCalc     (res,cad,pos,par);
			else                              res+=cad[pos];
		}

		return res;
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			par.data=Compall(cad,par);

			run(par);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		return[Compall(cad,pos), cad.length];
	 }

	/*Inicio*/
	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileHTML = function( result, data, position, params ) {
	/*IS*/
	function isTagStyle    ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			(cad[pos+1]=='s' || cad[pos+1]=='S') &&
			(cad[pos+2]=='t' || cad[pos+2]=='T') &&
			(cad[pos+3]=='y' || cad[pos+3]=='Y') &&
			(cad[pos+4]=='l' || cad[pos+4]=='L') &&
			(cad[pos+5]=='e' || cad[pos+5]=='E')
		);
	 }
	function isFinTagStyle ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			 cad[pos+1]=='/' &&
			(cad[pos+2]=='s' || cad[pos+2]=='S') &&
			(cad[pos+3]=='t' || cad[pos+3]=='T') &&
			(cad[pos+4]=='y' || cad[pos+4]=='Y') &&
			(cad[pos+5]=='l' || cad[pos+5]=='L') &&
			(cad[pos+6]=='e' || cad[pos+6]=='E') &&
			 cad[pos+7]=='>'
		);
	 }
	function isTagScript   ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			(cad[pos+1]=='s' || cad[pos+1]=='S') &&
			(cad[pos+2]=='c' || cad[pos+2]=='C') &&
			(cad[pos+3]=='r' || cad[pos+3]=='R') &&
			(cad[pos+4]=='i' || cad[pos+4]=='I') &&
			(cad[pos+5]=='p' || cad[pos+5]=='P') &&
			(cad[pos+6]=='t' || cad[pos+6]=='T')
		);
	 }
	function isFinTagScript( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			 cad[pos+1]=='/' &&
			(cad[pos+2]=='s' || cad[pos+2]=='S') &&
			(cad[pos+3]=='c' || cad[pos+3]=='C') &&
			(cad[pos+4]=='r' || cad[pos+4]=='R') &&
			(cad[pos+5]=='i' || cad[pos+5]=='I') &&
			(cad[pos+6]=='p' || cad[pos+6]=='P') &&
			(cad[pos+7]=='t' || cad[pos+7]=='T') &&
			 cad[pos+8]=='>'
		);
	 }

	/*Funciones*/
	function GetTagStyle ( res, cad, pos, par ) {
		let ret='', fin='', len=cad.length, num=0;

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos]=GetString (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos]=GetComment(res,cad,pos,par);
			else if ( cad[pos]=='>'          ){res+=cad[pos++]; break;}
			else                               res+=cad[pos];
		}

		for (;pos<len; pos++) {
			if      ( isFinTagStyle(cad,pos) ){for(;pos<len&&cad[pos]!='>';fin+=cad[pos++]); fin+=cad[pos]; break;}
			else if ( isStringCSS  (cad,pos) )[ret,pos]=GetString  (ret,cad,pos,par);
			else if ( isCommentCSS (cad,pos) )[ret,pos]=GetComment (ret,cad,pos,par);
			else                               ret+= cad[pos];
		}

		return[res+sCompileCSS(-1,ret,par)+fin,pos];
	 }
	function GetTagScript( res, cad, pos, par ) {
		let ret='', fin='', len=cad.length, num=0;

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos]=GetString (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos]=GetComment(res,cad,pos,par);
			else if ( cad[pos]=='>'          ){res+=cad[pos++]; break;}
			else                               res+=cad[pos];
		}

		for ( ;pos<len; pos++ ) {
			if      ( isFinTagScript(cad,pos) ){for(;pos<len&&cad[pos]!='>';fin+=cad[pos++]); fin+=cad[pos]; break;}
			else if ( isStringJS    (cad,pos) )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( isCommentJS   (cad,pos) )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( cad[pos]=='/'           )[ret,pos]=GetRegExp (ret,cad,pos);
			else                                ret+=cad[pos];
		}

		return[res+sCompileJS(-1,ret,par)+fin,pos];
	 }

	/*Funciones*/
	function Compall( cad, par={}           ) {
		let len=cad.length, pos=0, res='';

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos]=GetString    (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos]=GetComment   (res,cad,pos,par);
			else if ( isSpaces     (cad,pos) )[res,pos]=GetSpacesHTML(res,cad,pos,par);
			else if ( isTagStyle   (cad,pos) )[res,pos]=GetTagStyle  (res,cad,pos,par);
			else if ( isTagScript  (cad,pos) )[res,pos]=GetTagScript (res,cad,pos,par);
			else if ( isReplace    (cad,pos) )[res,pos]=GetReplace   (res,cad,pos,par);
			else                               res+=cad[pos];
		}

		return res;
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			par.data=Compall(cad,par);

			run(par);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		return[Compall(cad,pos), cad.length];
	 }

	/*Inicio*/
	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileMD   = function( result, data, position, params ) {
	/*Constantes*/
	const TYPE_TITLE=1;
	const TYPE_QUOTE=2;
	const TYPE_LIST =3;
	const TYPE_ORDER=4;
	const TYPE_PCODE=5;
	const TYPE_LINE =6;
	const TYPE_TABLE=7;
	const TYPE_OTHER=8;

	/*Is*/
	function isTitle( cad, pos ) {
		let len=cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='#';
	 };
	function isQuote( cad, pos ) {
		let len=cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='>';
	 };
	function isList ( cad, pos ) {
		let len=cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (cad[pos]=='-' || cad[pos]=='*' || cad[pos]=='+') && isSpacesTabs(cad,pos+1);
	 };
	function isOrder( cad, pos ) {
		let len=cad.length, num=0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);
		for (;pos<len && isNumber(cad,pos); pos++, num++);

		return num && cad[pos]=='.' && isSpacesTabs(cad,pos+1);
	 };
	function isPCode( cad, pos ) {
		let len=cad.length, num=0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (
			(cad[pos]=='`' && cad[pos+1]=='`' && cad[pos+2]=='`') ||
			(cad[pos]=='~' && cad[pos+1]=='~' && cad[pos+2]=='~')
		);
	 };
	function isLine ( cad, pos ) {
		let len=cad.length, num=0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (
			(cad[pos]=='-' && cad[pos+1]=='-' && cad[pos+2]=='-' && (cad[pos+3]=='\n' || pos+3>=len)) ||
			(cad[pos]=='_' && cad[pos+1]=='_' && cad[pos+2]=='_' && (cad[pos+3]=='\n' || pos+3>=len)) ||
			(cad[pos]=='*' && cad[pos+1]=='*' && cad[pos+2]=='*' && (cad[pos+3]=='\n' || pos+3>=len))
		);
	 };
	function isTable( cad, pos ) {
		let len=cad.length, num=0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='|';
	 };
	function isLink ( cad, pos ) {
		if ( cad[pos]!='[' ) return false;

		let len=cad.length, num=0;

		for (pos++; pos<len; pos++) {
			if      ( cad[pos]=='\\'            ) pos++;
			else if ( cad[pos]=='\n'            ) break;
			else if ( cad[pos]=='('             ) num++;
			else if ( cad[pos]=='['             ) num++;
			else if ( cad[pos]==')' && num--==0 ) break;
			else if ( cad[pos]==']' && num--==0 ) break;
		}

		if ( cad[pos]!=']' ) return false;

		return cad[++pos]=='(';
	 };
	function isALink( cad, pos ) {
		if ( cad[pos]=='w' && cad[pos+1]=='w' && cad[pos+2]=='w' && cad[pos+3]=='.' ) {
			let len=cad.length, num=0;

			for (pos+=4; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}
		else if ( cad[pos]=='h' && cad[pos+1]=='t' && cad[pos+2]=='t' && cad[pos+3]=='p' && cad[pos+4]==':' && cad[pos+5]=='/' && cad[pos+6]=='/' ) {
			let len=cad.length, num=0;

			for (pos+=7; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}
		else if ( cad[pos]=='h' && cad[pos+1]=='t' && cad[pos+2]=='t' && cad[pos+3]=='p' && cad[pos+4]=='s' && cad[pos+5]==':' && cad[pos+6]=='/' && cad[pos+7]=='/' ) {
			let len=cad.length, num=0;

			for (pos+=8; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}

		return false;
	 };
	function isAMeil( cad, pos ) {
		if (
			cad[pos]=='_' || cad[pos]=='-' || cad[pos]=='<' ||
			(cad[pos]>='a' && cad[pos]<='z') ||
			(cad[pos]>='A' && cad[pos]<='Z')
		) {
			let arr=0, poi=0;

			for (let len=cad.length; pos<len; pos++) {
				if      ( isSpaces(cad,pos) ) break;
				else if ( cad[pos]=='@'     ) arr++;
				else if ( cad[pos]=='.'     ) poi++;
			}

			return arr==1 && poi>=1;
		}

		return false;
	 };
	function isImage( cad, pos ) {
		if ( cad[pos]!='!' || cad[pos+1]!='[' ) return false;

		let len=cad.length, num=0;

		for (pos+=2; pos<len; pos++) {
			if      ( cad[pos]=='\\'            ) pos++;
			else if ( cad[pos]=='\n'            ) break;
			else if ( cad[pos]=='('             ) num++;
			else if ( cad[pos]=='['             ) num++;
			else if ( cad[pos]==')' && num--==0 ) break;
			else if ( cad[pos]==']' && num--==0 ) break;
		}

		if ( cad[pos]!=']' ) return false;

		return cad[++pos]=='(';
	 };

	/*Funciones*/
	function GetChildsTitle( ite, cad, pos, par ) {
		let len=cad.length, num=0, tem='';

		for (;pos<len && isSpaces(cad,pos); pos++);
		for (;pos<len && cad[pos]=='#'; pos++, num++);
		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_TITLE, number:num, cont:tem });

		return pos;
	 };
	function GetChildsQuote( ite, cad, pos, par ) {
		let len=cad.length, tem='';

		for (;pos<len && isSpaces(cad,pos); pos++);
		pos++;
		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_QUOTE, cont:tem });

		return pos;
	 };
	function GetChildsList ( ite, cad, pos, par ) {
		let len=cad.length, tem='', com='', lvl=0;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\t' ) lvl+=4;
			else if ( cad[pos]==' '  ) lvl++;
			else                       break;
		}

		lvl=parseInt(lvl/4);
		com=cad[pos++];
		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_LIST, level:lvl, list:com, cont:tem });

		return pos;
	 };
	function GetChildsOrder( ite, cad, pos, par ) {
		let len=cad.length, tem='', lvl=0;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\t' ) lvl+=4;
			else if ( cad[pos]==' '  ) lvl++;
			else                       break;
		}

		lvl=parseInt(lvl/4);
		for (;pos<len && isNumber(cad,pos); pos++);
		pos++;
		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_ORDER, level:lvl, cont:tem });

		return pos;
	 };
	function GetChildsPCode( ite, cad, pos, par ) {
		let len=cad.length, tem='', tip='', com='';

		for(;pos<len && isSpacesTabs(cad,pos); pos++);
		com=cad[pos];
		for (pos+=3; pos<len && cad[pos]!='\n'; tip+=cad[pos++]);
		for (pos++ ; pos<len; pos++) {
			if ( cad[pos]=='\n' && cad[pos+1]==com && cad[pos+2]==com && cad[pos+3]==com && cad[pos+4]=='\n' ){pos+=4; break}
			else                                                                                               tem+=cad[pos];
		}

		ite.push({ type:TYPE_PCODE, code:tip, cont:tem });

		return pos;
	 };
	function GetChildsLine ( ite, cad, pos, par ) {
		let len=cad.length;

		for(;pos<len && isSpacesTabs(cad,pos); pos++);

		pos+=3;

		ite.push({ type:TYPE_LINE });

		return pos;
	 };
	function GetChildsTable( ite, cad, pos, par ) {
		let len=cad.length, tem='';

		for (;pos<len && isSpacesTabs(cad,pos); pos++);
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_TABLE, cont:tem });

		return pos;
	 };
	function GetChildsOther( ite, cad, pos, par ) {
		let len=cad.length, tem='';

		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_OTHER, cont:tem });

		return pos;
	 };
	function GetChilds     ( cad, par           ) {
		let len=cad.length, pos=0, ite=[];

		for ( ;pos<len; pos++ ) {
			if      ( isTitle(cad,pos) ) pos=GetChildsTitle(ite,cad,pos,par);
			else if ( isQuote(cad,pos) ) pos=GetChildsQuote(ite,cad,pos,par);
			else if ( isList (cad,pos) ) pos=GetChildsList (ite,cad,pos,par);
			else if ( isOrder(cad,pos) ) pos=GetChildsOrder(ite,cad,pos,par);
			else if ( isPCode(cad,pos) ) pos=GetChildsPCode(ite,cad,pos,par);
			else if ( isLine (cad,pos) ) pos=GetChildsLine (ite,cad,pos,par);
			else if ( isTable(cad,pos) ) pos=GetChildsTable(ite,cad,pos,par);
			else                         pos=GetChildsOther(ite,cad,pos,par);
		}

		return ite;
	 };

	/*Funciones*/
	function ConvertItalics( res, cad, pos, par ) {
		let len=cad.length, ret='', com=cad[pos];

		for (pos++; pos<len; pos++) {
			if ( cad[pos]==com ) break;
			else                 ret+=cad[pos];
		}

		res+=`<span class="italics">${ret}</span>`;

		return[res,pos];
	 };
	function ConvertBolder ( res, cad, pos, par ) {
		let len=cad.length, ret='', com=cad[pos];

		for (pos+=2; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com ) break;
			else ret+=cad[pos];
		}

		pos++;
		res+=`<span class="bolder">${ret}</span>`;

		return[res,pos];
	 };
	function ConvertIBolder( res, cad, pos, par ) {
		let len=cad.length, ret='', com=cad[pos];

		for (pos+=3; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com && cad[pos+2]==com ) break;
			else ret+=cad[pos];
		}

		pos+=2;
		res+=`<span class="italics bolder">${ret}</span>`;

		return[res,pos];
	 };
	function ConvertCrossed( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for (pos+=2; pos<len; pos++) {
			if ( cad[pos]=='~' && cad[pos+1]=='~' ) break;
			else                                    ret+=cad[pos];
		}

		pos++;
		res+=`<span class="crossed">${ret}</span>`;

		return[res,pos];
	 };
	function ConvertPCode  ( res, cad, pos, par ) {
		let len=cad.length, ret='', com=cad[pos];

		for (pos+=3; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com && cad[pos+2]==com ) break;
			else ret+=cad[pos];
		}

		pos+=2;
		res+=`<code>${ret}</code>`;

		return[res,pos];
	 };
	function ConvertLink   ( res, cad, pos, par ) {
		let len=cad.length, num=0, lin='', tex='', iss=cad.substring(pos-1, pos)===' ';

		for (pos++; pos<len; pos++) {
			if      ( cad[pos]=='['             ) tex+=cad[pos], num++;
			else if ( cad[pos]==']' && num--==0 ) break;
			else                                  tex+=cad[pos];
		}

		for (num=0, pos+=2; pos<len; pos++) {
			if ( cad[pos]==')' ) break;
			else                 lin+=cad[pos];
		}

		tex =ConvertInlines(tex,par);
		res+=`${iss?'&nbsp;':''}<a href="${lin}">${tex}</a>`;

		return[res,pos];
	 };
	function ConvertALink  ( res, cad, pos, par ) {
		let len=cad.length, ret='', iss=cad.substring(pos-1, pos)===' ';

		for (;pos<len; pos++) {
			if ( isSpaces(cad,pos) ){pos--; break;}
			else                     ret+=cad[pos];
		}

		res+=`${iss?'&nbsp;':''}<a href="${ret}">${ret}</a>`;

		return[res,pos];
	 };
	function ConvertAMeil  ( res, cad, pos, par ) {
		let len=cad.length, ret='', iss=cad.substring(pos-1, pos)===' ';

		if ( cad[pos]=='<' ) pos++;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\n' ){pos--; break;}
			else if ( cad[pos]=='>'  ) break;
			else                       ret+=cad[pos];
		}

		res+=`${iss?'&nbsp;':''}<a href="mailto:${ret}">${ret}</a>`;

		return[res,pos];
	 };
	function ConvertImage  ( res, cad, pos, par ) {
		let len=cad.length, num=0, lin='', tex='';

		for (pos+=2; pos<len; pos++) {
			if      ( cad[pos]=='['             ) tex+=cad[pos], num++;
			else if ( cad[pos]==']' && num--==0 ) break;
			else                                  tex+=cad[pos];
		}

		for (num=0, pos+=2; pos<len; pos++) {
			if ( cad[pos]==')' ) break;
			else                 lin+=cad[pos];
		}

		res+=`<img src="${lin}" alt="${tex}"/>`;

		return[res,pos];
	 };
	function ConvertInlines( cad, par           ) {
		let res='', pos=0, len=cad.length;

		for (;pos<len; pos++) {
			if      ( isLink (cad,pos) )[res,pos]=ConvertLink (res,cad,pos,par);
			else if ( isALink(cad,pos) )[res,pos]=ConvertALink(res,cad,pos,par);
			else if ( isAMeil(cad,pos) )[res,pos]=ConvertAMeil(res,cad,pos,par);
			else if ( isImage(cad,pos) )[res,pos]=ConvertImage(res,cad,pos,par);
			else if ( cad[pos]=='\\'   ) res+=cad[++pos];
			else if (
				(cad[pos]=='*' && cad[pos+1]=='*' && cad[pos+2]=='*') ||
				(cad[pos]=='_' && cad[pos+1]=='_' && cad[pos+2]=='_')
			)[res,pos]=ConvertIBolder(res,cad,pos,par);
			else if (
				(cad[pos]=='`' && cad[pos+1]=='`' && cad[pos+2]=='`') ||
				(cad[pos]=='~' && cad[pos+1]=='~' && cad[pos+2]=='~')
			)[res,pos]=ConvertPCode(res,cad,pos,par);
			else if (
				(cad[pos]=='*' && cad[pos+1]=='*') ||
				(cad[pos]=='_' && cad[pos+1]=='_')
			)                                           [res,pos]=ConvertBolder (res,cad,pos,par);
			else if ( cad[pos]=='~' && cad[pos+1]=='~' )[res,pos]=ConvertCrossed(res,cad,pos,par);
			else if ( cad[pos]=='*' || cad[pos]=='_'   )[res,pos]=ConvertItalics(res,cad,pos,par);
			else                                         res+=cad[pos];
		}

		return res;
	 };

	/*Funciones*/
	function SetCodeTitle( res, lis, pos, par         ) {
		let lit=lis[pos], tem='';

		tem =ConvertInlines(lit.cont, par);
		tem =`<h${lit.number}>${lit.cont}</h${lit.number}>\n`;
		res+=tem

		return[res, pos];
	 };
	function SetCodeQuote( res, lis, pos, par         ) {
		let tem='';

		do {
			if ( tem!='' ) tem+='\n';

			tem+=lis[pos].cont;
		}
		while( lis[pos+1] && ( lis[pos+1].type==TYPE_QUOTE || (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') ) && ++pos );

		// res+=`<blockquote>${sCompileMD(-1,tem,par)}</blockquote>\n`;
		res+=`<blockquote>${ConvertInlines(tem,par)}</blockquote>\n`;

		return[res,pos];
	 };
	function SetCodeList ( res, lis, pos, par, tab='' ) {
		let tem='', inl=true;

		do {
			tes='';

			do {
				if ( tes!='' ) tes+='<br>';
				tes+=lis[pos].cont;
			}
			while( lis[pos+1] && (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') && pos++ );

			if ( lis[pos+1] && lis[pos+1].type==TYPE_LIST && lis[pos].level<lis[pos+1].level ) {
				[tes,pos]=SetCodeList(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl      =false;
			}
			else if ( lis[pos+1] && lis[pos+1].type==TYPE_ORDER && lis[pos].level<lis[pos+1].level ) {
				[tes,pos]=SetCodeOrder(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl      =false;
			}

			tem+=`\t${tab}<li>${tes}${inl?'':'\t'+tab}</li>\n`;
		}
		while(
			lis[pos+1] &&
			(
				lis[pos+1].type ==TYPE_LIST       &&
				lis[pos  ].list ==lis[pos+1].list &&
				lis[pos  ].level==lis[pos+1].level
			) &&
			pos++
		);

		tem =ConvertInlines(tem,par);
		res+=`${tab}<ul>\n${tem}${tab}</ul>\n`;

		return[res, pos];
	 };
	function SetCodeOrder( res, lis, pos, par, tab='' ) {
		let tem='', inl=true;

		do {
			tes='';

			do {
				if ( tes!='' ) tes+='<br>';
				tes+=lis[pos].cont;
			}
			while( lis[pos+1] && (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') && pos++ );

			if ( lis[pos+1] && lis[pos+1].type==TYPE_LIST && lis[pos].level<lis[pos+1].level ) {
				[tes,pos]=SetCodeList(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl      =false;
			}
			else if ( lis[pos+1] && lis[pos+1].type==TYPE_ORDER && lis[pos].level<lis[pos+1].level ) {
				[tes,pos]=SetCodeOrder(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl      =false;
			}

			tem+=`\t${tab}<li>${tes}${inl?'':'\t'+tab}</li>\n`;
		}
		while(
			lis[pos+1] &&
			(
				lis[pos+1].type ==TYPE_ORDER      &&
				lis[pos  ].list ==lis[pos+1].list &&
				lis[pos  ].level==lis[pos+1].level
			) &&
			pos++
		);

		tem =ConvertInlines(tem,par);
		res+=`${tab}<ol>\n${tem}${tab}</ol>\n`;

		return[res, pos];
	 };
	function SetCodePCode( res, lis, pos, par         ) {
		res+=`<pre type="${lis[pos].code}">${lis[pos].cont}</pre>\n`;

		return[res, pos];
	 };
	function SetCodeLine ( res, lis, pos, par         ) {
		res+=`<line></line>\n`;

		return[res, pos];
	 };
	function SetCodeTable( res, lis, pos, par, tab='' ) {
		let ist=false, tem='';

		if ( lis[pos+1] ) {
			let cad=lis[pos+1].cont, pot=0, len=cad.length;
			for (;pot<len && (cad[pot]=='|' || cad[pot]=='-' || isSpacesTabs(cad,pot)); pot++);
			lis[pos].isTitle=ist=pot>=len;
		}

		do {

			let cms=SetCodeCamps(res,lis,pos,par);

			if ( lis[pos].isTitle || (lis[pos-1] && !lis[pos-1].isTitle) ) {
				if ( tem!='' ) tem+='\n';
				tem+='\t<tr>\n\t\t'+ cms.reduce((r,v)=>r+`<th>${v}</th>`, '') +'\n\t</tr>';
			}
		}
		while ( lis[pos+1] && lis[pos+1].type==TYPE_TABLE && ++pos );

		tem =ConvertInlines(tem,par);
		res+=`<table>\n${tem}\n</table>\n`;

		return[res,pos];
	 };
	function SetCodeCamps( res, lis, pos, par         ) {
		let cad=lis[pos].cont, pot=0, len=cad.length, cms=[];

		do {
			let cam='';
			for (pot++; pot<len && isSpacesTabs(cad,pot); pot++);
			for (;pot<len; pot++) {
				if ( isSpacesTabs(cad,pot) ) {
					let poc=pot, tem='';
					for (;poc<len && isSpacesTabs(cad,poc); tem+=cad[poc++]);
					if ( cad[poc]!='|' ) {
						pot =poc-1;
						cam+=tem;
					}
					else {
						pot=poc;
						break;
					 };
				}
				else if ( cad[pot]=='\\' ) cam+=cad[++pot];
				else                       cam+=cad[pot];
			}

			if ( cam!='' ) cms.push(cam);
		}
		while (cad[pot]=='|');

		return cms;
	 };
	function SetCodeOther( res, lis, pos, par         ) {
		res+=`<p>${ConvertInlines(lis[pos].cont,par)}</p>\n`;

		return[res,pos];
	 };
	function SetCode     ( lis, par={}                ) {
		let res='', len=lis.length, pos=0;

		for (;pos<len; pos++) {
			switch (lis[pos].type) {
				case TYPE_TITLE: [res,pos]=SetCodeTitle(res,lis,pos,par); break;
				case TYPE_QUOTE: [res,pos]=SetCodeQuote(res,lis,pos,par); break;
				case TYPE_LIST : [res,pos]=SetCodeList (res,lis,pos,par); break;
				case TYPE_ORDER: [res,pos]=SetCodeOrder(res,lis,pos,par); break;
				case TYPE_PCODE: [res,pos]=SetCodePCode(res,lis,pos,par); break;
				case TYPE_LINE : [res,pos]=SetCodeLine (res,lis,pos,par); break;
				case TYPE_TABLE: [res,pos]=SetCodeTable(res,lis,pos,par); break;
				default        : [res,pos]=SetCodeOther(res,lis,pos,par); break;
			}
		}

		return res;
	 }

	/*Inicio*/
	function Compall( cad, par={}           ) {
		return sCompileHTML( -1, SetCode(GetChilds(cad,par), par), par );
	 };
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			sCompileINC(cad,par)
			.run(x=>sCompileDEF(x,par))
			.run(x=>{par.data=Compall(x,par); run(par)})
			.err(err);
		});
	 };
	function Escape ( res, cad, pos, par={} ) {
		return[res, pos];
	 };

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileBC   = function( result, data, position, params ) {
	/*Constantes*/
	const TYPE_TAG =1;
	const TYPE_TAGF=2;
	const TYPE_TAGN=3;
	const TYPE_TAGC=4;
	const TYPE_KEYF=5;
	const TYPE_MEDI=6;
	const TYPE_ARRB=7;
	const TYPE_PROP=8;
	const TYPE_INCL=9;

	/*Is*/
	function isStartTag( cad, pos ) {
		return isLetter(cad,pos) || cad[pos]=='-' || cad[pos]=='_';
	 }
	function isChild   ( cad, pos ) {
		return (
			cad[pos]==':' && (cad[pos+1]=='<' || cad[pos+1]=='>') ||
			cad[pos]==';'
		);
	 }
	function isBreak   ( cad, pos ) {
		return (
			cad[pos]=='}'  ||
			cad[pos]=='\n' ||
			isChild(cad,pos)
		);
	 }
	function isFrom    ( cad, pos ) {
		return(
			cad[pos  ]=='f' &&
			cad[pos+1]=='r' &&
			cad[pos+2]=='o' &&
			cad[pos+3]=='m' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isTo      ( cad, pos ) {
		return(
			cad[pos  ]=='t' &&
			cad[pos+1]=='o' &&
			isSpecial(cad,pos+2) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isTag     ( cad, pos ) {
		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isRInclude  (cad,pos) ) pos=sCompileINC('',cad,pos,{})[1];
			else if ( isCommentJS (cad,pos) ) pos=GetComment ('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if (
			cad[pos]=='.' || cad[pos]=='#' || cad[pos]=='<' || cad[pos]=='>' ||
			cad[pos]=='+' || cad[pos]=='*' || cad[pos]==':' || cad[pos]=='@' ||
			cad[pos]=='&' || cad[pos]=='$' || cad[pos]==','
		) return true;

		if ( isFrom(cad,pos) || isTo(cad,pos) ) return false;

		if ( isStartTag(cad,pos) ) {
			for ( ;pos<len; pos++ ) {
				if      ( isNumber(cad,pos) || isStartTag(cad,pos) );
				else if ( isRInclude (cad,pos)                     ) pos=sCompileINC('',cad,pos,{})[1];
				else if ( isCommentJS(cad,pos)                     ) pos=GetComment ('',cad,pos,{})[1];
				else if ( cad[pos]=='{'                            ) pos=sCompileBJ ('',cad,pos,{})[1];
				else break;
			}

			for ( ;pos<len && isSpacesTabs(cad,pos); pos++ );

			if (
				isBreak(cad,pos) || cad[pos]=='>' || cad[pos]==',' ||
				cad[pos]=='+' || cad[pos]=='*' || cad[pos]=='@' ||
				cad[pos]=='[' || cad[pos]=='&'
			) return true;

			if      ( (cad[pos]=='.'||cad[pos]=='#') && isStartTag(cad,pos+1) ) return true;
			else if ( cad[pos]==':'                  && isStartTag(cad,pos+1) ) return true;
		}

		return false;
	 }
	function isProp    ( cad, pos ) {
		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos=GetComment('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if ( isFrom(cad,pos) || isTo(cad,pos) ) return false;

		for ( ;pos<len && (isNumber(cad,pos)||isStartTag(cad,pos)); pos++ );
		for ( ;pos<len && isSpacesTabs(cad,pos); pos++ );

		if      ( isBreak    (cad,pos)                                     ) return false;
		else if ( isStringCSS(cad,pos)                                     ) return true;
		else if ( cad[pos]=='{'                                            ) return true;
		else if ( cad[pos]==':'                  && !isStartTag(cad,pos+1) ) return true;
		else if ( cad[pos]=='$'                  && isLetter   (cad,pos+1) ) return true;
		else if ( (cad[pos]=='.'||cad[pos]=='#') && !isStartTag(cad,pos+1) ) return true;
		else if ( cad[pos]=='-' || cad[pos]=='_'                           ) return true;
		else if ( isLetter(cad,pos) || isNumber(cad,pos)                   ) return true;

		return false;
	 }
	function isPerc    ( cad, pos ) {
		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos=GetComment('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if ( cad[pos]=='.' || isNumber(cad,pos) || isFrom(cad,pos) || isTo(cad,pos) ) {
			for ( ;pos<len && !isSpaces   (cad,pos) && !isBreak(cad,pos); pos++ );
			for ( ;pos<len && isSpacesTabs(cad,pos) && !isBreak(cad,pos); pos++ );

			return isBreak(cad,pos);
		}

		return false;
	 }
	function isRgba    ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='r' &&
				cad[pos+1]=='g' &&
				cad[pos+2]=='b' &&
				cad[pos+3]=='a' &&
				isSpecial(cad,pos+4) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		let len=cad.length;

		for ( pos+=4; pos<len && isSpacesTabs(cad,pos); pos++ );

		return cad[pos]=='(';
	 }
	function isPropProc( cad, pos ) {
		return (
			cad=='top'            ||
			cad=='left'           ||
			cad=='right'          ||
			cad=='bottom'         ||
			cad=='width'          ||
			cad=='height'         ||
			cad=='margin'         ||
			cad=='margin-top'     ||
			cad=='margin-left'    ||
			cad=='margin-right'   ||
			cad=='margin-bottom'  ||
			cad=='padding'        ||
			cad=='padding-top'    ||
			cad=='padding-left'   ||
			cad=='padding-right'  ||
			cad=='padding-bottom' ||
			cad=='max-height'     ||
			cad=='max-width'      ||
			cad=='min-height'     ||
			cad=='min-width'      ||
			cad=='font-size'      ||
			cad=='box-shadow'     ||
			cad=='border-radius'
		);
	 };

	/*Funciones*/
	function GetProp        ( name, value, par        ) {
		let val='', cad=value, len=value.length, pos=0;

		if ( isPropProc(name) ) {
			for ( ;pos<len; pos++ ) {
				if ( isNumber(cad,pos) ) {
					for ( ;pos<len && (isNumber(cad,pos)||cad[pos]=='.'); val+=cad[pos++] );

					if      ( isSpecial(cad,pos) ) val+='px'+cad[pos];
					else if ( pos>=len           ) val+='px';
					else if ( pos<len            ) val+=cad[pos];
				}
				else if ( cad[pos]=='#' ) {
					val+=cad[pos++];
					for ( ;pos<len && (isLetter(cad,pos) || isNumber(cad,pos)); val+=cad[pos++] );
					pos--;
				}
				else if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos]=GetChildsCssVar(val,cad,pos,par);
				else if ( isRgba(cad,pos) ) {
					for ( let key=-1; pos<len; pos++ ) {
						if      ( cad[pos]=='('                    ) val+=cad[pos], key++;
						else if ( cad[pos]==')' && key--==0        ){val+=cad[pos]; break}
						else if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos]=GetChildsCssVar(val,cad,pos,par);
						else val+=cad[pos];
					}
				}
				else if ( cad[pos]=='$' && cad[pos+1]=='{' )[val,pos]=sCompileJS(val,cad,pos,par);
				else val+=cad[pos];
			}
		}
		else {
			for (;pos<len; pos++) {
				if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos]=GetChildsCssVar(val,cad,pos,par);
				else                                    val+=cad[pos];
			}
		}

		return name+' '+val;
	 };
	function GetChildsCssVar( res, cad, pos, par      ) {
		let tem='', len=cad.length;

		for ( pos++; pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || cad[pos]=='-' || cad[pos]=='_'); tem+=cad[pos++] );

		res+=`var(--${tem})${cad[pos]?cad[pos]:''}`;

		return[res,pos];
	 };
	function GetChildsFin   ( cad, pos, par           ) {
		let len=cad.length, ret='';

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) ) pos     =GetComment('',cad,pos,{})[1];
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( isBreak    (cad,pos) ) break;
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='['        )[ret,pos]=GetAttrs  (ret,cad,pos,{zip:true});
			else                             ret+=cad[pos];
		}

		return[ret,pos];
	 }
	function GetChildsLvl   ( cad, pos, lva           ) {
		let len=cad.length, lvl=0;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS (cad,pos) ){pos=GetComment('',cad,pos,{})[1]; cad[pos]=='\n'&&(lvl=0)}
			else if ( isSpacesTabs(cad,pos) ) lvl++;
			else break;
		}

		for ( let num=1; pos<len; pos++ ) {
			if      ( cad[pos]==':' && cad[pos+1]=='<' ) lvl=lva-(num++), pos++;
			else if ( cad[pos]==':' && cad[pos+1]=='>' ) lvl=lva+(num++), pos++;
			else if ( cad[pos]==';'                    ) lvl=lva;
			else if ( isSpacesTabs(cad,pos)            );
			else break;
		}

		return[lvl,pos];
	 }
	function GetChildRIncl  ( ite, cad, pos, par, lva ) {
		let len=cad.length, lvl=0, ret='';

		[ret,pos]=sCompileINC('',cad,pos,par);

		ite.push({
			level: lvl,
			value: ret,
			type : TYPE_INCL,
		});

		return[ite,pos,lva];
	 };
	function GetChildsTag   ( ite, cad, pos, par, lva ) {
		let len=cad.length, lvl=0, ret='';

		[lvl,pos]=GetChildsLvl(cad,pos,lva);
		[ret,pos]=GetChildsFin(cad,pos,par);

		if ( isChild(cad,pos) ) pos--;

		if      ( ret.indexOf('@media'    )==0 ) tip=TYPE_MEDI;
		else if ( ret.indexOf('@keyframes')==0 ) tip=TYPE_KEYF;
		else if ( ret[0]=='@'                  ) tip=TYPE_ARRB;
		else if ( ret.indexOf(',')>-1          ) tip=TYPE_TAGC;
		else                                     tip=TYPE_TAG;

		ite.push({
			level: lvl,
			value: ret,
			type : tip,
		});

		return[ite,pos,lvl];
	 }
	function GetChildsProp  ( ite, cad, pos, par, lva ) {
		let len=cad.length, lvl=0, ret='', nam='', val='';

		[lvl,pos]=GetChildsLvl(cad,pos,lva);
		[ret,pos]=GetChildsFin(cad,pos,par);

		for ( let car=ret, por=0, ler=ret.length, isn=0; por<ler; por++ ) {
			if      ( isn==0 && isSpacesTabs(car,por) ) isn=1;
			else if ( isn==0                          ) nam+=car[por];
			else if ( isn==1                          ) val+=car[por];
		}

		if ( isChild(cad,pos) ) pos--;

		ite.push({
			level: lvl,
			value: GetProp(nam,val,par),
			type : TYPE_PROP,
		});

		return[ite,pos,lvl];
	 }
	function GetChildsPerc  ( ite, cad, pos, par, lva ) {
		let len=cad.length, lvl=0, ret='';

		[lvl,pos]=GetChildsLvl(cad,pos,lva);
		[ret,pos]=GetChildsFin(cad,pos,par);

		if ( !isNaN(parseFloat(ret)) ) ite.push({ level:lvl, value:parseFloat(ret)+'%', type:TYPE_TAGN });
		else                           ite.push({ level:lvl, value:ret                , type:TYPE_TAGF });

		if ( isChild(cad,pos) ) pos--;

		return[ite,pos,lvl];
	 }
	function GetChildsIgnor ( ite, cad, pos, par, lva ) {
		for ( let len=cad.length; pos<len; pos++ ) {
			if      ( isRInclude (cad,pos) )[ite,pos,lva]=GetChildRIncl(ite,cad,pos,par,lva);
			else if ( isCommentJS(cad,pos) ) pos         =GetComment   ('',cad,pos,{})[1];
			else if ( cad[pos]=='\n'       ) break;
		}

		return[ite,pos,lva];
	 };
	function GetChilds      ( cad, par                ) {
		let len=cad.length, pos=0, ite=[], lva=0;

		for (;pos<len; pos++) {
			if      ( isRInclude (cad,pos) )[ite,pos,lva]=GetChildRIncl (ite,cad,pos,par,lva);
			else if ( isCommentJS(cad,pos) ) pos         =GetComment    ('' ,cad,pos,{}     )[1];
			else if ( isTag      (cad,pos) )[ite,pos,lva]=GetChildsTag  (ite,cad,pos,par,lva);
			else if ( isProp     (cad,pos) )[ite,pos,lva]=GetChildsProp (ite,cad,pos,par,lva);
			else if ( isPerc     (cad,pos) )[ite,pos,lva]=GetChildsPerc (ite,cad,pos,par,lva);
			else                            [ite,pos,lva]=GetChildsIgnor(ite,cad,pos,par,lva);
		}

		return ite;
	 };

	/*Funciones*/
	function SetCodeSpa ( cad                               ) {
		let len=cad.length, res='', pos=0;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) )[res,pos]=GetSpacesCSS(res,cad,pos,{zip:true});
			else if ( cad[pos]=='&'         );
			else                              res+=cad[pos];
		}

		if ( res.indexOf('$')>-1 ) res=res.substring(res.indexOf('$')+1);

		return res;
	 };
	function SetCodePar ( pap, paa={}                       ) {
		let res=[''], tem, isw=0;
		let fes=_pap=>{
			if ( _pap.type===TYPE_TAGC ) {
				tem=res, res=[];

				for (let y=0; y<tem.length; y++) {
					for ( let z=0; z<_pap.arrayTagC.length; z++ ) {
						res.push(` ${_pap.arrayTagC[z]} ${tem[y]} `);
					}
				}
			}
			else for (let y=res.length; y--; res[y]=' '+_pap.value+' '+res[y]);
		 };

		pap.push(paa);
		fes(paa);

		let lvl=paa.level;
		for (let x=pap.length; x--;) {
			if ( pap[x].level<lvl ) {
				lvl=pap[x].level;

				fes(pap[x]);
			}
		}

		return SetCodeSpa( res.join(',') );
	 };
	function SetCodeTag ( res, chi, pos, pap, lva, tab, par ) {
		let ret='', lep=pap.length, pop=0;
		let pat=SetCodePar(pap, chi[pos]);

		/*Agregando*/
		if ( !chi[pos+1] || chi[pos+1].type!=TYPE_PROP ) return[res,pos,pap,lva];
		if ( res!=''                                   ) res+=`\n${tab} ${!chi[pos-1] || chi[pos-1].type!=TYPE_INCL?'}':''}\n`;

		return[res+`${tab}${SetCodeSpa(` ${pat} { `)}`,pos,pap,lva];
	 };
	function SetCodeTagc( res, chi, poc, pap, lva, tab, par ) {
		let prs=[], tem='', cad=chi[poc].value, len=cad.length, pos=0;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]==',' ){prs.push(tem); tem=''}
			else                 tem+=cad[pos];
		}

		if ( tem!='' ) prs.push(tem);

		chi[poc].arrayTagC=prs;

		/*Parent*/
		let pat=SetCodePar(pap, chi[poc]);

		/*Agregando*/
		if ( pat=='' || !chi[poc+1] || chi[poc+1].type!=TYPE_PROP ) return[res,poc,pap,lva];
		if ( res!=''                                              ) res+=`\n${tab} ${!chi[pos-1] || chi[pos-1].type!=TYPE_INCL?'}':''}\n`;

		return[res+`${tab}${SetCodeSpa(` ${pat} { `)}`,poc,pap,lva];
	 };
	function SetCodeProp( res, chi, pos, pap, lva, tab, par ) {
		if ( !pap.length ) return[res,pos,pap,lva];

		if ( res=='' && pap.length==1 && pap[0].level==-1 )
			res+=pap[0].value+' {';

		let cad=chi[pos].value, len=cad.length, por=0, ret='', poi=false;

		for ( ;por<len; por++ ) {
			if      ( (cad[por]==' '||cad[por]=='\t') && !poi ) ret+=':', poi=true;
			else if ( cad[por]==':' && !poi                   ) ret+=':', poi=true;
			else if ( cad[por]==':' &&  poi                   );
			else if ( isSpaces   (cad,por)                    )[ret,por]=GetSpacesCSS(ret,cad,por,{zip:true});
			else if ( isStringCSS(cad,por)                    )[ret,por]=GetString   (ret,cad,por,par);
			else                                                ret+=cad[por];
		}

		return[`${res}${tab}\n${tab}\t${ret};`,pos,pap,lva];
	 };
	function SetCodeKeyf( res, chi, poc, pap, lva, tab, par ) {
		let cad=chi[poc].value, lec=chi.length, len=cad.length, pos=0, ret='', cht=[], psa=poc;

		for ( poc++; poc<lec; poc++ ) {
			if (
				chi[poc].type===TYPE_TAG  ||
				chi[poc].type===TYPE_KEYF ||
				chi[poc].type===TYPE_MEDI ||
				chi[poc].type===TYPE_ARRB
			) {poc--;break}
			else cht.push(chi[poc]);
		}

		for ( ;pos<len && !isSpaces(cad,pos); pos++ );
		for ( ;pos<len &&  isSpaces(cad,pos); pos++ );

		if ( cad[pos]=='.' || isNumber(cad,pos) ) {
			let val='', nam='';
			for ( ;pos<len && !isBreak(cad,pos); val+=cad[pos++] );
			nam=IntToExcel( Hash(new Date().valueOf()+'a'), false );
			res=SetCodeProp(res,[{type:TYPE_PROP,value:`animation ${nam} ${val}`}],0,pap,0,tab)[0];
			if ( res!='' ) res+=`\n${tab} }\n`;
			ret=tab + SetCodeSpa(` @keyframes ${nam} {`);
		}
		else {
			if ( res!='' ) res+=`\n${tab} ${chi[poc-1].type!=TYPE_INCL?'}':''}\n`;
			ret=tab + SetCodeSpa(` ${chi[psa].value} {`);
		}

		ret+='\n';
		ret+=SetCode(cht, tab+'\t');

		return[res+ret,poc,pap,lva];
	 };
	function SetCodeMedi( res, chi, poc, pap, lva, tab, par ) {
		let cht=[], lec=chi.length, psa=poc;

		for ( poc++; poc<lec; poc++ ) {
			if ( chi[poc].level<=chi[psa].level && chi[poc].type!=TYPE_PROP ){poc--;break}
			else                                                              cht.push(chi[poc]);
		}

		if ( res!='' ) res+=`\n${tab} ${chi[psa-1].type!=TYPE_INCL?'}':''}\n`;

		res+= tab + SetCodeSpa(` ${chi[psa].value} {`) +'\n';
		res+= SetCode(cht, tab+'\t', par);

		return[res,poc,pap,lva];
	 };
	function SetCodeArro( res, chi, poc, pap, lva, tab, par ) {
		let cht=[], lec=chi.length, psa=poc;

		for ( poc++; poc<lec; poc++ ) {
			if ( chi[poc].level<=chi[psa].level && chi[poc].type!=TYPE_PROP ){poc--;break}
			else                                                              cht.push(chi[poc]);
		}
		if ( res!='' ) res+=`\n${tab} ${chi[psa-1].type!=TYPE_INCL?'}':''}\n`;

		res+= tab + SetCodeSpa(` ${chi[psa].value} {`);

		cht.forEach((x,i)=>res+=SetCodeProp('', cht, i, [chi[psa]], chi[psa].level, tab, par )[0]);

		return[res,poc,pap,lva];
	 };
	function SetCodeIncl( res, chi, poc, pap, lva, tab, par ) {
		res+=chi[poc].value;

		return[res,poc,pap,lva];
	 };
	function SetCode    ( chi, tab=-1, par={}               ) {
		let len=chi.length, pos=0, res='', pap=[], lva=0;

		if ( tab===-1 ) {
			tab='';
			for ( let pol=0; len && pol<chi[0].level; tab+='\t', pol++ );
		}

		if ( typeof par.sClass==VALUE_STRING && par.sClass!='' )
			pap.push({ level:-1, type:TYPE_TAG, value:par.sClass });

		for ( ;pos<len; pos++ ) {
			switch ( chi[pos].type ) {
				case TYPE_TAGF:
				case TYPE_TAGN:
				case TYPE_TAG : [res,pos,pap,lva]=SetCodeTag (res,chi,pos,pap,lva,tab,par); break;
				case TYPE_TAGC: [res,pos,pap,lva]=SetCodeTagc(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_PROP: [res,pos,pap,lva]=SetCodeProp(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_KEYF: [res,pos,pap,lva]=SetCodeKeyf(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_MEDI: [res,pos,pap,lva]=SetCodeMedi(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_ARRB: [res,pos,pap,lva]=SetCodeArro(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_INCL: [res,pos,pap,lva]=SetCodeIncl(res,chi,pos,pap,lva,tab,par); break;
			}
		}

		if ( res!='' ) res+=`\n${tab} }`;

		return res;
	 };

	/*Inicio*/
	function Compall( cad, par={}           ) {
		return sCompileCSS( -1, SetCode(GetChilds(cad,par),-1,par), par );
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			sCompileDEF(cad,par)
			.run(x=>sCompileINC(x,par))
			.run(x=>{par.data=Compall(x,par); run(par)})
			.err(err);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='';
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let ite=par.includeTemp                               ; delete par.includeTemp;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString (ret,cad,pos,par,true);
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ(ret,cad,pos,par);
			else if ( cad[pos]=='}'        ) break;
			else if ( cad[pos]=='\\'       ) ret+=cad[pos], ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		ret=Compall(ret,par);

		if ( inc ) ret=ini + ret + cad[pos];

		return[res+ret, pos];
	 }

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileBH   = function( result, data, position, params ) {
	/*Is*/
	function isBreak    ( cad, pos, lin ) {
		return(
			(cad[pos]==':' && (cad[pos+1]=='<'||cad[pos+1]=='>')) ||
			cad[pos]==';' ||
			(lin && lin.body=='' && cad[pos]=='/') ||
			cad[pos]=='\n'
		);
	 }
	function isChild    ( cad, pos      ) {
		return(
			cad[pos]==';'
			||
			(
				cad[pos]==':'
				&&
				(cad[pos+1]=='<' || cad[pos+1]=='>')
			)
		);
	 }
	function isTag      ( cad, pos      ) {
		return (
			isLetter(cad,pos) ||
			isNumber(cad,pos) ||
			cad[pos]=='-'     ||
			cad[pos]=='_'
		);
	 }
	function isFinTag   ( cad, pos      ) {
		return(
			cad[pos]=='.' ||
			cad[pos]=='#' ||
			cad[pos]=='(' ||
			isSpacesTabs(cad,pos) ||
			isBreak(cad,pos)
		);
	 }
	function isSp       ( cad, pos      ) {
		if ( cad[pos]!='&'  ) return false;

		let len=cad.length;

		for ( pos++; pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || cad[pos]=='#'); pos++ );

		return cad[pos]==';';
	 }
	function isHtmlTag  ( cad, pos      ) {
		if ( cad[pos]!='<' || (!isLetter(cad,pos+1) && !isNumber(cad,pos+1) && cad[pos+1]!='/') ) return false;

		return true;
	 }
	function isBody     ( cad, pos, lin ) {
		return(
			(cad[pos]==' ' || cad[pos]=='\t')
			&&
			lin.tag!=''
		);
	 };
	function isRIncludeC( cad, pos      ) {
		if ( isSpacesTabs(cad,pos) ) {
			let len=cad.length;

			for (;pos<len && isSpacesTabs(cad,pos); pos++);

			return isRInclude(cad,pos);
		}
		else if ( isCommentJS(cad,pos) ) return isRInclude(cad,pos);
	 };

	/*Funciones*/
	function GetChildsSp        ( res, cad, pos, par      ) {
		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]==';' ){res+=cad[pos]; break}
			else                 res+=cad[pos];
		}

		return[res,pos];
	 }
	function GetChildsHtmlTag   ( res, cad, pos, par      ) {
		let len=cad.length;

		for ( ;pos<len && cad[pos]!='>'; res+=cad[pos++] );

		res+=cad[pos];

		return[res,pos];
	 }
	function GetChildsSubb      ( res, cad, pos, par      ) {
		let ret='';
		[ret,pos]=GetAttrs  (ret,cad,pos,Object.assign(par, {cleanSpaces:0, replacekeys:0}),false);
		res     +=sCompileBH(-1, ret,par);

		return[res,pos];
	 }
	function GetChildsLvl       ( lin, cad, pos, par, lva ) {
		let len=cad.length, lvl=0, isc=false;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS (cad,pos) ){pos=GetComment('',cad,pos,{})[1]; cad[pos]=='\n'&&(lvl=0)}
			else if ( isSpacesTabs(cad,pos) ) lvl++;
			else break;
		}

		let lvn=0;
		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]==':' && cad[pos+1]=='<' ) isc=true, lvn--, pos++;
			else if ( cad[pos]==':' && cad[pos+1]=='>' ) isc=true, lvn++, pos++;
			else if ( cad[pos]==';'                    ) isc=true;
			else if ( isSpacesTabs(cad,pos)            );
			else break;
		}

		lin.level=(isc?lva:0)+lvl+lvn;
		lin.level<0 && (lin.level=0);
		pos--;

		return pos;
	 }
	function GetChildsBody      ( lin, cad, pos, par      ) {
		let len=cad.length, bod='';

		for ( pos++; pos<len; pos++ ) {
			if      ( isBreak    (cad,pos,{body:bod})    ){pos--; break}
			else if ( isCommentJS(cad,pos)               ) pos     =GetComment      ('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos)               )[bod,pos]=GetString       (bod,cad,pos,par,true);
			else if ( isHtmlTag  (cad,pos)               )[bod,pos]=GetChildsHtmlTag(bod,cad,pos,par);
			else if ( isSp       (cad,pos)               )[bod,pos]=GetChildsSp     (bod,cad,pos,par);
			else if ( cad[pos]=='['                      )[bod,pos]=GetChildsSubb   (bod,cad,pos,par);
			else if ( cad[pos]=='{'                      )[bod,pos]=sCompileBJ      (bod,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\' && cad[pos+1]=='n'  ) bod+='<br>'  , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='t'  ) bod+='&emsp;', pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='s'  ) bod+='&nbsp;', pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=="'"  ) bod+='&#39;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='"'  ) bod+='&#34;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='<'  ) bod+='&#60;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='>'  ) bod+='&#62;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='\\' ) bod+='\\'    , pos++;
			else if ( cad[pos]=='\\'                     ) bod+=cad[++pos];
			else                                           bod+=cad[pos];
		}

		lin.body = bod;

		return pos;
	 }
	function GetChildsTag       ( lin, cad, pos, par      ) {
		let len=cad.length, ret='';

		for ( ;pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos     =GetComment('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\'       ) ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		lin.tag=ret;

		return pos;
	 }
	function GetChildsIds       ( lin, cad, pos, par      ) {
		let len=cad.length, ret='', not=false;

		for ( pos++; pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos     =GetComment('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='!'        ) not     =true;
			else if ( cad[pos]=='\\'       ) ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		lin.id=ret;

		if ( lin.tag=='' ) lin.tag='div';
		if (  not        ) lin.notView=true;
		if ( !not        ) par.idsView[ret]=1;

		return pos;
	 }
	function GetChildsAttsString( res, cad, pos, par      ) {
		let ret, len, por=0;

		[ret,pos]=GetString('',cad,pos,par,true,false);

		len=ret.length;

		for ( ;por<len; por++ ) {
			if      ( ret[por]=='\\' ) res+=ret[por], res+=ret[++por];
			else if ( ret[por]=='"'  ) res+='\\"';
			else                       res+=ret[por];
		}

		return[res, pos];
	 }
	function GetChildsAtts      ( lin, cad, pos, par      ) {
		let len=cad.length, ret=[], isk=0, key='', val='';
		let fre=obj=>{ret.push(obj); isk=0; key=''; val='';};

		let col='';
		for ( pos++; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                       )[col,pos]=GetComment('',cad,pos,par);
			else if ( cad[pos]==')'                              ) break;
			else if ( isk==1 && cad[pos]=='{'                    )[val,pos]=sCompileBJ(val,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( isk==1 && isStringCSS (cad,pos)            )[val,pos]=GetChildsAttsString(val,cad,pos,par), fre({key,val});
			else if ( isk==1 && isSpacesTabs(cad,pos) && val=='' );
			else if ( isk==1 && isSpacesTabs(cad,pos)            ) fre({key,val});
			else if ( isk==1                                     ) val+=cad[pos];
			else if ( isk==0 && cad[pos]=='='                    ) isk=1;
			else if ( isk==0 && cad[pos]=='{'                    )[key,pos]=sCompileBJ(key,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( isk==0 && isSpacesTabs(cad,pos) && key=='' );
			else if ( isk==0 && isSpacesTabs(cad,pos)            ){
				let psa=pos;
				for( ;pos<len && isSpacesTabs(cad,pos); pos++);
				if ( cad[pos]!='=' ) fre({key,val});
				pos--;
			}
			else if ( isk==0 ) key+=cad[pos];
		}

		if ( key!='' ) ret.push({key,val});

		for ( let x=ret.length; x--; ) {
			if ( ret[x].key==='class' ) {
				if ( lin.class!='' ) lin.class+=' ';
				lin.class+=ret[x].val;
				ret.splice(x,1);
			}
		}

		lin.atts=ret;

		if ( lin.tag=='' ) lin.tag='div';

		return pos;
	 }
	function GetChildsClass     ( lin, cad, pos, par      ) {
		let len=cad.length, ret='';

		for ( pos++; pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos     =GetComment('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\'       ) ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		if ( lin.tag  =='' ) lin.tag   ='div';
		if ( lin.class!='' ) lin.class+=' ';

		lin.class+=ret;

		return pos;
	 }
	function GetChildsRInclude  ( lin, cad, pos, par      ) {
		[lin.body,pos]=sCompileINC('', cad, pos, Object.assign(par, {includeKeys:0}));
		lin.onlyBody  =true;

		return pos;
	 };
	function GetChilds          ( cad, par                ) {
		let fli=()=>{ return{level:0, id:'', tag:'', atts:[], class:'', body:'', aclose:false} };
		let res=[], len=cad.length, pos=0, lva=0, lin=fli();

		for ( ;pos<len; pos++ ) {
			if      ( isRIncludeC(cad,pos)                        ){pos=GetChildsRInclude(lin,cad,pos,par); res.push(lin); lin=fli()}
			else if ( isCommentJS(cad,pos)                        ) pos=GetComment       ('' ,cad,pos,{} )[1];
			else if ( isBody     (cad,pos,lin)                    ) pos=GetChildsBody    (lin,cad,pos,par);
			else if ( isTag      (cad,pos)                        ) pos=GetChildsTag     (lin,cad,pos,par);
			else if ( cad[pos]=='#'                               ) pos=GetChildsIds     (lin,cad,pos,par);
			else if ( cad[pos]=='('                               ) pos=GetChildsAtts    (lin,cad,pos,par);
			else if ( cad[pos]=='.'                               ) pos=GetChildsClass   (lin,cad,pos,par);
			else if ( cad[pos]=='/'  && lin.body==''              ) lin.aclose=true;
			else if ( cad[pos]=='\n' && isBody(' ',0,lin)         ){lva=lin.level; res.push(lin); lin=fli()}
			else if ( (isSpacesTabs(cad,pos) || isChild(cad,pos)) ){
				if ( lin.tag!='' ) {
					lva=lin.level;
					res.push(lin);
					lin=fli();
				}

				pos=GetChildsLvl(lin,cad,pos,par,lva);
			}
		}

		if ( isBody(' ',0,lin) ) res.push(lin);

		return res;
	 }

	/*Funciones*/
	function GetStruct( res, lin, pos ) {
		let len=lin.length;

		if ( pos>=len           ) return[res+''  , pos];
		if ( lin[pos].level==-1 ) return[res+'\n', pos];

		let line=lin[pos], ret='', chi='';

		for ( let x=line.level; x--; ) ret+= '\t';

		if ( line.onlyBody ) {
			res+=line.body;

			return[res+'\n', pos];
		}

		ret+= '<'+
			 line.tag +
			(line.id   !=''   ? ' id="'    + (line.notView?'':'${gthis.id}-')+line.id + '"' : '') +
			(line.class!=''   ? ' class="' + line.class + '"' : '') +
			(line.atts.length ? ' '        + line.atts.reduce((r,v)=>`${r} ${v.key}="${v.val}"`,'') : '') +
			(line.aclose      ? '/' : '') +
		'>'+ line.body;

		for ( pos++; pos<len; pos++ ) {
			if ( line.level>=lin[pos].level ){pos--; break;}
			else                             [chi,pos]=GetStruct(chi,lin,pos);
		}

		if ( chi!='' ) {
			ret+= '\n' + chi;
			for ( let x=line.level; x--; ) ret+= '\t';
		}

		if ( !line.aclose )
			ret+= '</'+ line.tag +'>';

		return[res+ret+'\n', pos];
	 };

	/*Inicio*/
	function Compall( cad, par={}           ) {
		par.idsView={};

		let ite=GetChilds(cad,par), tem='', pos=0, len=cad.length;

		for ( pos=0, tem='', len=ite.length; pos<len; pos++ )
			[tem,pos]=GetStruct(tem,ite,pos);

		while ( tem[tem.length-1]=='\n' )
			tem = tem.substring(0, tem.length-1);

		return sCompileHTML(-1, tem, par);
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			sCompileDEF(cad,par)
			.run( x=>sCompileINC(x,par) )
			.run( x=>{par.data=Compall(x,par); run(par)} )
			.err(err);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='';
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let ite=par.includeTemp                               ; delete par.includeTemp;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isRInclude (cad,pos) )[ret,pos]=sCompileINC(ret,cad,pos,par);
			else if ( isCommentJS(cad,pos) )[ret,pos]=GetComment (ret,cad,pos,par);
			else if ( isStringCSS(cad,pos) )[ret,pos]=GetString  (ret,cad,pos,par,true);
			else if ( cad[pos]=='{'        )[ret,pos]=sCompileBJ (ret,cad,pos,par);
			else if ( cad[pos]=='}'        ) break;
			else if ( cad[pos]=='\\'       ) ret+=cad[pos], ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		ret=Compall(ret,par);

		if ( inc ) ret=ini + ret + cad[pos];

		return[res+ret, pos];
	 }

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileBJ   = function( result, data, position, params ) {
	/*Variables*/
	const TYPE_FUNC=1;

	/*Is*/
	function isView       ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='v' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='e' &&
				cad[pos+4]=='w' &&
				isSpecial(cad,pos+5) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=5;

		for (let len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isStyle      ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='s' &&
				cad[pos+2]=='t' &&
				cad[pos+3]=='y' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='e' &&
				isSpecial(cad,pos+6) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=6;

		for (let len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBH         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='h' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=2;

		for (let len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBC         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='c' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=2;

		for (let len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isMD         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='m' &&
				cad[pos+1]=='d' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=2;

		for (let len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 };
	function isClass      ( cad, pos ) {
		if (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpacesTabs(cad,pos+5) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		) {
			let len=cad.length, poa=pos;

			for (pos+=5; pos<len && isSpaces(cad,pos); pos++);

			return cad[pos]=='-' || cad[pos]=='_' || isLetter(cad,pos);
		}

		return false;


		// return(
		// 	cad[pos  ]=='c' &&
		// 	cad[pos+1]=='l' &&
		// 	cad[pos+2]=='a' &&
		// 	cad[pos+3]=='s' &&
		// 	cad[pos+4]=='s' &&
		// 	isSpacesTabs(cad,pos+5) &&
		// 	(isSpecial(cad,pos-1)||pos-1<0)
		// );
	 }
	function isFunction   ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		let len=cad.length;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpaces(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=8; pos<len && isSpaces(cad,pos); pos++ );

		return isLetter(cad,pos) || cad[pos]=='_';
	 }
	function isFunctionA  ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]=='=' || (isSpecial(cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.') ) break;
		}

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpecial(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=8;

		return cad[pos]=='(';
	 }
	function isArrowP     ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                                        ) pos=GetComment('',cad,pos,{})[1];
			else if ( isSpecial  (cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.' ) break;
		}

		if ( cad[pos]!='=' ) return false;
		for ( pos++; pos<len && isSpacesTabs(cad,pos); pos++ );

		if ( cad[pos]=='(' ) {
			for ( let key=-1; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++;
				else if ( cad[pos]==')' && key--==0 ){pos++; break}
				else if ( isCommentJS(cad,pos)      ) pos=GetComment('',cad,pos,{})[1];
				else if ( isStringJS (cad,pos)      ) pos=GetString ('',cad,pos,{})[1];
			}
		}
		else for ( ;pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || isSpacesTabs(cad,pos) || cad[pos]=='_'); pos++ );

		if ( cad[pos]!='=' || cad[pos+1]!='>' ) return false;

		pos+=2;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		return cad[pos]=='{';
	 }
	function isArrowN     ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		let len=cad.length;

		for ( pos++; pos<len; pos++ ) {
			if      ( isSpaces   (cad,pos) );
			else if ( isCommentJS(cad,pos) ) pos=GetComment('',cad,pos,{})[1];
			else break;
		}

		for ( ;pos<len && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		return cad[pos]=='=' && cad[pos+1]=='>';
	 }
	function isVaIgual    ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		let len=cad.length;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		if ( cad[pos]!='v' || cad[pos+1]!='a' || cad[pos+2]!='.' ) return false;

		for ( ;pos<len && (!isSpecial(cad,pos) || isSpaces(cad,pos) || cad[pos]=='.'); pos++ );

		return cad[pos]=='=';
	 }
	function isConstructor( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='c' &&
			cad[pos+2 ]=='o' &&
			cad[pos+3 ]=='n' &&
			cad[pos+4 ]=='s' &&
			cad[pos+5 ]=='t' &&
			cad[pos+6 ]=='r' &&
			cad[pos+7 ]=='u' &&
			cad[pos+8 ]=='c' &&
			cad[pos+9 ]=='t' &&
			cad[pos+10]=='o' &&
			cad[pos+11]=='r' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDestructor ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='s' &&
			cad[pos+4 ]=='t' &&
			cad[pos+5 ]=='r' &&
			cad[pos+6 ]=='u' &&
			cad[pos+7 ]=='c' &&
			cad[pos+8 ]=='t' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='r' &&
			isSpecial(cad,pos+11) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefinition ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='f' &&
			cad[pos+4 ]=='i' &&
			cad[pos+5 ]=='n' &&
			cad[pos+6 ]=='i' &&
			cad[pos+7 ]=='t' &&
			cad[pos+8 ]=='i' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='n' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefif      ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='d' &&
			cad[pos+2]=='e' &&
			cad[pos+3]=='f' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			cad[pos+6]==' '
		);
	 }
	function isEndif      ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='n' &&
			cad[pos+3]=='d' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			(isSpecial(cad,pos+6)||pos+6>=cad.length)
		);
	 }
	function isThis       ( cad, pos ) {
		return(
			cad[pos  ]=='t' &&
			cad[pos+1]=='h' &&
			cad[pos+2]=='i' &&
			cad[pos+3]=='s' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isVarResJSON ( cad, pos ) {
		return (
			cad[pos]=='-' &&
			(
				(cad[pos+1]=='P' && cad[pos+2]=='r' && cad[pos+3]=='o' && cad[pos+4]=='j' && cad[pos+5]=='e' && cad[pos+6]=='c' && cad[pos+7]=='t') ||
				(cad[pos+1]=='V' && cad[pos+2]=='a' && cad[pos+3]=='r' && cad[pos+4]=='i' && cad[pos+5]=='a' && cad[pos+6]=='b' && cad[pos+7]=='l' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='S' && cad[pos+2]=='e' && cad[pos+3]=='r' && cad[pos+4]=='v' && cad[pos+5]=='e' && cad[pos+6]=='r') ||
				(cad[pos+1]=='P' && cad[pos+2]=='a' && cad[pos+3]=='t' && cad[pos+4]=='h' && cad[pos+5]=='s') ||
				(cad[pos+1]=='C' && cad[pos+2]=='o' && cad[pos+3]=='m' && cad[pos+4]=='p' && cad[pos+5]=='i' && cad[pos+6]=='l' && cad[pos+7]=='e' && cad[pos+8]=='r') ||
				(cad[pos+1]=='R' && cad[pos+2]=='e' && cad[pos+3]=='s' && cad[pos+4]=='o' && cad[pos+5]=='u' && cad[pos+6]=='r' && cad[pos+7]=='c' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='A' && cad[pos+2]=='p' && cad[pos+3]=='i')
			) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 };

	/*Funciones - Proc*/
	function GetProcFunction( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for ( res+=cad[pos++]; pos<len; pos++ ) {
			if      ( cad[pos]=='{'     ) break;
			// else if ( isSpaces(cad,pos) )[res,pos]=GetSpacesJS(res,cad,pos,{zip:true});
			else                          res+=cad[pos];
		}

		def        =par.defines;
		par.defines={};
		[ret,pos]  =sCompileBJ('',cad,pos,par);
		par.defines=def;

		return[res+ret+'\n',pos];
	 }
	function GetProcArrowN  ( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for ( ;pos<len && (cad[pos]!='=' || cad[pos+1]!='>'); res+=cad[pos++] );

		res+=cad[pos++];//=
		res+=cad[pos++];//>

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos]=GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+=cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+=cad[pos];
			else if ( cad[pos]=='['             ) ret+=cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+=cad[pos];
			else if ( cad[pos]=='{'             ) ret+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+=cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+=cad[pos];
				break;
			}
			else ret+=cad[pos];
		}

		def        =par.defines;
		par.defines={};
		ret        =sCompileBJ(-1,ret,par);
		par.defines=def;

		return[res+ret+'\n',pos];
	 }
	function GetVaIgual     ( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for ( ;pos<len && cad[pos]!='='; res+=cad[pos++] );

		res+=cad[pos++];

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos]=GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos]=GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos]=GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+=cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+=cad[pos];
			else if ( cad[pos]=='['             ) ret+=cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+=cad[pos];
			else if ( cad[pos]=='{'             ) ret+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+=cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+=cad[pos];
				break;
			}
			else ret+=cad[pos];
		}

		return[res+ret+(ret[ret.length-1]!=';'?';':'')+'\n',pos];
	 }
	function GetStyle       ( res, cad, pos, par ) {
		let ret       = '';
		let zip       = par.zip;
		par.zip       = !par.noZipDebug;
		[ret,pos]     = sCompileBC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip       = zip;
		par.hashStyle = Hash(ret);
		par.codeStyle = ret;

		// return[res+`this.style   = ( _style=''           ) => fs.style( \`${ret} \${_style}\` );`, pos];
		return[res+`
        	this.style   = () => {
        		fs.style();

        		Eval({ id:'style_${par.bjClassName}', data:\`${ret}\`, type:VALUE_CSS });

        		return gthis;
        	 }`.replace(/^( *)/gm, '')
		, pos];
	 }
	function GetBC          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = sCompileBC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetView        ( res, cad, pos, par ) {
		let ret      = '';
		let zip      = par.zip;
		par.zip      = !par.noZipDebug;
		[ret,pos]    = sCompileBH('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip      = zip;
		par.yaView   = true;
		par.hashView = Hash(ret);
		par.codeView = ret;

		return[res+`
        	this.html    = ( _html='', _class='' ) => fs.html ( _html + \`${ret}\`, '${par.bjClassName} '+_class );
        	this.idsView = (                     ) => {
        		if ( fs.idsView ) fs.idsView();

        		${par.idsView.sReduce((r,v,k)=>r+`\n\t\tco[\`${k}\`]=gthis.fin(\`#\${gthis.id}-${k}\`);`, '')}

        		return this;
        	 };
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetBH          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = sCompileBH('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetMD          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = sCompileMD('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetConstructor ( res, cad, pos, par ) {
		let ret='', len=cad.length;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def              =par.defines;
		par.defines      ={};
		[ret,pos]        =sCompileBJ('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines      =def;
		par.yaConstructor=true;

		return[res+`
        	this.constructor = ( _props ) => {
        		fs.constructor( Object.assign({}, _props, {noexec:1}) );
        		this.idsView();

        		if ( !_props.noexec ) {
        			this.exe(_props).tri('onConstructor', this);
        		}

        		${ret}

        		return this;
        	 }`.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetDestructor  ( res, cad, pos, par ) {
		let ret='', len=cad.length;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def             =par.defines;
		par.defines     ={};
		[ret,pos]       =sCompileBJ('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines     =def;
		par.yaDestructor=true;

		return[res+`
        	this.destructor  = (        ) => {
        		for( let x in co )
        			if( tipode(co[x])==VALUE_OBJECT && typeof co[x].destructor==VALUE_FUNCTION )
        				co[x].destructor();

        		fs.destructor();
        	 };
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetDefinition  ( res, cad, pos, par ) {
		let ret='', len=cad.length;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		let def    =par.defines;
		par.defines={};
		[ret,pos]  =sCompileBJ('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines=def;

		eval(`def={${ret}}`);

		par.definition=def;

		return[res,pos];
	 }
	function GetDefif       ( res, cad, pos, par ) {
		let nco='', len=cad.length;

		for (      ; pos<len && !isSpaces(cad,pos); pos++           );
		for ( pos++; pos<len && !isSpaces(cad,pos); nco+=cad[pos++] );

		if ( par.environment===nco ) {
			for( nco=''; pos<len && !isEndif(cad,pos); nco+=cad[pos++] );

			nco = sCompileBJ(-1, nco, par);
		}
		else nco='';

		for ( ;pos<len && !isEndif(cad,pos); pos++ );

		pos+=6;
		nco+=(cad[pos]||'');

		return[res+nco,pos];
	 }
	function GetThis        ( res, cad, pos, par ) {
		let len=cad.length;

		for (;pos<len && !isSpecial(cad,pos); pos++);

		return[res+'__x__',--pos];
	 }
	function GetVarResJSON  ( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for (pos++; pos<len && !isSpecial(cad,pos); ret+=cad[pos++]);

		res+=ret;

		return[res,--pos];
	 };

	/*Funciones - GET*/
	function GetClassName  ( cad, pos, par      ) {
		let len=cad.length, res='';

		for ( pos+=5; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		par.sClass = '.'+res;

		return res;
	 }
	function GetClassParent( cad, pos, par      ) {
		let len=cad.length, res='';

		for ( ;pos<len && cad[pos]!=':' && cad[pos]!='{'; pos++ );
		if ( cad[pos]!=':' ) return'';
		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		return res;
	 }
	function GetClassBody  ( cad, pos, par      ) {
		let len=cad.length, res='', con='', fun='', vas='', key=0;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		// Proc
		for ( pos++; pos<len; pos++ ) {
			if      ( cad[pos]=='{'                                 ) res+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0                     ) break;
			else if ( isRInclude   (cad,pos)                        )[res,pos]=sCompileINC   (res,cad,pos,par);
			else if ( isCommentJS  (cad,pos)                        )[res,pos]=GetComment    (res,cad,pos,par);
			else if ( isStringJS   (cad,pos)                        )[res,pos]=GetString     (res,cad,pos,par);
			else if ( isStyle      (cad,pos)                        )[con,pos]=GetStyle      (con,cad,pos,par);
			else if ( isView       (cad,pos)                        )[con,pos]=GetView       (con,cad,pos,par);
			else if ( isConstructor(cad,pos)                        )[con,pos]=GetConstructor(con,cad,pos,par);
			else if ( isDestructor (cad,pos)                        )[con,pos]=GetDestructor (con,cad,pos,par);
			else if ( isDefinition (cad,pos)                        )[con,pos]=GetDefinition (con,cad,pos,par);
			else if ( isBH         (cad,pos)                        )[res,pos]=GetBH         (res,cad,pos,par);
			else if ( isBC         (cad,pos)                        )[res,pos]=GetBC         (res,cad,pos,par);
			else if ( isMD         (cad,pos)                        )[res,pos]=GetMD         (res,cad,pos,par);
			else if ( isDefine     (cad,pos) || isGDefine (cad,pos) )[res,pos]=sCompileDEF   (res,cad,pos,par);
			else if ( isFunctionA  (cad,pos) || isFunction(cad,pos) ) res+=cad[pos++], [fun,pos]=GetProcFunction(fun,cad,pos,par);
			else if ( isArrowP     (cad,pos)                        ) res+=cad[pos++], [fun,pos]=GetProcFunction(fun,cad,pos,par);
			else if ( isArrowN     (cad,pos)                        ) res+=cad[pos++], [fun,pos]=GetProcArrowN  (fun,cad,pos,par);
			else if ( isVaIgual    (cad,pos)                        ) res+=cad[pos++], [vas,pos]=GetVaIgual     (vas,cad,pos,par);
			else                                                      res+=cad[pos];
		}

		if ( !par.yaView        ) con=GetView       (con,'',0,{bjClassName:par.bjClassName})[0];
		if ( !par.yaConstructor ) con=GetConstructor(con,'',0,{})[0];
		if ( !par.yaDestructor  ) con=GetDestructor (con,'',0,{})[0];

		delete par.yaView;

		return[`
        	/*Herencia*/
        	this.__props__ =props;
        	this.${par.bjClassParent}=${par.bjClassParent};
        	this.${par.bjClassParent}({ noexec:1 });

        	/*Variables*/
        	const gthis = this;
        	const fs    = this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
        	const op    = props;
        	const va    = {};
        	const co    = {};

        	/*Variables - va*/
            ${vas}

        	/*Creacion*/
        	${con}

        	/*Funciones*/
        	${fun}

        	/*Inicio*/
        	this.pClass = this.sClass;
        	this.sClass = '${par.bjClassName}';
        	if ( !props.noexec )
        	this.constructor(props);

        	/*Body*/
        	${res}
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetClass      ( res, cad, pos, par ) {
		let len  =cad.length, bod;
		let nam  =GetClassName  (cad,pos,par);
		let pad  =GetClassParent(cad,pos,par); pad=='' && (pad='sDomVisual');
		[bod,pos]=GetClassBody  ( cad,pos,Object.assign(par, {bjClassName:nam, bjClassParent:pad}) );

		return[
			`(function(){`+
				`\n${res}`+
				`\nwindow.${nam} = function( props={} ) {${bod}};window.${nam}.prototype=new ${pad}({ noexec:1 });`+
			`\n})();`
		, pos];
	 }

	/*Inicio*/
	function Compall( cad, par={}           ) {
		let len=cad.length, res='', pos=0;

		for ( ;pos<len; pos++ ) {
			if      ( isRInclude  (cad,pos) )[res,pos]=sCompileINC  (res,cad,pos,par);
			else if ( isCommentJS (cad,pos) )[res,pos]=GetComment   (res,cad,pos,par);
			else if ( isStringJS  (cad,pos) )[res,pos]=GetString    (res,cad,pos,par);
			else if ( isClass     (cad,pos) )[res,pos]=GetClass     (res,cad,pos,par);
			else if ( isDefif     (cad,pos) )[res,pos]=GetDefif     (res,cad,pos,par);
			else if ( isBH        (cad,pos) )[res,pos]=GetBH        (res,cad,pos,par);
			else if ( isBC        (cad,pos) )[res,pos]=GetBC        (res,cad,pos,par);
			else if ( isVarResJSON(cad,pos) )[res,pos]=GetVarResJSON(res,cad,pos,par);
			else if ( cad[pos]=='{'         )[res,pos]=sCompileBJ   (res,cad,pos,Object.assign(par,{includeTemp:0}));
			else                              res+=cad[pos];
		}

		res           =sCompileNEO(res,par);
		res           =sCompileJS (-1,res,par);
		par.hashScript=Hash(res);

		return res;
	 }
	function Compile( cad, par={}           ) {
		return new sProm((run,err)=>{
			sCompileDEF(cad,par)
			.run( x=>sCompileINC(x,par) )
			.run( x=>{par.data=Compall(x,par); run(par)} )
			.err(err);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='', com=false, reu=false;
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let ite=par.includeTemp                               ; delete par.includeTemp;
		let rep=par.replaceThis                               ; delete par.replaceThis;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos]=GetComment (ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos]=GetString  (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos]=GetRegExp  (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+=cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else if ( !com && cad[pos]==';'     ) ret+=cad[pos], com=true;
			else if ( !reu && isReturn(cad,pos) ) ret+=cad[pos], reu=true;
			else if ( isView (cad,pos)          )[ret,pos]=GetView (ret,cad,pos,par);
			else if ( isBH   (cad,pos)          )[ret,pos]=GetBH   (ret,cad,pos,par);
			else if ( isBC   (cad,pos)          )[ret,pos]=GetBC   (ret,cad,pos,par);
			else if ( isStyle(cad,pos)          )[ret,pos]=GetStyle(ret,cad,pos,par);
			else if ( isThis (cad,pos) && rep   )[ret,pos]=GetThis (ret,cad,pos,par);
			else                                  ret+=cad[pos];
		}

		ret=Compall(ret,par);

		if ( ite ) {
			if      ( !reu && com ) ret = "(function(){return "+ ret +"})()";
			else if (  reu        ) ret = "(function(){"       + ret +"})()";

			ret='${'+ ret +'}';
		}
		else if ( inc ) ret=ini + ret + cad[pos];

		return[res+ret, pos];
	 }

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileDEF  = function( result, data, position, params ) {
	/*Variables*/
	const TYPE_NORM=1;
	const TYPE_FUNC=2;

	/*Is*/
	function isReplace( cad, pos, def, ind ) {
		return (isSpecial(cad,pos) || pos==0) && def && def[ind] && def[ind].name!='' && cad[pos+1]==def[ind].name[0];
	 }

	/*Funciones - Define*/
	function GetDefineType ( cad, pos, par ) {
		let len=cad.length;

		for ( pos+=7; pos<len && cad[pos]!='(' && cad[pos]!='=' && cad[pos]!='\n'; pos++ );

		return cad[pos]=='(' ? TYPE_FUNC : TYPE_NORM;
	 }
	function GetDefineName ( cad, pos, par ) {
		let len=cad.length, res='';

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len &&  isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\n' || cad[pos]=='=' || cad[pos]=='(' ) break;
			else if ( isSpacesTabs(cad,pos)                            );
			else                                                         res+=cad[pos];
		}

		return res;
	 }
	function GetDefineAtts ( cad, pos, par ) {
		let len=cad.length, res=[], tem='';

		for ( ;pos<len && cad[pos]!='(' && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		if ( cad[pos]=='(' ) {
			let key=0;

			for ( pos++; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++, tem+=cad[pos];
				else if ( cad[pos]==')' && key--==0 ) break;
				else if ( cad[pos]==','             ) tem!=''&&res.push(tem)&&(tem='');
				else                                  tem+=cad[pos];
			}

			if ( tem!='' ) res.push(tem);
		}

		return res;
	 }
	function GetDefineValue( cad, pos, par ) {
		let len=cad.length, res='';

		for ( ;pos<len && cad[pos]!='=' && cad[pos]!='\n'; pos++ );

		if ( cad[pos]=='=' ) {
			for ( pos++; isSpacesTabs(cad,pos); pos++ );
			for ( ;pos<len; pos++ ) {
				if      ( cad[pos]=='\n' ) break;
				else if ( cad[pos]=='\\' ) res+=cad[++pos];
				else                       res+=cad[pos];
			}
		}

		return res;
	 }
	function GetDefineFin  ( cad, pos, par ) {
		let len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\n' ) break;
			else if ( cad[pos]=='\\' ) pos++;
		}

		return pos;
	 }
	function GetDefine     ( cad, par      ) {
		let len=cad.length, pos=0, res='', def=[], lin, glo;

		for ( ;pos<len; pos++ ) {
			if ( isDefine(cad,pos) || (glo=isGDefine(cad,pos)) ) {
				if ( glo ) pos++;

				(glo ? global.__defines__ : def).push({
					type : GetDefineType (cad,pos,par),
					name : GetDefineName (cad,pos,par),
					atts : GetDefineAtts (cad,pos,par),
					value: GetDefineValue(cad,pos,par),
				});

				pos =GetDefineFin(cad,pos,par);
				glo =false;
				res+='\n';
			}
			else if ( isCommentJS(cad,pos) )[res,pos]=GetComment(res,cad,pos,par);
			else res+=cad[pos];
		}

		par.defines=[].concat(global.__defines__, def);

		return res;
	 }

	/*Funciones - Replace*/
	function GetReplaceFunc( res, cad, pos, def ) {
		let len=cad.length, psa=pos, tem='';

		for ( res+=cad[pos++]; pos<len && !isSpecial(cad,pos); tem+=cad[pos++] );

		if ( cad[pos]=='(' && def.name===tem ) {
			let val=def.value, led=def.atts.length, pod=0, reg, att=GetDefineAtts(cad,pos), key=0;

			for ( ;pod<led; pod++ )
				val=val.replace(new RegExp('\\b'+def.atts[pod]+'\\b','g'), att[pod]);

			for ( pos++; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++;
				else if ( cad[pos]==')' && key--==0 ) break;
			}

			return[res+val, pos, true];
		}

		return[res, psa, false];
	 }
	function GetReplaceNorm( res, cad, pos, def ) {
		let tem='', len=cad.length, psa=pos++, rep=false;

		for ( ;pos<len && !isSpecial(cad,pos); tem+=cad[pos++] );

		if ( def.name===tem ) {
			res+= cad[psa] + def.value;
			pos--;
			rep = true;
		}
		else {
			pos = psa;
			res+= cad[pos];
		}

		return[res,pos,rep];
	 }
	function GetReplace    ( cad, par           ) {
		if ( !par.defines || !par.defines.length ) return cad;

		let len, pos, res='', rep=true, def=par.defines;

		for ( let cou=0; rep && cou<100; cou++ ) {
			rep=false;

			for ( let i=def.length; i--; ) {
				len=cad.length, pos=0, res='';

				for ( ;pos<len; pos++ ) {
					if      ( isStringJS (cad,pos)       )[res,pos]=GetString (res,cad,pos,par);
					else if ( isCommentJS(cad,pos)       )[res,pos]=GetComment(res,cad,pos,par);
					else if ( isReplace  (cad,pos,def,i) ){
						if ( def[i].type==TYPE_FUNC )[res,pos,rep]=GetReplaceFunc(res,cad,pos,def[i]);
						else                         [res,pos,rep]=GetReplaceNorm(res,cad,pos,def[i]);
					}
					else res+= cad[pos];
				}

				cad=res;
			}
		}

		return res;
	 }

	/*Inicio*/
	function Compall( cad, par={}        ) {
		if ( !global.__defines__ ) global.__defines__=[];

		return GetReplace(GetDefine(cad,par),par);
	 }
	function Compile( cad, par={}        ) {
		return new sProm( (run,err)=>run( Compall(cad,par) ) );
	 }
	function Escape ( res, cad, pos, par ) {
		let len=cad.length, ret='';

		for ( ;pos<len && cad[pos]!='=' && cad[pos]!='\n'; ret+=cad[pos++] );

		if ( cad[pos]=='=' ) {
			for ( ;pos<len; pos++ ) {
				if      ( cad[pos]=='\n'                     ){ret+=cad[pos]; break}
				else if ( cad[pos]=='\\' && cad[pos+1]=='\n' ) ret+=cad[pos], ret+=cad[++pos];
				else                                           ret+=cad[pos];
			}
		}

		return[res+ret, pos];
	 }

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return'';
 };
global.sCompileNEO  = function( data, params                   ) {
	/*Variables*/
	function isNeo( cad, pos ) {
		return(
			cad[pos  ]=='n' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='o' &&
			isSpaces(cad,pos+3) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }

	/*Funciones*/
	function GetName  ( res, cad, pos, par ) {
		let len=cad.length, aco=true;

		for ( pos+=3; pos<len && isSpaces(cad,pos); pos++ );

		for ( ;pos<len; pos++ ) {
			if      ( isSpaces   (cad,pos) );
			else if ( isCommentJS(cad,pos) ) pos=GetComment('',cad,pos,par)[1];
			else if ( cad[pos]=='['        ) {
				aco=false;

				for ( pos++; pos<len; pos++ ) {
					if ( cad[pos]==']' ) break;
					else                 res+=cad[pos];
				}
			}
			else if ( isSpecial  (cad,pos) ) break;
			else                             res+=cad[pos];
		}

		return[aco ? "'"+res+"'" : res,pos];
	 }
	function GetParent( res, cad, pos, par ) {
		if ( cad[pos]=='(' ) {
			let len=cad.length, num=0, spo=pos;

			for (;pos<len && isSpecial(cad,pos) && !isStringJS(cad,pos); pos++);
			if ( !isStringJS(cad,pos) ) {
				for (;pos<len; pos++) {
					if      ( isCommentJS(cad,pos)                            ) pos=GetComment('',cad,pos,{})[1];
					else if ( cad[pos]==')' || cad[pos]==',' || cad[pos]==':' ) break;
				}
				if ( cad[pos]==':' ) return[res,spo];
			}

			pos=spo;

			for ( pos++; pos<len; pos++ ) {
				if      ( isSpaces   (cad,pos)            );
				else if ( isStringJS (cad,pos)            )[res,pos]=GetString(res,cad,pos,par);
				else if ( isCommentJS(cad,pos)            ) pos     =GetComment('',cad,pos,{} )[1];
				else if ( cad[pos]=='/'                   )[res,pos]=GetRegExp(res,cad,pos);
				else if ( cad[pos]=='('                   ) res+=cad[pos], num++;
				else if ( cad[pos]==')' && num            ) res+=cad[pos], num--;
				else if ( cad[pos]==')' || cad[pos]==','  ) break;
				else                                        res+=cad[pos];
			}
		}

		return[res,pos];
	 }
	function GetParams( res, cad, pos, par ) {
		if ( cad[pos]==',' || cad[pos]=='(' ) {
			let len=cad.length;

			for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

			if ( cad[pos]!=')' ) {
				for ( let num=0; pos<len; pos++ ) {
					if      ( cad[pos]=='('             ) res+=cad[pos], num++;
					else if ( cad[pos]==')' && num--==0 ) break;
					else if ( isSpaces   (cad,pos)      );
					else if ( isStringJS (cad,pos)      )[res,pos]=GetString (res,cad,pos,par);
					else if ( isCommentJS(cad,pos)      ) pos     =GetComment('' ,cad,pos,{} )[1];
					else if ( cad[pos]=='/'             )[res,pos]=GetRegExp (res,cad,pos,par);
					else                                  res+=cad[pos];
				}
			}
		}

		return[res,pos];
	 }
	function GetIgual ( res, cad, pos, par ) {
		let len=cad.length;

		for ( pos+=2; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos) || cad[pos]=='.'; !isSpaces(cad,pos) ? res+=cad[pos++] : pos++ );

		return[res,pos];
	 }
	function GetNeo   ( cad, par           ) {
		let res='', pos=0, len=cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isRInclude (cad,pos) )[res,pos]=sCompileINC(res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )[res,pos]=GetComment (res,cad,pos,par);
			else if ( isStringJS (cad,pos) )[res,pos]=GetString  (res,cad,pos,par);
			else if ( cad[pos]=='/'        )[res,pos]=GetRegExp  (res,cad,pos,par);
			else if ( isNeo(cad,pos) ) {
				let cln='', pan='', pam='', bod='', igu='';

				[cln,pos]=GetName  ('',cad,pos,par);
				[pan,pos]=GetParent('',cad,pos,par);
				[pam,pos]=GetParams('',cad,pos,par);

				/*Finish*/
				if      ( cad[pos]=='-' && cad[pos+1]=='>' )[igu,pos]=GetIgual('',cad,pos,par);
				else if ( cad[pos]==')'                    ) {
					let len=cad.length, spo=++pos;

					for ( ;pos<len && isSpaces(cad,pos); pos++ );

					if      ( cad[pos]=='-' && cad[pos+1]=='>' )[igu,pos]=GetIgual  ('',cad,pos,par);
					else if ( cad[pos]=='{'                    )[bod,pos]=sCompileBJ('',cad,pos,Object.assign(par,{includeKeys:0,replaceThis:1})), pos++;
					else if ( cad[pos]!=';'                    ) pos=spo;
				}
				else if ( cad[pos]=='{' )[bod,pos]=sCompileBJ('',cad,pos,Object.assign(par,{includeKeys:0,replaceThis:1})), pos++;

				// igu=='' && bod=='' && pan[0]=='c' && pan[1]=='o' && pan[2]=='.' && (igu=pan);

				res+=
				'Exec('+ cln +', {select:'+ (pan||'\'body\'') +','+ pam +'}).all(__x__=>{'+
					(igu? igu+'=__x__;' : bod) +
				'})'+
				(cad[pos]||'');
			 }
			else res+=cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	function Compall( cad, par={} ) {
		return GetNeo(cad,par);
	 }
	function Compile( cad, par={} ) {
		return new sProm( (run,err)=>run( Compall(cad,par) ) );
	 }

	 return Compall(data,params);
 };
global.sCompileINC  = function( result, data, position, params ) {
	/*Is*/
	function isIncludeFile ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		let len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='f' &&
			cad[pos+1]=='i' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='e' &&
			isSpecial(cad,pos+4)
		);
	 }
	function isIncludeUrl  ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		let len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return cad[pos]=='u' && cad[pos+1]=='r' && cad[pos+2]=='l' && isSpecial(cad,pos+3);
	 }
	function isIncludeClass( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		let len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpecial(cad,pos+5)
		);
	 }

	/*Funciones - GetInclude*/
	function GetIncludeFile ( res, cad, pos, par ) {
		return new sProm((run,err)=>{
			let len=cad.length, ret='', exe=false;

			for (;pos<len && cad[pos]!='"' && cad[pos]!="'"; pos++);

			[ret,pos]=GetString('',cad,pos,par,false,false);

			for (;pos<len && cad[pos]!='\n' && cad[pos]!='}'; pos++);
			if ( cad[pos]=='}' ) pos--;
			if ( api.IsFile(ret) ) {
				ret            = api.GetParsePath(ret);
				ret.lower_path = ret.path.toLowerCase();

				if ( !par.included[ret.lower_path] ) {
					exe                          = true;
					par.included[ret.lower_path] = 1;

					sCompileALL(ret.path, par)
					.run(x=>{
						run(res
							// +`/*INCLUDE FILE>>>${ret.path}*/\n`
							+ x.data
							// +`/*END INCLUDE*/\n`
						, pos)
					})
					.err(e=> run(res+`/*Include Error: ${e}*/`, pos) );
				}
				else ret='';
			}
			else {
				!par.zip && par.environment!='production' && (ret="/*El archivo '"+ ret +"' no existe y/o no se puede incluir*/")
				console.info("El archivo '"+ ret +"' no existe y/o no se puede incluir");
			}

			if ( !exe )
				run(res+ret,pos);
		});
	 }
	function GetIncludeUrl  ( res, cad, pos, par ) {
		return new sProm((run,err)=>{
			let len=cad.length, ret='', exe=false;

			for (;pos<len && !isStringCSS(cad,pos); pos++);

			[ret,pos]=GetString('',cad,pos,par, false, false);

			for (;pos<len && cad[pos]!='\n'; pos++);

			if ( !par.included[ret.toLowerCase()] ) {
				exe                            =true;
				par.included[ret.toLowerCase()]=1;

				api.Ajax(ret)
				.run(x=>
					run(
						res
						// +`/*INCLUDE URL >>> ${ret}*/\n`
						+ x
						// +`\n/*END INCLUDE*/`
					, pos)
				)
				.err(e=> run(res+`/*Include Error url: ${ret}, ${e}*/`, pos) );
			}

			if ( !exe )
				run(res+ret,pos);
		});
	 }
	function GetIncludeClass( res, cad, pos, par ) {
		return new sProm((run, err)=>{
			let len=cad.length;

			for (;pos<len && cad[pos]!='(' && cad[pos]!='\n'; pos++);

			if ( cad[pos]=='\n' ) run(res,pos+1);

			let mod='', ver='', ser='';
			let aja=url=>{
				api.Ajax(url)
				.run(x=>
					run(res
						// +`/*INCLUDE CLASS >>> ${url}*/\n`
						+ x
						// +`\n/*END INCLUDE*/`
					, pos)
				)
				.err(e=>run(res+`/*Error Include Class: ${url}, ${e}*/`, pos))
			 };

			for (pos++; pos<len; pos++) {
				if      ( cad[pos]==','     ) break;
				else if ( isLetter(cad,pos) ) mod+=cad[pos];
			}

			for (pos++; pos<len && isSpacesTabs(cad,pos); pos++);

			if ( isStringCSS(cad,pos) ) [ser,pos]=GetString('',cad,pos,par,0,0);
			else {
				for (;pos<len; pos++) {
					if      ( cad[pos]==','                                       ) break;
					else if ( isNumber(cad,pos) || cad[pos]=='.' || cad[pos]=='+' ) ver+=cad[pos];
				}
			}

			for (pos++; pos<len && isSpacesTabs(cad,pos); pos++);

			if ( isStringCSS(cad,pos) )[ser,pos]=GetString('',cad,pos,par,0,0);

			for (;pos<len && cad[pos]!='\n'; pos++);

			ser=api.ParseURL(ser);

			if ( ver=='' ) aja(`${ser.protocol}://${ser.host}:${ser.port}/modules/${mod}.bj`);
			else           aja(`${ser.protocol}://${ser.host}:${ser.port}/modules/${mod}-${ver}.bj`);
		});
	 }
	function GetInclude     ( res, cad, pos, par ) {
		return new sProm((run,err)=>{
			let len=cad.length, inc;
			let fin=func=>{
				inc=1;

				func(res,cad,pos,par)
				.err(err)
				.run((r,p)=>{
					if ( p<len ) GetInclude(r,cad,p,par).run(run);
					else         run(r);
				});
			 };

			for ( ;pos<len; pos++ ) {
				if      ( isIncludeFile (cad,pos) ){fin(GetIncludeFile ); break}
				else if ( isIncludeUrl  (cad,pos) ){fin(GetIncludeUrl  ); break}
				else if ( isIncludeClass(cad,pos) ){fin(GetIncludeClass); break}
				else if ( isCommentJS   (cad,pos) )[res,pos]=GetComment(res,cad,pos,par);
				else if ( isStringJS    (cad,pos) )[res,pos]=GetString (res,cad,pos,par);
				else                                res+=cad[pos];
			}

			if ( !inc ) run(res);
		});
	 };

	/*Inicio*/
	function Compall( cad, par={}           ) {return '';};
	function Compile( cad, par={}           ) {
		!par.included && (par.included={});

		return new sProm( (run,err)=>{
			GetInclude('',cad,0,par)
			.run(run)
			.err(err);
		});
	 }
	function Escape ( res, cad, pos, par={} ) {
		let len=cad.length, ret='', ini='', fin='';
		let inc=par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let ite=par.includeTemp                               ; delete par.includeTemp;

		for ( ;pos<len && cad[pos]!='\n'; ini+=cad[pos++] );

		ini+=cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if ( isEndRInclude(cad,pos) ) break;
			else                          ret+=cad[pos];
		}

		for ( let lee=pos+15; pos<lee; fin+=cad[pos++] );

		if ( inc ) ret=ini + ret + fin;

		return[res+ret, pos];
	 };

	if      ( typeof result==VALUE_NUMBER && result===-1                ) return Compall(data,position);
	else if ( typeof result==VALUE_STRING && tipode(data)==VALUE_OBJECT ) return Compile(result,data);
	else if ( typeof result==VALUE_STRING && typeof data ==VALUE_STRING ) return Escape (result,data,position,params);

	return Compile(data,params);
 };
global.sCompileALL  = function( path, params                   ) {
	return new sProm((run,err)=>{
		let file={};

		if ( api.IsFile(path) ) {
			file            = api.GetParsePath(path);
			file.ext        = file.ext .toLowerCase();
			file.lower_path = file.path.toLowerCase();
			file.mime       = Mime(file.ext);
			file            = Object.assign({}, params, file);
			const frun      = fil=>{fil.original_data=fil.data; fil.hash=Hash(fil.data); run(fil)}

			switch (file.ext) {
				case '.svg' :
				case '.htm' :
				case '.html': sCompileHTML(api.Read(file.path),file).run(frun).err(err); break;
				case '.md'  : sCompileMD  (api.Read(file.path),file).run(frun).err(err); break;
				case '.css' : sCompileCSS (api.Read(file.path),file).run(frun).err(err); break;
				case '.json': sCompileJSON(api.Read(file.path),file).run(frun).err(err); break;
				case '.js'  : sCompileJS  (api.Read(file.path),file).run(frun).err(err); break;
				case '.bc'  : sCompileBC  (api.Read(file.path),file).run(frun).err(err); break;
				case '.bh'  : sCompileBH  (api.Read(file.path),file).run(frun).err(err); break;
				case '.bj'  : sCompileBJ  (api.Read(file.path),file).run(frun).err(err); break;
				default     : file.data=api.Read(file.path,false); frun(file);
			}
		}
		else run(file);
	});
 };

global.sProm         = function( parent, func  ) {
	this.sClass = 'sProm';
	this.fwait  = [];
	this.funcs  = { run:Run, err:Err, all:All };

	if ( typeof parent==VALUE_FUNCTION ) {
		func   = parent;
		parent = undefined;
	}
	else {
		for ( let x in parent ) {
			if ( typeof parent[x]==VALUE_FUNCTION ) this[x]=(...params)=>ExecFunc(x, params);
		}
	}

	/*Variables*/
	const gthis = this;
	const va    = { run:[], err:null, all:null };

	/*Metodos*/
	gthis.run=fnew=>{va.run.push(fnew); return gthis}
	gthis.err=fnew=>{va.err=fnew;       return gthis}
	gthis.all=fnew=>{va.all=fnew;       return gthis}

	/*Funciones*/
	function Run( ...params ) {
		while ( va.run.length ) {
			if ( typeof va.run[0]==VALUE_FUNCTION ) {
				let res = va.run[0](...params);

				if ( res && res.sClass && res.sClass=='sProm' ) {
					res.err(va.err);
					va.run.splice(0, 1);
					for(let x=0;x<va.run.length;x++) res.run(va.run[x]);
					break;
				}
			}
			else if ( typeof va.run[0]==VALUE_OBJECT && parent ) {
				if ( typeof va.run[0].func==VALUE_FUNCTION || typeof parent[va.run[0].func]==VALUE_FUNCTION ) {
					let res;

					if ( typeof va.run[0].func==VALUE_FUNCTION ) res = va.run[0].func(va.run[0].params);
					else                                         res = parent[va.run[0].func](...va.run[0].params);

					if ( res!=undefined ) {
						if ( res.sClass && res.sClass=='sProm' ) {
							res.err(va.err);
							va.run.splice(0, 1);
							for(let x=0;x<va.run.length;x++) res.run(va.run[x]);
							break;
						}
						else {
							for ( let x=va.run.length; x--; ) {
								if      ( typeof va.run[x]     ==VALUE_FUNCTION ) va.run[x]        = { func:va.run[x], params:res };
								else if ( typeof va.run[x].func==VALUE_FUNCTION ) va.run[x].params = res;
							}
						}
					}
				}
			}

			va.run.splice(0, 1);
		}

		if ( typeof va.all==VALUE_FUNCTION )
			va.all(...params);
	 }
	function Err( ...params ) {
		if ( typeof va.err==VALUE_FUNCTION ) va.err(...params);
		if ( typeof va.all==VALUE_FUNCTION ) va.all('error', params);
	 }
	function All( ...params ) {
		if ( typeof va.all==VALUE_FUNCTION )
			va.all(...params);
	 }

	function ExecFunc( func, params ) {
		if ( params[0]==VALUE_ASYNC ){
			params.splice(0, 1);

			return parent[func](...params);
		}
		else {
			gthis.fwait.push(func);
			va.run.push({ func:func, params:params });
		}

		return gthis;
	 }

	/*Inicio*/
	setTimeout(func.bind(gthis, Run, Err, All), 10);
 };
global.sObject       = function( props, noexec ) {
	/*Variables*/
	const co={};
	const va={bcEvents:[]};

	/*Creacion*/
	this.constructor = function( _props, _noexec ) {
		co.events=new (require('events').EventEmitter)();

		if ( !_noexec ) this.exe(_props);
	 };

	/*Metodos*/
	this.eve = function( events, idDelete ) {
		if      ( events===false               ) {
			va.bcEvents.forEach(x=>co.events.removeListener(x.event, x.func));
			va.bcEvents=[];
		 }
		else if ( events===undefined           ) return va.bcEvents;
		else if ( typeof events===VALUE_STRING ) {
			if ( idDelete===false ) {
				for ( let x=va.bcEvents.length; x--; ) {
					if ( va.bcEvents[x].id===events ) {
						co.events.removeListener(va.bcEvents[x].event, va.bcEvents[x].func);
						va.bcEvents.splice(x, 1);
					}
				}
			 }
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				va.bcEvents.push({ id:'', event:events, func:idDelete });
				co.events.on(events, idDelete);
			}
			else return va.bcEvents.reduce((r,v)=>{ x.id===events && r.push(v); return r; },[]);
		 }
		else if ( tipode(events)==VALUE_OBJECT ) {
			if ( idDelete===false ) {
				for ( let x in events ) {
					for ( let y=va.bcEvents.length; y--; ) {
						if ( va.bcEvents[y].event===x && va.bcEvents[y].func===events[x] ) {
							co.events.removeListener(va.bcEvents[y].event, va.bcEvents[y].func);
							va.bcEvents.splice(y, 1);
						}
					}
				}
			 }
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id='';

				for ( let x in events ) {
					if ( x!='id' ) {
						va.bcEvents.push({ id:events.id, event:x, func:events[x] });
						co.events.on(x, events[x]);
					}
				}
			 }
		 }

		return this;
	 };
	this.tri = function( event , ...extra ) {
		typeof event==VALUE_STRING && event!='' && co.events.emit(event, ...extra);

		return this;
	 }
	this.exe = function( method           ) {
		if ( tipode(method)==VALUE_OBJECT ) {
			for ( let x in method ) {
				if ( this[x]!=undefined ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(method[x])==VALUE_ARRAY ) this[x].apply(this, method[x]);
						else                                  this[x](method[x]);
					}
				}
				else {
					this[x]=method[x];

					if ( x[0]==='o' && x[1]==='n' && typeof method[x]===VALUE_FUNCTION )
						this.eve(x, method[x]);
				 }
			}
		}

		return this;
	 }
	this.run = function( func             ) {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 }
	this.err = function( func             ) {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 }
	this.all = function( func             ) {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 }

	/*Inicio*/
	this.sClass='sObject';
	if ( noexec ) return;
	this.constructor(props);
 };
global.sApi          = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(1,1);

	/*Variables*/
	const gt=this;
	const co={};
	const fs=this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});

	/*Creacion*/
	this.constructor = function( _props, _noexec ) {
		fs.constructor(_props, 1);

		/*Asignaciones*/
		co.path   = require('path' );
		co.fs     = require('fs'   );
		co.util   = require('util' );
		co.http   = require('http' );
		co.https  = require('https');
		co.crypto = require('crypto');
		co.child  = require('child_process');

		if ( !_noexec ) this.exe(_props);

		/*Inicio*/
		return this;
	 };

	/*Metodos*/
	this.IsExist    =( path )=>{
		try {
			return co.fs.statSync(path);
		}
		catch(e) {return false;}
	 };
	this.IsFile     =( path )=>{
		try {
			return co.fs.statSync(path).isFile();
		}
		catch(e) {return false;}
	 };
	this.IsDirectory=( path )=>{
		try {
			return co.fs.statSync(path).isDirectory();
		}
		catch(e) { return false;}
	 };

	this.GetName     =( path     )=>co.path.basename(path);
	this.GetDirectory=( path     )=>co.path.dirname(path);
	this.GetExtension=( path     )=>co.path.extname(path);
	this.GetJoinPath =( ...paths )=>co.path.join(...paths);
	this.GetParsePath=( ...paths )=>{
		paths    = paths.map(x=>{x.indexOf('~')>-1 && (x=x.replace(/[~]/g, process.env.HOME)); return x});
		let pat  = paths.length==0 ? '.' : (paths.length==1?paths[0]:co.path.join(...paths));
		let res  = co.path.parse(co.path.resolve(pat));
		res.path = co.path.join(res.dir, res.base);

		return res;
	 };

	this.CreateDirectory=( path )=>{
		if ( !this.IsExist(path) ) {
			this.CreateDirectory(this.GetDirectory(path));
			co.fs.mkdirSync(path);
		}

		return this;
	 };

	this.Read  =( path, isText=true          )=>{
		try {
			var fil = co.fs.readFileSync(path, isText ? 'UTF8' : undefined);
		}
		catch(e) {return null;}

		if ( isText=='lower' ) return fil.toLowerCase();
		if ( isText=='upper' ) return fil.toUpperCase();

		return fil;
	 };
	this.Write =( path, data, options='UTF8' )=>{
		try {
			co.fs.writeFileSync(path, data, options);

			return true;
		}
		catch(e) {return false;}
	 };
	this.Watch =( path, options              )=>{
		return new sProm(gthis, (run, err, all)=>{
			co.fs.watch(path, options, (eventType, filename)=>{
				let pat       = co.path.join(path, filename);
				let obj       = co.path.parse(pat);
				obj.path      = pat;
				obj.type      = gthis.IsFile(pat) ? VALUE_FILE : VALUE_FOLDER;
				obj.eventType = eventType;

				all(obj);
			});
		});

		return gthis;

		/*RECURSIVO PARA LINUX*/
		// return new sProm(gt, (run, err, all)=>{
		// 	if ( tipode(options)==VALUE_OBJECT && options.recursive ) {
		// 		delete options.recursive;

		// 		(function fpat( _path ) {
		// 			let sta=api.IsExist(_path);

		// 			if ( sta && sta.isDirectory() ) {
		// 				api.Travel(_path).forEach(x=>fpat(x.path));
		// 				gt.Watch(_path, options).run(run).err(err).all(all);
		// 			}
		// 		 })(path);
		// 	}
		// 	else {
		// 		co.fs.watch(path, options, (eventType, filename)=>{
		// 			let pat       = co.path.join(path, filename);
		// 			let obj       = co.path.parse(pat);
		// 			obj.path      = pat;
		// 			obj.type      = gt.IsFile(pat) ? VALUE_FILE : VALUE_FOLDER;
		// 			obj.eventType = eventType;

		// 			all(obj);
		// 		});
		// 	}
		// });
	 };
	this.Travel=( path                       )=>{
		let sta=gt.IsExist(path), obj, pat;

		if      ( !sta         ) return [];
		else if ( sta.isFile() ) {
			obj      = co.path.parse(path);
			obj.type = VALUE_FILE;
			obj.path = co.path.join(obj.dir, obj.base);

			return [obj];
		}
		else if ( sta.isDirectory() ) {
			return co.fs.readdirSync(path).map(x=>{
				pat      = co.path.join(path, x);
				obj      = co.path.parse(pat);
				obj.type = gt.IsFile(pat) ? VALUE_FILE : VALUE_FOLDER;
				obj.path = pat;

				return obj;
			});
		}

		return [];
	 };

	this.LogObject=( object )=>console.info( co.util.inspect(object, {depth:null}) );

	this.ParseURL       =( url         )=>{
		if ( typeof url!=VALUE_STRING ) return{};

		let cad=url, len=url.length, pos=0, res={};

		[res.protocol,pos          ]=GetHttp  (cad,pos);
		[res.host    ,pos          ]=GetHost  (cad,pos);
		[res.port    ,pos          ]=GetPort  (cad,pos);
		[res.path    ,pos,res.paths]=GetPath  (cad,pos);
		[res.params  ,pos          ]=GetParams(cad,pos);

		res.protocol=res.protocol||'https';
		res.port    =parseInt(res.port)||(res.protocol=='https'?443:80);

		return res;
	 };
	this.ParseParcialURL=( url         )=>{
		let eur=/\b([\w\n\.\-]+)\b/g;
		let tur=url, par='', mat, dat=[];

		/*Params*/
		if ( tur.indexOf('?')>-1 ) {
			par = tur.substring(tur.indexOf('?')+1, tur.length);
			tur = tur.substring(0, tur.indexOf('?'));
		}

		let tem='', pas=[];
		while ( (mat=eur.exec(par))!==null ) {
			mat.index===eur.lastIndex && eur.lastIndex++;
			tem=='' ? tem=mat[0] : (pas.push({[tem]:mat[0]}), tem='');
		}

		/*URL*/
		while ( (mat=eur.exec(tur))!==null ) {
			mat.index===eur.lastIndex && eur.lastIndex++;
			mat.forEach((match, gindex)=>gindex===1 && dat.push(match));
		}

		return {
			url       :tur,
			ourl      :url,
			path      :dat,
			params    :par,
			paramsPath:pas,
		};
	 };
	this.ParseHeaders   =( headers     )=>{
		let res={};

		headers.sForEach((v,k)=>{
			switch (k) {
				case 'referer': k='url'   ; break;
				case ':status': k='status'; break;
			}

			k=k.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
				if ( +match===0 ) return '';

				return index==0 ? match.toLowerCase() : match.toUpperCase();
			}).replace(/\s+|-/g, '');

			res[k]=v;
		 });

		return res;
	 };
	this.Ajax           =( url, params )=>{
		if      ( params.protocol=='http2' ) return AjaxHTTP2(url, params);
		else                                 return AjaxHTTP (url, params);
	 };
	this.OpenLink       =( url, params )=>{
		let com='', arg=[];
		params =Object.assign({}, params, {wait:false, app:false});

		if ( process.platform==='darwin' ) {
			com='open';

			params.wait && arg.push('-W');
			params.app  && arg.push('-a', params.app);
		}
		else if ( process.platform==='win32' ) {
			com='cmd';
			url=url.replace(/&/g, '^&');

			arg.push('/c', 'start', '""', '/b');

			params.wait && arg.push('/wait');
			params.app  && arg.push(params.app);
		}

		arg.push(url);

		let res=co.child.spawn(com, arg);

		res.unref();

		return res;
	 };

	this.SHA256   =( data )=>co.crypto.createHash('sha256').update(data).digest('base64');
	this.EncodeJWT=( json )=>{
		let res={};

		for ( let k in json ) {
			if ( k[0]==='-' )
				res[k]=json[k];
		}

		json['-firm']=this.SHA256( JSON.stringify(res)+(global.firmaToken||'firma') );

		return json;
	 };
	this.VerifyJWT=( json )=>{
		let res={};

		for ( let k in json ) {
			if ( k[0]==='-' && k.toLowerCase()!='-firm' )
				res[k]=json[k];
		}

		return json['-firm']===this.EncodeJWT(res)['-firm'];
	 };

	/*Funciones*/
	const GetHttp  =( cad, pos )=>{
		let len=cad.length, res='';

		for (;pos<len; pos++) {
			if   ( isSpecial(cad,pos) ) break;
			else                        res+=cad[pos];
		}

		return[res,pos+3];
	 };
	const GetHost  =( cad, pos )=>{
		let len=cad.length, res='';

		for (;pos<len && cad[pos]!='/' && cad[pos]!=':'; res+=cad[pos++] );

		return[res,pos];
	 };
	const GetPort  =( cad, pos )=>{
		if ( cad[pos]!=':' ) return['',pos];

		let len=cad.length, res='';

		for ( pos++; pos<len && isNumber(cad,pos); res+=cad[pos++] );

		return[res,pos];
	 };
	const GetPath  =( cad, pos )=>{
		if ( cad[pos]!='/' ) return[[],pos,''];

		let len=cad.length, pat=[], tem='', pas='';

		for (;pos<len; pos++) {
			if ( cad[pos]=='/' ) {
				if ( tem!='' ) {
					pat.push(tem);
					tem='';
				}
			}
			else if ( cad[pos]=='?' ) break;
			else                      tem+=cad[pos];

			pas+=cad[pos];
		}

		if ( tem!='' ) pat.push(tem);

		return[pat,pos,pas];
	 };
	const GetParams=( cad, pos )=>{
		if ( cad[pos]!='?' ) return[{},pos];

		let res={}, tem='', igu='', isi=false, len=cad.length;

		for ( pos++; pos<len; pos++ ) {
			if ( cad[pos]=='&' && isi ) {
				res[tem]=igu;
				tem     ='';
				igu     ='';
				isi     =0;
			}
			else if ( cad[pos]=='=' ) isi=true;
			else if ( isi           ) igu+=cad[pos];
			else                      tem+=cad[pos];
		}

		if ( isi ) res[tem]=igu;

		return[res,pos];
	 };

	const AjaxHTTP =( url, params )=>{
		return new sProm(gt, (run,err)=>{
			delete params.protocol;

			let aut=process.env.NODE_TLS_REJECT_UNAUTHORIZED; process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';
			params =Object.assign({ method:'GET' }, params);
			url    =gt.ParseURL(url);
			let htp=url.protocol==='http' ? co.http : co.https;
			url    =Object.assign({ host:url.host, port:url.port, method:'GET', path:url.paths }, params);

			delete url.data;

			if ( !url.headers                 ) url.headers                ={};
			if ( !url.headers['Content-Type'] ) url.headers['Content-Type']='application/x-www-form-urlencoded';

			if ( params.data ) url.headers['Content-Length']=Buffer.byteLength(params.data);
			else               url.headers['Content-Length']=0;

			const req=htp.request(url, res=>{
				if ( res.statusCode===200 ) {
					let data='';

					res.setEncoding('utf8');
					res.on( 'data', dat=>data+=dat );
					res.on( 'end' , x  =>run( data ) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut) );
				}
				else err(res.statusCode+': error en la consulta') || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut);
			});

			req.on('error', e=>err(e) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut));
			req.write(params.data||'');
			req.end();
		});
	 };
	const AjaxHTTP2=( url, params )=>{
		return new sProm(gt, (run,err)=>{
			let   aut=process.env.NODE_TLS_REJECT_UNAUTHORIZED; process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';
			let   res={ data:'' };
			let   pat=gt.ParseURL(url);
			const buf=new Buffer.from(params.data);
			const ht2=require('http2');
			const cli=ht2.connect(`https://${pat.host}${pat.port===80 ? '':':'+pat.port}`); cli.on('error', err);
			const req=cli.request(Object.assign(
				{
					[ht2.constants.HTTP2_HEADER_SCHEME]: 'https',
					[ht2.constants.HTTP2_HEADER_METHOD]: ht2.constants.HTTP2_METHOD_POST,
					[ht2.constants.HTTP2_HEADER_PATH  ]: pat.paths||'/',
					'Content-Type'                     : 'text/plain',
					'Content-Length'                   : buf.length,
				},
				params.headers
			));

			req.setEncoding('utf8');
			req.on( 'error'   , e=>err(e) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut) );
			req.on( 'response', headers=>{for(let k in headers)res[k]=headers[k];} );
			req.on( 'data'    , chunk  =>res.data+=chunk );
			req.on( 'end'     , ()     =>cli.close() || run( api.ParseHeaders( res ) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut) ) );
			req.write(buf);
			req.end();
		 });
	 };

	/*Inicio*/
	this.sClass='sApi';
	if ( noexec ) return;
	this.constructor(props);
 };sApi.prototype=new sObject(1,1);
global.sStarts       = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(1,1);

	/*Variables*/
	const gt=this;
	const va={};
	const fs=this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});

	/*Creacion*/
	this.constructor = function( _props, _noexec ) {
		fs.constructor(_props, 1);

		/*Inicio*/
		if ( !_noexec ) this.exe(_props) && gt.GetStarts( api.GetParsePath('.').path );

		/*Inicio*/
		return this;
	 };

	this.GetStarts=path=>{
		if ( path===undefined ) return va.path;

		(function tpa( tpath ) {
			let sta=api.IsExist( tpath );

			if      ( !sta                                           );
			else if ( sta.isDirectory()                              ) api.Travel(tpath).forEach(v=>tpa(v.path));
			else if ( sta.isFile() && tpath.indexOf('boStart.js')>-1 ) try{require(tpath)}catch(e){console.error(e)}
		})(path);

		va.path=path;

		return gt;
	 };

	/*Inicio*/
	this.sClass='sApi';
	if ( noexec ) return;
	this.constructor(props);
 };sApi.prototype=new sObject(1,1);
global.sPrompt       = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const fs       =this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
	const gthis    = this;
	global.prompt  = gthis;
	gthis.readline = require('readline');
	gthis.stream   = gthis.readline.createInterface({ input:process.stdin, output:process.stdout });

	gthis.stream.on('close', x=>gthis.Close());

	/*Metodos*/
	gthis.Prompt      = function(                     ) {
		gthis.levelPrompt = 0;

		gthis.stream.setPrompt(`%fmblacko%cd%fw(${api.GetParsePath('.').path})%r%fm:%r `.cmd());
		gthis.stream.prompt();
		gthis.stream.on('line', line=>{
			tipode(props)==VALUE_OBJECT && props.Parse && props.Parse(line.trim());
			gthis.stream.prompt();
		 });

		return gthis;
	 }
	gthis.Message     = function( message='', rdef='' ) {
		gthis.levelPrompt=props?1:0;

		return new sProm(gthis, run=>{
			let ca1=`%fy?%r${message}`, ca2=`%cd(${rdef})%r `;

			gthis.stream.question((ca1+ca2).cmd(), answer=>{
				process.stdout.moveCursor(0, -1-parseInt((ca1+ca2).cmd(false).length/process.stdout.columns));
				process.stdout.cursorTo(0);
				process.stdout.clearScreenDown();

				if ( answer=='' ) answer=rdef;

				console.info(`${ca1} %fc${answer}%r`.cmd());
				run(answer);
			});
		});
	 }
	gthis.Close       = function(                     ) {
		if ( gthis.levelPrompt==0 ) {
			console.info();
			process.exit(0);
		}
		else {
			clearTimeout(gthis.timer);
			gthis.stream.close();
			gthis.stream = gthis.readline.createInterface({ input:process.stdin, output:process.stdout });
			gthis.stream.on('close', x=>gthis.Close());
			gthis.Prompt();
		}
	 }
	gthis.PlaySpinner = function( message=''          ) {
		clearTimeout(gthis.timer);

		gthis.levelPrompt=props?1:0;

		let spi='⣾⣽⣻⢿⡿⣟⣯⣷';
		let len=spi.length, pos=-1;
		let tic=()=>{
			process.stdout.moveCursor(0, 0);
			process.stdout.cursorTo(0);
			process.stdout.clearScreenDown();
			process.stdout.write(message.replace(/%spinner/g, spi[(pos=++pos%len)]).cmd());
		 };

		gthis.timer = setInterval(tic, 80);

		return gthis;
	 }
	gthis.StopSpinner = function( clear=false         ) {
		clearInterval(gthis.timer);

		if ( clear!=false ) {
			process.stdout.moveCursor(0, 0);
			process.stdout.cursorTo(0);
			process.stdout.clearScreenDown();

			if ( typeof clear==VALUE_STRING ) process.stdout.write(clear.cmd());
			if ( props ) console.info();
		}

		gthis.stream.close();

		return gthis;
	 }

	/*Inicio*/
	this.sClass='sPrompt';
	if ( noexec ) return;
	this.constructor(props);
 };sPrompt.prototype=new sObject(0,1);
global.sCommands     = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const gthis   = this;
	const fs      = this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
	const va      = {};
	this.commands = {};

	/*Metodos*/
	this.Commands = function( commands                   ) {
		if ( commands===undefined ) return va.commands;

		va.commands=commands;

		tipode(commands)==VALUE_OBJECT && commands.sForEach(v=>this.Param(v));

		return this;
	 };
	this.Param    = function( command, desc, error, func ) {
		if ( command==undefined ) return;

		let help='', unique=false;

		if ( tipode(command)==VALUE_OBJECT ) {
			desc    = typeof command.description==VALUE_FUNCTION ? command.description() : command.description ||'';
			error   = typeof command.error      ==VALUE_FUNCTION ? command.error      () : command.error       ||'';
			help    = typeof command.help       ==VALUE_FUNCTION ? command.help       () : command.help        ||'';
			func    = command.func;
			unique  = command.unique||false;
			command = command.command||'';
		}

		if ( typeof error==VALUE_FUNCTION ) func =error;
		if ( typeof error!=VALUE_STRING   ) error='';
		if ( typeof desc ==VALUE_FUNCTION ) func =desc;
		if ( typeof desc !=VALUE_STRING   ) desc ='';
		if ( func==='GetHelp'             ) func = GetHelp;

		let all      = command.split(' ').filter(x=>x!='');
		let params   = all.filter(x=>x[0]=='<');
		let commands = all.filter(x=>x[0]!='<');

		commands.forEach(x=>this.commands[x]={params,desc,error,func,help,unique});

		return this;
	 };
	this.Parse    = function( commandLine                ) {
		if ( typeof commandLine ==VALUE_STRING ) commandLine=ParseString(commandLine);
		if ( tipode(commandLine)!=VALUE_ARRAY  ) commandLine=process.argv;

		va.scommandLine = commandLine.join(' ');
		commandLine     = commandLine.slice();

		let com, res={}, key;

		for ( let x=0; x<commandLine.length; x++ ) {
			if ( (com=this.commands[commandLine[x]]) && (key=commandLine[x]) ) {
				if ( com.params.length ) {
					if (!res[key]) res[key] = {accept:0, ok:com.params.length, params:[], func:com.func, error:com.error, unique:com.unique};

					com.params.forEach(par=>{
						let trq = par.indexOf('?>')==par.length-2 ? VALUE_QUERY : (par.indexOf('$>')==par.length-2 ? VALUE_VALID : true);

						switch (par.replace(/[<?$>]/g, '')) {
							case 'path'   : ParseParam(commandLine, x, res[key], trq, VALUE_PATH   ); break;
							case 'dir'    : ParseParam(commandLine, x, res[key], trq, VALUE_FOLDER ); break;
							case 'string' : ParseParam(commandLine, x, res[key], trq, VALUE_STRING ); break;
							case 'number' : ParseParam(commandLine, x, res[key], trq, VALUE_NUMBER ); break;
							case 'command': ParseParam(commandLine, x, res[key], trq, VALUE_SPECIAL); break;
						}
					});
				}
				else if ( !res[commandLine[x]] ) res[commandLine[x]] = {func:com.func};
			}
		}

		ExecResult(res);

		return this;
	 };
	this.Default  = function( func                       ) {
		if ( typeof func==VALUE_FUNCTION ) va.default = func.bind(this);
		else                               va.default = null;

		return this;
	 };

	/*Funciones*/
	function ParseString ( cad                                        ) {
		let pos=0, len=cad.length, res=[], tem='';

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\\'                 ) tem+=cad[++pos];//, tem+=cad[pos];
			else if ( cad[pos]==' ' && tem!=''       ) res.push(tem), tem='';
			else if ( cad[pos]=='"' || cad[pos]=="'" ){
				let com=cad[pos];
				for ( pos++; pos<len; pos++ ) {
					if      ( cad[pos]==com  ) break;
					else if ( cad[pos]=='\\' ) tem+=cad[pos++], tem+=cad[pos];
					else                       tem+=cad[pos];
				}
			}
			else tem+=cad[pos];
		}

		if ( tem!='' ) res.push(tem);

		return res;
	 };
	function ParseCommand( command                                    ) {
		if ( typeof command!=VALUE_STRING || command=='' ) return '';

		return command.split(' ').filter(x=>x!='');
	 };
	function ParseParam  ( commandLine, index, result, required, type ) {
		let ace=false, res, ind=index+1;
		for (;ind<commandLine.length; ind++) {
			if (
				( type==VALUE_PATH    && api.IsExist    (commandLine[ind])    ) ||
				( type==VALUE_FILE    && api.IsFile     (commandLine[ind])    ) ||
				( type==VALUE_FOLDER  && api.IsDirectory(commandLine[ind])    ) ||
				( type==VALUE_NUMBER  && !isNaN(parseFloat(commandLine[ind])) ) ||
				( type==VALUE_STRING  && !gthis.commands[commandLine[ind]]    ) ||
				( type==VALUE_SPECIAL &&  gthis.commands[commandLine[ind]]    )
			) {
				type==VALUE_NUMBER && (commandLine[ind]=parseFloat(commandLine[ind]));
				ace = true;
				result.accept++;
				result.params.push(commandLine[ind]);
				commandLine.splice(ind,1);
				break;
			}
			else if ( commandLine[ind][0]=='-' ) break;
		}

		required==VALUE_VALID && !ace && index+1==ind && ++result.accept && result.params.push(null);
		required==VALUE_QUERY && !ace                 && ++result.accept && result.params.push(null);
	 };
	function ExecResult  ( result                                     ) {
		let err = result.sFilter(x=>x.accept!=x.ok);

		if ( Object.keys(err).length ) console.error(err.sReduce((r,v)=>r+`  %fr*%r ${v.error}`,'').err('Error'));
		else {
			let uni = result.sSome((x,k)=>x.unique&&(x.key=k)&&x);
			uni && (result={}) && (result[uni.key]=uni);

			result.sForEach(x=>typeof x.func==VALUE_FUNCTION && x.func.apply(gthis, x.params));

			if ( typeof va.default==VALUE_FUNCTION && !result.sReduce((r,v)=>r||typeof v.func==VALUE_FUNCTION, false) ) {
				va.default(va.scommandLine);
			}
		}

		if ( props && typeof props.onFinish==VALUE_FUNCTION ) props.onFinish();
	 };

	/*Inicio*/
	this.sClass='sCommands';
	if ( noexec ) return;
	this.constructor(props);
 };sCommands.prototype=new sObject(0,1);
global.sOptions      = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const gthis = this;
	const op    = Object.assign({}, props);
	const fs    = this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});

	/*Creacion*/
	this.constructor = function( _props, _noexec ) {
		this.Project  ={};
		this.Variables={};
		this.Server   ={};
		this.Paths    ={};
		this.Compiler ={project:this.Project, variables:this.Variables};
		this.Resources={project:this.Project, variables:this.Variables};
		this.Api      ={project:this.Project, variables:this.Variables};

		this.Project.type    = VALUE_APP;
		this.Project.author  = '';
		this.Project.version = '';

		this.Server.protocol = 'http';
		this.Server.cert     = {};
		this.Server.host     = 'localhost';
		this.Server.port     = 8080;

		this.Paths.public = './public';
		this.Paths.rest   = './rest'  ;
		this.Paths.sock   = './sock'  ;

		this.Compiler.type             = VALUE_COMPILER;
		this.Compiler.watchFiles       = true;
		this.Compiler.autoRefresh      = true;
		this.Compiler.protocol         = 'http';
		this.Compiler.cert             = {};
		this.Compiler.host             = 'localhost';
		this.Compiler.port             = 8080;
		this.Compiler.pathSave         = './build';
		this.Compiler.pathPublic       = './public';
		this.Compiler.extensions       = ['.html','.htm','.svg','.css','.js','.json', '.bc','.bh','.bj'];
		this.Compiler.serverComponents = {host:process.env.hostComponents||'BlackKraken', protocol:process.env.protocolComponents||'https'};

		this.Resources.type       = VALUE_RESOURCES;
		this.Resources.watchFiles = true;
		this.Resources.protocol   = 'http';
		this.Resources.cert       = {};
		this.Resources.host       = 'localhost';
		this.Resources.port       = 8081;
		this.Resources.pathPublic = './public';
		this.Resources.extensions = ['.png','.ico','.jpg'];

		this.Api.type       = VALUE_API;
		this.Api.watchFiles = true;
		this.Api.protocol   = 'http';
		this.Api.cert       = {};
		this.Api.host       = 'localhost';
		this.Api.port       = 8082;
		this.Api.pathPublic = './public';
		this.Api.extensions = ['.json'];

		fs.constructor(_props, 1);
		if ( !_noexec ) this.exe(_props);
	 };

	/*Metodos*/
	this.Path    = function( path, environment ) {
		if ( path===undefined ) return op.path===undefined ? op.path='' : op.path;

		if ( environment ) op.environment=environment;

		return new sProm(this, (run,err)=>{
			if ( !api.IsFile(path) ) {err('el archivo no existe.'); return;}

			sCompileALL(path)
			.err(err)
			.run(opc=>{
				eval('this._options='+opc.data);
				this.Compile(this._options);

				if ( op.environment && this._options[op.environment] ) {
					this.Compile(this._options[op.environment]);

					this.environment=op.environment;
				}

				this.tri('onLoad', this);
				run(this);
			});
		});
	 };
	this.Compile = function( options           ) {
		if ( !options ) return;

		options.sForEach((v,k)=>{
			if ( k==='Project' ) {
				this.Project.type    = v.type   ||this.Project.type   ||VALUE_APP;
				this.Project.author  = v.author ||this.Project.author ||'';
				this.Project.version = v.version||this.Project.version||'';
			}
			else if ( k==='Server' ) {
				this.Server.protocol = v.protocol||this.Server.protocol||'http';
				this.Server.cert     = v.cert    ||this.Server.cert    ||{};
				this.Server.host     = v.host    ||this.Server.host    ||'localhost';
				this.Server.port     = v.port    ||this.Server.port    ||8080;
			}
			else if ( k==='Paths' ) {
				this.Paths.public = v.public||this.Paths.public||'./public';
				this.Paths.rest   = v.rest  ||this.Paths.rest  ||'./rest'  ;
				this.Paths.sock   = v.sock  ||this.Paths.sock  ||'./sock'  ;
			}
			else if ( k==='Compiler' ) {
				this.Compiler.watchFiles       = v.watchFiles ===undefined ? this.Compiler.watchFiles  : v.watchFiles ;
				this.Compiler.autoRefresh      = v.autoRefresh===undefined ? this.Compiler.autoRefresh : v.autoRefresh;
				this.Compiler.protocol         = v.protocol        ||this.Compiler.protocol        ||'http';
				this.Compiler.cert             = v.cert            ||this.Compiler.cert            ||{};
				this.Compiler.host             = v.host            ||this.Compiler.host            ||'localhost';
				this.Compiler.port             = v.port            ||this.Compiler.port            ||8080;
				this.Compiler.pathSave         = v.pathSave        ||this.Compiler.pathSave        ||'./build';
				this.Compiler.pathPublic       = v.pathPublic      ||this.Compiler.pathPublic      ||'./public';
				this.Compiler.extensions       = v.extensions      ||this.Compiler.extensions      ||['.html','.htm','.svg','.css','.js','.json', '.bc','.bh','.bj'];
				this.Compiler.serverComponents = v.serverComponents||this.Compiler.serverComponents||{host:'BlackKraken', protocol:'https'};

				this.Compiler.protocolText                 =this.Compiler.protocol.replace(/2/g, 's');
				this.Compiler.serverText                   =`${this.Compiler.protocolText}://${this.Compiler.host+(this.Compiler.port && this.Compiler.port!=80?':'+this.Compiler.port:'')}`;
				this.Compiler.serverComponents.protocolText=this.Compiler.serverComponents.protocol.replace(/2/g, 's');
				this.Compiler.serverComponents.serverText =`${this.Compiler.serverComponents.protocolText}://${this.Compiler.serverComponents.host+(this.Compiler.serverComponents.port && this.Compiler.serverComponents.port!=80?':'+this.Compiler.serverComponents.port:'')}`;
			}
			else if ( k==='Resources' ) {
				this.Resources.watchFiles = v.watchFiles===undefined ? this.Resources.watchFiles : v.watchFiles;
				this.Resources.protocol   = v.protocol  ||this.Resources.protocol  ||'http';
				this.Resources.cert       = v.cert      ||this.Resources.cert      ||{};
				this.Resources.host       = v.host      ||this.Resources.host      ||'localhost';
				this.Resources.port       = v.port      ||this.Resources.port      ||8081;
				this.Resources.pathSave   = v.pathSave;
				this.Resources.pathPublic = v.pathPublic||this.Resources.pathPublic||'./public';
				this.Resources.extensions = v.extensions||this.Resources.extensions||['.png','.ico','.jpg'];
			}
			else if ( k==='Api' ) {
				this.Api.watchFiles = v.watchFiles===undefined ? this.Api.watchFiles : v.watchFiles;
				this.Api.protocol   = v.protocol  ||this.Api.protocol  ||'http';
				this.Api.cert       = v.cert      ||this.Api.cert      ||{};
				this.Api.host       = v.host      ||this.Api.host      ||'localhost';
				this.Api.port       = v.port      ||this.Api.port      ||8082;
				this.Api.pathSave   = v.pathSave;
				this.Api.pathPublic = v.pathPublic||this.Api.pathPublic||'./public';
				this.Api.extensions = v.extensions||this.Api.extensions||['.json'];
			}
			else if ( k==='Variables' ) v.sForEach((vv,kk)=>this.Variables[kk]=vv);
		});
	 };

	/*Inicio*/
	this.sClass='sOptions';
	if ( noexec ) return;
	this.constructor(props);
 };sOptions.prototype=new sObject(0,1);
global.sCompileFiles = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const op   ={};
	const gthis=this;
	const fs   =this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
	let   nfils=0;

	/*Creacion*/
	this.constructor = function( _props, _noexec ) {
		this.files={};

		fs.constructor(_props, 1);
		if ( !_noexec ) this.exe(_props);
	 };

	/*Metodos*/
	this.Build = function( build ) {
		if ( build===undefined ) return op.build===undefined ? op.build='' : op.build;

		op.build   =build;
		gthis.files={};

		if      ( build.type==VALUE_RESOURCES ) TypeResources(build);
		else if ( build.type==VALUE_API       ) TypeFunctions(build);
		else                                    TypeCompiler (build);

		return gthis;
	 };

	/*Funciones*/
	function TravelVersion    ( path               ) {
		if ( path=='' ) return;

		let sta=api.IsExist(path);

		if      ( !sta               ) return '';
		else if (  sta.isDirectory() ) return api.Travel(path).reduce((r,v)=>r+TravelVersion(v.path),'');
		else if (  sta.isFile     () ) {
			let par=api.GetParsePath(path);

			if ( par.ext.toLowerCase()=='.json' ) {
				let dat='';

				try {
					dat=JSON.stringify( JSON.parse(api.Read(par.path)) );
				}
				catch(e){}

				return `\n\t${par.base.replace(/[0-9-_ ]/g, '').replace(/[.]/g,'_').toLowerCase()}: '${Hash(dat)}',`;
			}
		}

		return '';
	 };
	function GetDataVersion   ( build              ) {
		let jso=api.GetParsePath(api.GetParsePath(build.pathPublic).dir, 'api').path;

		return 'const CACHE_HASH = {'

			+gthis.files.sReduce((res,act)=>{
				return res+`\n\t${act.base.replace(/[0-9-_ ]/g, '').replace(/[.]/g,'_').toLowerCase()}: '${act.hash}',`;
			}, '')

			+api.Travel(jso).reduce((r,v)=>r+TravelVersion(v.path),'')

		+'\n };';
	 };
	function CompilerVersionJS( build              ) {
		let cor                    = gthis.files['/bcore.bj'];
		let ver                    = api.GetParsePath('/versions.js');
		ver.url                    = '/versions.js';
		ver.lower_url              = '/versions.js';
		ver.lower_path             = ver.path.toLowerCase();
		ver.clean                  = ver.dir;
		ver.data                   = GetDataVersion(build);
		ver.hash                   = Hash(ver.data);
		ver.data                   = sCompileJS(-1, `const CACHE_VERSION='${cor ? cor.hash : ver.hash}';\n${ver.data}`, Object.assign({},build));
		ver.mime                   = Mime('.js');
		gthis.files[ver.lower_url] = ver;

		if ( build.environment!='production' ) ver.data+='\nwindow.__debug__=true;';
		if ( cor                             ) cor.data = sCompileJS(-1, ver.data + '\n\n' + cor.original_data, Object.assign({}, build));
	 };
	function CompilerRefresh  ( build, file        ) {
		if (
			gthis.autoRefresh!=undefined ? gthis.autoRefresh : (build.autoRefresh!=undefined?build.autoRefresh:1)
			&&
			file.lower_url=='/index.html'
		) {
			file.data+=`<script src="./bcore_refresh.bj"></script>`;
		}
	 };
	function CompilerInclude  ( build, file, clean ) {
		gthis.files.sForEach(v=>{
			if ( v.included && v.included.sSome( (vv,kk)=>kk.toLowerCase().indexOf(file.base.toLowerCase())>-1 ) ) {
				let par = api.GetParsePath(v.path);
				par.ext = par.ext.toLowerCase();

				TypeCompilerBUILD(build, par, clean);
			}
		});
	 };

	/*Funciones - TypeCompiler*/
	function TypeCompilerBUILD( build, file, clean ) {
		nfils++;

		let vha=null, sha=null, jha=null, uha='/modules/'+file.base.replace(/[0-9-_ ]/g, '').toLowerCase();

		if ( gthis.files[uha] ) {
			vha=gthis.files[uha].hashView;
			sha=gthis.files[uha].hashStyle;
			jha=gthis.files[uha].hashScript;
		}

		sCompileALL(file.path, Object.assign({}, file, build))
		.run(x=>{
			x.url   = x.path.replace(clean, '').replace(/\\/g, '/');
			x.clean = clean;

			if ( x.url.indexOf('/modules/' )>-1 ) x.url = '/modules/'  + x.base.replace(/[0-9-_ ]/g, '');
			if ( x.url.indexOf('/json/'    )>-1 ) x.url = '/json/'     + x.base;
			if ( x.url.indexOf('/taxonomy/')>-1 ) x.url = '/taxonomy/' + x.base;

			x.lower_url              = x.url.toLowerCase();
			gthis.files[x.lower_url] = x;

			/*Compile Includes*/
			gthis.files.sForEach(x=>{
				if ( !x.included || !x.included[x.lower_path] ) return;
				if ( x.lower_path!=x.lower_path               ) TypeCompilerBUILD(api.GetParsePath(x.path), x.clean);
			});

			CompilerVersionJS(build);
			CompilerRefresh  (build, x);
			CompilerInclude  (build, x, clean);

			if ( gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined?build.viewLog:1) )
				console.info(`%cdLoad ['${(new Date).toLocaleTimeString("es-MX")}']:%r %fy${x.url}%r %cd${x.path}%r`.cmd());

			if      ( sha && sha!=x.hashStyle  ) gthis.tri('onChange', { sender:gthis, class:x.name, style  :x.codeStyle, data:x.data });
			else if ( vha && vha!=x.hashView   ) gthis.tri('onChange', { sender:gthis, class:x.name, view   :x.codeView , data:x.data });
			else if ( jha && jha!=x.hashScript ) gthis.tri('onChange', { sender:gthis, class:x.name, script :x.data     , data:x.data });
			else                                 gthis.tri('onChange', { sender:gthis, class:x.name, browser:1                        });
			if      ( --nfils==0               ) gthis.tri('onLoad'  , gthis);
		});
	 };
	function TypeCompilerLOAD ( build, path, clean ) {
		if ( path=='' ) return;

		let sta=api.IsExist(path);

		if      ( !sta               ) return;
		else if (  sta.isDirectory() ) api.Travel(path).forEach(x=>TypeCompilerLOAD(build, x.path, clean));
		else if (  sta.isFile     () ) {
			let par = api.GetParsePath(path);
			par.ext = par.ext.toLowerCase();

			if ( !build.extensions || build.extensions.indexOf(par.ext)>-1 )
				TypeCompilerBUILD(build, par, clean);
		}
	 };
	function TypeCompiler     ( build              ) {
		let pat=api.GetParsePath(build.pathPublic).path;

		if ( !api.IsDirectory(pat) ) return;

		TypeCompilerLOAD(build, pat              , pat              );
		TypeCompilerLOAD(build, __dirname+'/core', __dirname+'/core');

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined?build.watchFiles:1) ) {
			api.Watch(pat              , {recursive:true}).all(x=>TypeCompilerLOAD(build, x.path, pat              ));
			api.Watch(__dirname+'/core', {recursive:true}).all(x=>TypeCompilerLOAD(build, x.path, __dirname+'/core'));
		}
	 };

	/*Funciones - TypeResources*/
	function TypeResourcesBUILD( build, file, clean ) {
		nfils++;

		sCompileALL(file.path, Object.assign({}, file, build))
		.run(x=>{
			x.url              = x.path.replace(clean, '').replace(/\\/g, '/');
			gthis.files[x.url] = x;

			if ( gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined?build.viewLog:1) )
				console.info(`%cdLoad ['${(new Date).toLocaleTimeString("es-MX")}']:%r %fy${x.url}%r %cd${x.path}%r`.cmd());

			if ( --nfils==0 ) gthis.tri('onLoad', gthis);
		});
	 };
	function TypeResourcesLOAD ( build, path, clean ) {
		if ( path=='' ) return;

		let sta=api.IsExist(path);

		if      ( !sta               ) return;
		else if (  sta.isDirectory() ) api.Travel(path).forEach(x=>TypeResourcesLOAD(build, x.path, clean));
		else if (  sta.isFile     () ) {
			let par = api.GetParsePath(path);
			par.ext = par.ext.toLowerCase();

			if ( !build.extensions || build.extensions.indexOf(par.ext)>-1 )
				TypeResourcesBUILD(build, par, clean);
		}
	 };
	function TypeResources     ( build              ) {
		let pat=api.GetParsePath(build.pathPublic).path;

		if ( !api.IsDirectory(pat) ) return;

		TypeResourcesLOAD(build, pat, pat);

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined?build.watchFiles:1) )
			api.Watch(pat, {recursive:true}).all(x=>TypeResourcesLOAD(build, x.path, pat));
	 };

	/*Funciones - TypeFunctions*/
	function TypeFunctionsBUILD( build, file, clean ) {
		file.url = file.path.replace(clean, '').replace(file.ext, '').replace(/\\/g, '/');

		if ( gthis.files[file.url] && require.cache[file.path] )
			delete require.cache[file.path];

		file.clean = clean;

		try{file.exec=require(file.path).exec}catch(e){file.exec=null; console.error(e)}

		if ( file.url.indexOf('/rest/')>-1 ) file.url = '/rest/' + file.base.replace(/(^[0-9-_ ]+|(.js)$)/gmi, '');
		if ( file.url.indexOf('/sock/')>-1 ) file.url = '/sock/' + file.base.replace(/(^[0-9-_ ]+|(.js)$)/gmi, '');
		if ( file.url.indexOf('/json/')>-1 ) file.url = '/json/' + file.base;

		file.lower_url        = file.url.toLowerCase();
		gthis.files[file.url] = file;

		if ( gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined?build.viewLog:1) )
			console.info(`%cdLoad ['${(new Date).toLocaleTimeString("es-MX")}']:%r %fy${file.url}%r %cd${file.path}%r`.cmd());
	 };
	function TypeFunctionsLOAD ( build, path, clean ) {
		if ( path=='' || path.indexOf('node_modules')>-1 ) return;

		let sta=api.IsExist(path);

		if      ( !sta               ) return;
		else if (  sta.isDirectory() ) api.Travel(path).forEach(x=>TypeFunctionsLOAD(build, x.path, clean));
		else if (  sta.isFile     () ) {
			let par = api.GetParsePath(path);

			if ( par.ext.toLowerCase()==='.js' )
				TypeFunctionsBUILD(build, par, clean);
		}
	 };
	function TypeFunctions     ( build              ) {
		if ( !api.IsDirectory(build.pathPublic) ) return;

		let pat=api.GetParsePath(build.pathPublic).path;

		if ( !api.IsDirectory(pat) ) return;

		TypeFunctionsLOAD(build, pat, pat);

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined?build.watchFiles:1) )
			api.Watch(pat, {recursive:1}).all(x=>TypeFunctionsLOAD(build, x.path, pat));
	 };

	/*Inicio*/
	this.sClass='sCompileFiles';
	if ( noexec ) return;
	this.constructor(props);
 };sCompileFiles.prototype=new sObject(0,1);
global.sServer       = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
	const op   ={};
	const va   ={};
	const co   ={};

	/*Metodos*/
	gthis.Protocol    = function( protocol                                                 ) {
		if ( protocol===undefined ) return op.protocol===undefined ? op.protocol='http' : op.protocol;

		if ( protocol!=op.protocol ) {
			op.protocol=protocol;

			gthis.Disconnect();

			switch (protocol) {
				case 'https': if ( !co.https ){co.https=require('https');} break;
				case 'http2': if ( !co.http2 ){co.http2=require('http2');} break;
				default     : if ( !co.http  ){co.http =require('http' );} break;
			}
		}

		return gthis;
	 };
	gthis.Connect     = function({ host='0.0.0.0', port=8080, cert={}, protocol=undefined }) {
		if ( !op.protocol          ) this.Protocol(protocol||'http');
		if ( api.IsFile(cert.cert) ) cert.cert=api.Read(cert.cert);
		if ( api.IsFile(cert.key ) ) cert.key =api.Read(cert.key );

		switch (op.protocol) {
			case 'https': co.server=co.https.createServer(cert, requs_this).on('upgrade', updat_this); break;
			case 'http2': co.server=co.http2.createSecureServer(cert); co.server.on('stream', requ2_this); break;
			default     : co.server=co.http .createServer(reque_this).on('upgrade', updat_this); break;
		}

		co.server.listen( port, host, x=>gthis.tri('onConnect') );

		return gthis;
	 };
	gthis.Disconnect  = function(                                                          ) {
		if ( co.server ) {
			if ( gthis.response ) gthis.response.end();
			if ( gthis.request  ){gthis.request.connection.end(); gthis.request.connection.destroy;}
			co.server.close(x=>gthis.tri('onDisconnect'));
		}

		return gthis;
	 };
	gthis.EnabledCors = function( enabledcors                                              ) {
		if ( enabledcors===undefined ) return op.enabledcors===undefined ? op.enabledcors=false : op.enabledcors;

		op.enabledcors=enabledcors;

		return gthis;
	 };
	gthis.GetData     = function( headers                                                  ) {
		return new sProm(gthis, (run,err)=>{
			let dat = [];

			(op.protocol==='http2' ? headers.stream : headers.request)
			.on('error', err)
			.on('data' , chunk=>dat.push(chunk) )
			.on('end'  , x=>run(Buffer.concat(dat).toString()) );
		 });
	 };
	gthis.Request     = function( headers                                                  ) {return gthis};

	/*Eventos*/
	function reque_this( request, response ) {
		if ( request.method==='OPTIONS' ) {Response({code:204}, { petition:{request, response} }); return}

		let body='', json;
		request.setEncoding('utf8');
		request
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body)} catch(e) {json={}}

			let hea=ParseHttpHeaders(
				Object.assign(
					{method:request.method},
					{protocol:'http'},
					{petition:{request, response}},
					request.headers,
					{headers:request.headers},
					{referer: 'http://'+request.headers.host+request.url},
					{body, json},
				)
			);

			hea.Response=o=>Response(o, hea);

			gthis.Request(hea);
			gthis.tri('onRequest', hea, gthis);
		});
	 };
	function requs_this( request, response ) {
		if ( request.method==='OPTIONS' ) {Response({code:204}, { petition:{request, response} }); return}

		let body='', json;
		request.setEncoding('utf8');
		request
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body)} catch(e) {json={}}

			let hea=ParseHttpHeaders(
				Object.assign(
					{method:request.method},
					{protocol:'https'},
					{petition:{request, response}},
					request.headers,
					{headers:request.headers},
					{referer: 'https://'+request.headers.host+request.url},
					{body, json},
				)
			);

			hea.Response=o=>Response(o, hea);

			gthis.Request(hea);
			gthis.tri('onRequest', hea, gthis);
		});
	 };
	function requ2_this( stream , headers  ) {
		if ( request.method==='OPTIONS' ) {Response({code:204}, { petition:{stream} }); return}

		let body='', json;
		stream.setEncoding('utf8');
		stream
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body)} catch(e) {json={}}

			let obj={};

			for (let x in headers) obj[x.replace(/:/g, '')]=headers[x];

			obj.protocol='http2';
			obj.referer ='https://'+headers[':authority']+headers[':path'];
			obj.petition={stream};
			obj.method  =headers[':method'];
			obj         =ParseHttpHeaders(obj);
			obj.Response=o=>Response(o, obj);

			gthis.Request(hea);
			gthis.tri('onRequest', obj, gthis);
		});
	 };

	function updat_this  ( request, socket ) {
		if ( !va.crypto  ) va.crypto =require('crypto');
		if ( !va.sockets ) va.sockets=[], va.idsSockets=0;

		// Conectar Socket
		if ( !(gthis.Key=request.headers['sec-websocket-key']) ) {
			Reject(400, 'El cliente no proporciono la llave publica WebSocket.');
			return;
		}

		if ( !(gthis.Version=parseInt(request.headers['sec-websocket-version'], 10)) || isNaN(gthis.Version) ) {
			Reject(400, 'El cliente no proporciono la version del protocolo.');
			return;
		}

		if      ( gthis.Version===13 ) { gthis.Origin = request.headers['origin'              ]; }
		else if ( gthis.Version===8  ) { gthis.Origin = request.headers['sec-websocket-origin']; }
		else                           { Reject(400, 'Conexion rechazada por no ser version 8 o 13'); return; }

		socket.write(
			'HTTP/1.1 101 Switching Protocols\r\n' +
			'Upgrade: websocket\r\n'               +
			'Connection: Upgrade\r\n'              +
			'Sec-WebSocket-Accept: '               + va.crypto.createHash('sha1').update(gthis.Key + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11').digest('base64') + '\r\n' +
			'Sec-WebSocket-protocolo: chat\r\n'    +
			'\r\n'
		, 'ascii');

		// Configurando socket
		socket.setTimeout(0);

		let tem=va.idsSockets++;
		tem    ={ id:tem, socket };

		gthis.tri('onSocketOpen', socket);
		socket.on('close'  , function(x){ close_socket.call(this, x, tem) });
		socket.on('data'   , function(x){ datas_socket.call(this, x, tem) });
		socket.on('timeout', function(x){ timeo_socket.call(this, x, tem) });
		va.sockets.push(tem);

		socket.Json=response=>{
			if ( !response.code ) response.code=200;
			if ( !response.data ) response.data=GetDefResponse(response.code);
			if ( response.data && tipode(response.data)===VALUE_OBJECT && response.data.code && response.data.data===undefined )
				response.data.data=GetDefResponse(response.data.code);

			socket.write( StrToDat( JSON.stringify(response) ) );
		 };
		socket.Send=response=>socket.write( StrToDat(response) );

		gthis.SocketSend   =(json, sock)=>(sock||socket).write(StrToDat(json));
		gthis.SocketSendAll=(json      )=>va.sockets.forEach(s=>s.socket.write(StrToDat(json)));
	 };
	function timeo_socket( data, tsoc      ) {
		gthis.tri('onSocketTimeout', gthis, tsoc.socket);
	 };
	function close_socket( data, tsoc      ) {
		gthis.tri('onSocketClose', gthis, tsoc.Socket);

		for ( let x=va.sockets.length; x--; ) {
			if ( va.sockets[x].id===tsoc.id ) {
				va.sockets.splice(x, 1);
				break;
			}
		}
	 };
	function datas_socket( data, tsoc      ) {
		if ( data[0]===0x88 ) this.end();
		else {
			let res = DatToStr(data);

			try {
				let jso=null;

				try{jso=JSON.parse(res)} catch(e){}

				gthis.tri('onSocketData', tsoc.socket, jso, res);
			}
			catch(e) {
				gthis.tri('onSocketError', tsoc.socket, e, res);
			}
		}
	 };

	/*Funciones*/
	function GetDefResponse  ( code    ) {
		switch (code) {
			case 200: return 'Ok';
			case 201: return 'Created';
			case 202: return 'Accepted';
			case 203: return 'Non-Authoritative Information';
			case 204: return 'No Content';
			case 205: return 'Reset Content';
			case 206: return 'Partial Content';
			case 207: return 'Multi-Status';
			case 208: return 'Multi-Status';
			case 226: return 'IM Used';
			case 204: return 'Ok';

			case 300: return 'Multiple Choice';
			case 301: return 'Moved Permanently';
			case 302: return 'Found';
			case 303: return 'See Other';
			case 304: return 'Not Modified';
			case 305: return 'Use Proxy';
			case 306: return 'unused';
			case 307: return 'Temporary Redirect';
			case 308: return 'Permanent Redirect';

			case 400: return 'Bad Request';
			case 401: return 'Unauthorized';
			case 402: return 'Payment Required';
			case 403: return 'Forbidden';
			case 404: return 'Not Found';
			case 405: return 'Method Not Allowed';
			case 406: return 'Not Acceptable';
			case 407: return 'Proxy Authentication Required';
			case 408: return 'Request Timeout';
			case 409: return 'Conflict';
			case 410: return 'Gone';
			case 411: return 'Length Required';
			case 412: return 'Precondition Failed';
			case 413: return 'Payload Too Large';
			case 414: return 'URI Too Long';
			case 415: return 'Unsupported Media Type';
			case 416: return 'Requested Range Not Satisfiable';
			case 417: return 'Expectation Failed';
			case 418: return 'I\'m a teapot';
			case 421: return 'Misdirected Request';
			case 422: return 'Unprocessable Entity';
			case 423: return 'Locked';
			case 424: return 'Failed Dependency';
			case 426: return 'Upgrade Required';
			case 428: return 'Precondition Required';
			case 429: return 'Too Many Requests';
			case 431: return 'Request Header Fields Too Large';
			case 451: return 'Unavailable For Legal Reasons';

			case 500: return 'Internal Server Error';
			case 501: return 'Not Implemented';
			case 502: return 'Bad Gateway';
			case 503: return 'Service Unavailable';
			case 504: return 'Gateway Timeout';
			case 505: return 'HTTP Version Not Supported';
			case 506: return 'Variant Also Negotiates';
			case 507: return 'Insufficient Storage';
			case 508: return 'Loop Detected';
			case 510: return 'Not Extended';
			case 511: return 'Network Authentication Required';
		}

		return '';
	 };
	function ParseHttpHeaders( headers ) {
		let res={};
		let pat=(headers.referer||'').replace(/^http(s?):\/\/[A-z0-9:.]+/g, '');
		let par='';

		if ( pat.indexOf('?')>-1 ) {
			par=pat.substring(pat.indexOf('?')+1);
			pat=pat.substring(0, pat.indexOf('?'));
		}

		pat=pat.split('/').splice(1);
		par=par.split('&');
		par=par.reduce((r,v)=>{
			let t=v.split('=');

			if ( t[0] && t[1] && t[0]!='' && t[1]!='' )
				r[t[0]||'']=(t[1]||'');

			return r;
		}, {});

		res=api.ParseHeaders( headers );

		if ( pat.length==1 && pat[0]==='' ) pat=[];

		res.path  =pat;
		res.params=par;

		return res;
	 };

	function Reject  ( socket, code, message ) {
		console.info( message );
		socket.end(`HTTP/1.1 ${code} ${va.codes[code]}\r\nConnection: close\r\n`, 'ascii');
	 };
	function DatToStr( data                  ) {
		let buf = '';
		let dat = 0;

		if ( data[1]&0x80 ) {
			let mlen = data[1] & 0x7F;

			if ( mlen<126 ) {
				for ( let i=0; i<mlen; i++ ) {
					buf+= String.fromCharCode(data[6+i] ^ data[i%4+2]);
				}
			}
			else if( mlen===126 ) {
				mlen = data[2] * 256 + data[3];

				for ( let i=0; i<mlen; i++ ) {
					buf+= String.fromCharCode(data[8+i] ^ data[i%4+4]);
				}
			}
		}

		return buf;
	 };
	function StrToDat( data                  ) {
		data = String(data);

		let length = Buffer.byteLength(data);
		let index  = 2 + (length>65535 ? 8 : (length>125 ? 2 : 0));
		let buffer = new Buffer.alloc(index+length);

		buffer[0] = 129;

		if ( length>65535 ) {
			buffer[1] = 127;

			buffer.writeUInt32BE(0     , 2);
			buffer.writeUInt32BE(length, 6);
		}
		else if ( length>125 ) {
			buffer[1] = 126;

			buffer.writeUInt16BE(length, 2);
		}
		else buffer[1] = length;

		buffer.write(data, index);

		return buffer;
	 };

	function Response(response, headers) {
		response=Object.assign({data:'', code:404, type:'txt'}, response);

		if ( response.data=='' ) {
			response.data=GetDefResponse(response.code);
			response.type='txt';
		}

		if ( headers.petition.response  ) {
			headers.petition.response.writeHead(
				response.code,
				Object.assign(
					(!op.enabledcors?{}:{
						'Access-Control-Allow-Origin'     : '*',
						'Access-Control-Allow-Methods'    : 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
						'Access-Control-Allow-Headers'    : 'Origin, X-Requested-With, Content-Type, X-Auth-Token, Accept',
						'Access-Control-Allow-Credentials': 'true',
					}),
					{'Content-Type':Mime(response.type)},
					response.headers||{},
				)
			);
			headers.petition.response.end(response.data);
		}
		else if ( headers.petition.stream ) {
			headers.petition.stream.respond(
				Object.assign(
					(!op.enabledcors?{}:{
						'Access-Control-Allow-Origin'     : '*',
						'Access-Control-Allow-Methods'    : 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
						'Access-Control-Allow-Headers'    : 'Origin, X-Requested-With, Content-Type, X-Auth-Token, Accept',
						'Access-Control-Allow-Credentials': 'true',
					}),
					{':status':response.code,'content-type':Mime(response.type)},
					response.headers||{},
				)
			);
			headers.petition.stream.end(response.data);
		}
	 };

	/*Inicio*/
	this.sClass='sServer';
	if ( noexec ) return;
	this.constructor(props);
 };sServer.prototype=new sObject(0,1);
global.sServerBO     = function( props, noexec ) {
	/*Herencia*/
	this.sServer=sServer;
	this.sServer(0,1);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});

	/*Creacion*/
	gthis.constructor = function( _props, _noexec ) {
		fs.constructor(_props, 1);

		if ( _noexec ) return;

		this.exe(_props);

		this.EnabledCors(true);
	 };

	/*Metodos*/
	gthis.Connect = function( _props ) {
		props=_props;

		this
		.Protocol(_props.protocol)
		.eve({
			onSocketOpen :socko_this,
			onSocketClose:sockc_this,
			onSocketData :sockd_this,
			onRequest    :reque_this,
			onConnect    :x=>{
				console.info(`servidor %fy"${_props.type}"%r %fgconectado%r`.cmd())

				gthis.scompiles=new sCompileFiles({
					eve  :{
						onLoad:x=>gthis.tri('onLoad',x) &&
						gthis.scompiles.eve(false).eve({ onChange:chang_file })
					},
					Build: _props,
				});
			},
		});

		fs.Connect({
			host: _props.host,
			port: _props.port,
			cert: _props.cert,
		});
	 };

	/*Eventos*/
	function socko_this( sock                ) {
		let fil=gthis.scompiles.files.sSome((v,k)=>v.lower_url==='/sock/onopen' ? v : false);

		if ( fil && typeof fil.exec===VALUE_FUNCTION )
			try{fil.exec(sock)} catch(e){console.error('Error:', e)}
	 };
	function sockc_this( sock                ) {
		let fil=gthis.scompiles.files.sSome((v,k)=>v.lower_url==='/sock/onclose' ? v : false);

		if ( fil && typeof fil.exec===VALUE_FUNCTION )
			try{fil.exec(sock)} catch(e){console.error('Error:', e)}
	 };
	function sockd_this( sock, json, message ) {
		if ( !json || tipode(json)!=VALUE_OBJECT || tipode(json.data)!=VALUE_OBJECT ) {sock.Json({ code:400 }); return}

		json.data.sForEach((v,k)=>{
			let fil=gthis.scompiles.files.sSome((vv,kk)=>vv.lower_url==='/sock/'+k.toLowerCase() ? vv : false);

			if ( fil && typeof fil.exec===VALUE_FUNCTION )
				try{
					let res=null;
					let sda=data=>sock.Json({ id:json.id, type:tipode(data), data:data });

					if ( tipode(v)==VALUE_ARRAY ) res=fil.exec.apply(sock, v);
					else                          res=fil.exec.call (sock, v);

					if ( tipode(res)===VALUE_OBJECT && res.sClass==='sProm' ) {
						res.run(r=>sda(r));
						res.err(e=>sda(e));
					}
					else sda(res);
				} catch(e) {
					console.error('Error:', e);
				}
		});

	 };
	function reque_this( headers             ) {
		if      ( headers.path[0]=='alive' ) headers.Response({ code:200 });
		else if ( headers.path[0]=='stop'  ) headers.Response({ code:200 }) && process.exit(0);
		else {
			if      ( headers.botype==='component' ) ResponseBOC(headers);
			else if ( props.type===VALUE_API       ) ResponseAPI(headers);
			else                                     ResponseCOM(headers);
		}
	 };

	function chang_file( change ) {
		if ( !gthis.SocketSendAll ) return;

		if ( change.style ) gthis.SocketSendAll( JSON.stringify({ type:'style', class:change.class, code:change.style }) );
		else                gthis.SocketSendAll( JSON.stringify({ type:'code' , class:change.class, code:change.data  }) );
	 };

	/*Funciones*/
	function ResponseBOC( headers ) {
		let fil=gthis.scompiles.files.sSome((v,k)=>headers.path[0] && v.lower_url===`/modules/${headers.path[0].toLowerCase()}.bj` ? v : null);

		if ( fil ) headers.Response({ code:200, data:fil.data, type:'js' });
		else {
			api.Ajax(
				`${props.serverComponents.serverText}/${headers.path[0]}`,
				{ protocol:props.serverComponents.protocol, headers:{token:'1', version:headers.version, botype:'component'}
			})
			.run(x=>{
				if ( x.status===200 ) {
					headers.Response({ code:200, data:x.data });

					gthis.scompiles.files[`/modules/${headers.path[0]}.bj`]={
						data     : x.data,
						base     : `${headers.path[0]}.bj`,
						hash     : Hash(x.data),
						lower_url: `/modules/${headers.path[0]}.bj`.toLowerCase()
					};
				}
				else headers.Response({ code:404 });
			})
			.err( e=>headers.Response({ code:404 }) && console.error(e) );
		}
	 };
	function ResponseAPI( headers ) {
		let url='/rest/'+headers.path.join('/').toLowerCase();
		let fil=gthis.scompiles.files.sSome((v,k)=>v.lower_url===url ? v : false);

		if ( fil && typeof fil.exec===VALUE_FUNCTION ) try{fil.exec(headers)} catch(e){console.error('Error:', e); headers.Response({ code:500 });}
		else                                           headers.Response({ code:404 });
	 };
	function ResponseCOM( headers ) {
		let url='/'+headers.path.join('/').toLowerCase(); url=='/' &&(url='/index.html');
		let fil=gthis.scompiles.files.sSome((v,k)=>k.toLowerCase()===url ? v : null);

		if      ( fil                           ) headers.Response({ code:200, data:fil.data, type:fil.ext });
		else if ( props.project.type==VALUE_SPA ) {
			gthis.scompiles.files.sSome((v,k)=>{
				if ( k.toLowerCase()=='/index.html' ) {
					headers.Response({ code:200, data:v.data, type:v.ext });
					return true;
				}
			}) || headers.Response({ code:404 });
		}
		else headers.Response({ code:404 });
	 };

	/*Inicio*/
	this.sClass='sServerBO';
	if ( noexec ) return;
	this.constructor(props);
 };global.sServerBO.prototype=new sServer;
global.sMongo        = function( props, noexec ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0,1);

	/*Variables*/
	const gthis=this;
	const va   ={};
	const fs   =this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});

	/*Creacion*/
	gthis.constructor=( _props, _noexec )=>{
		fs.constructor(_props, 1);

		va.client=require(props.mongodb||'mongodb').MongoClient;
		va.db    =null;

		if ( _noexec ) return;
		gthis.exe(_props);
	 };

	/*Metodos*/
	gthis.Connect=({ host='127.0.0.1', port=27017, dbName='BODB' })=>{
		return new sProm(gthis, (run,err)=>{
			va.client.connect(`mongodb://${host}:${port}`, {useNewUrlParser:true}, function(ero, client) {
				if ( ero!=null ) {
					gthis.tri('onError', `error al conectarse a mongoDB con el host: "${host}", y el puerto: ${port}`);
					err(`error al conectarse a mongoDB con el host: "${host}", y el puerto: ${port}`);
					console.error(`error al conectarse a mongoDB con el host: "${host}", y el puerto: ${port}`);
					return;
				}

				va.db=client.db(dbName);

				gthis.tri('onConnect', `conexion a mongoDB en el host: "${host}", y el puerto: ${port}`);
				run(va.db);
				gthis.Get('tokens').run( v=>v.forEach(vv=>tok[vv.token]=vv.user) );
			});
		});
	 };
	gthis.Get    =(collection, filter={}                          )=>{
		return new sProm(gthis, (run,err)=>{
			const cl=va.db.collection(collection);

			cl.find(filter).toArray(function(error, docs) {
				if ( error===null ) run(docs);
				else                err('error en Get de cMongoDB: '+error);
			});
		});
	 };
	gthis.Set    =(collection, data                               )=>{
		return new sProm(gthis, (run,err)=>{
			const cl=va.db.collection(collection);
			let   en=[];

			if ( tipode(data)===VALUE_ARRAY ) en=data;
			else                              en=[data];

			cl.insertMany(en, (error, result)=>{
				if ( error===null ) run(result);
				else                err('error en Set cMongo: ' + error);
			});
		});
	 };
	gthis.Del    =(collection, filter                             )=>{
		return new sProm(gthis, (run,err)=>{
			const col=va.db.collection(collection);

			if ( tipode(filter)===VALUE_OBJECT ) {
				col.drop((error, result)=>{
					if      ( error        ===null           ) run(result);
					else if ( error.message==='ns not found' ) run(null);
					else                                       err('error en Del/drop de cMong: '+error);
				});
			}
			else col.deleteOne(filter, (error, result)=>{
				if      ( error        ===null           ) run(result);
				else if ( error.message==='ns not found' ) run(null);
				else                                       err('error en Del/one de cMongo: '+error);
			});
		});
	 };

	/*Inicio*/
	if ( !noexec ) gthis.constructor(props);
 };sMongo.prototype=new sObject(0,1);

global.api = new sApi();

// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/js/test0.js')
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/js/test0.js', {zip:1})
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/json/test0.json')
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/html/test0.html')
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/bh/test0.bh', {environment:'debug'})
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/bc/test0.bc', {environment:'debug'})
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/bj/test0.bj', {environment:'debug', zip:0})
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/www/public/modules/cMain.bj', {environment:'debug'})
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/bh/test0.bh')
// sCompileALL('/Users/anda/Desktop/BlackOcean/code/npm/test/bj/test0.bj')

// .run(file=>api.Write('/Users/anda/Desktop/BlackOcean/code/npm/test/file_compile0', file.data) /*&& console.log(99, file.definition)*/)
// .err(e=>console.error(e));



// new sServer({
// 	eve    :{
// 		onConnect:x=>console.log('conectado'),
// 		onRequest:(x,t)=>t.Response({ code:200 }),
// 	 },
// 	Connect:{protocol:'http', cert:{cert:'/Users/anda/Desktop/BlackKraken/code/node/certs/kraken_server.crt', key:'/Users/anda/Desktop/BlackKraken/code/node/certs/kraken_key.pem'}},
// });
