/*Variables*/
const sfiles    = {};
const errorFile = '%fmstart%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj';

/*Funciones*/
const IsAlive     = function( path, environment='debug' ) {
	return new sProm((run,err)=>{
		const file=api.GetParsePath(path||'.', 'BlackOcean.bj');

		if ( api.IsExist(file.path) ) {
			require('child_process').exec(`node '${__dirname+'/../server.js'}' '${path}' ${environment}`, { cwd:path });

			new sOptions({ Path:file.path, eve:{
				onLoad:x=>{
					(function con( number ){
						require('http')
						.get({
							hostname: x.Compiler.host||'0.0.0.0',
							port    : x.Compiler.port||8080,
							path    : '/alive',
							method  : 'GET',
						}, res=>{
							const {statusCode} = res;

							if ( statusCode!=200 ) err(statusCode);
							else {
								try {
									let fil, res=null;

									try {fil=JSON.parse( api.Read(__dirname+'/connections.json') );}
									catch (e) {fil={};}

									fil[path]=environment;

									api.Write(__dirname+'/connections.json', JSON.stringify(fil));
									run();
								}
								catch (e) {err(e);}
							}
						})
						.on('error', x=>{
							if ( number>50 ) err(x);
							else             setTimeout(x=>con(number+1), 100);
						});
					})(0);
				}
			}});
		}
		else err(errorFile.cmd());
	});
 }
const ExecService = function( path                      ) {
	let file=api.GetParsePath(path||'.', 'BlackOcean.bj');

	if ( api.IsExist(file.path) ) {
		(new sPrompt).PlaySpinner(`%bb%fy %spinner Iniciando servicio... %r`);

		IsAlive(file.dir)
		.run(x=>prompt.StopSpinner('%fg✓%r Servicio %fginiciado%r correctamente\n'))
		.err(x=>prompt.StopSpinner(`%fr✗%r ${x}`));
	}
	else console.log(`%fr*%r ${errorFile}`.err('Error'));
 }
const ExecLog     = function( path                      ) {
	let file=api.GetParsePath(path||'.', 'BlackOcean.bj');
	if ( api.IsExist(file.path) ) {
		global.starts=new sStarts();

		let   rop;
		const opc=new sOptions, sec=new sServerBO, ser=new sServerBO, sea=new sServerBO;

		opc.eve({
			onLoad:x=>{
				rop=x;
				sec.Connect(rop.Compiler);
			}
		});
		sec.eve({ onLoad:x=>ser.Connect(rop.Resources) });
		ser.eve({ onLoad:x=>sea.Connect(rop.Api      ) });

		opc.Path(file.path);
	}
	else console.log(`%fr*%r ${errorFile}`.err('Error'));
 }
const Exec        = function( command, path             ) {
	if ( command==='service' ) ExecService(path);
	else                       ExecLog    (path);
 };

/*Definicion*/
module.exports.command = {
	command    : '-s start <command?> <dir$>',
	description: 'Inicia el servicio de BlackOcean',
	error      : errorFile,
	func       : Exec,
	help       :
		`%fmstart%r inicia el servicio de BlackOcean que se podrá visualizar en un navegador según la configuración en BlackOcean.bj

		%fcuso:%r
		  %fc*%r bo %fmstart%r (sin argumentos en el directorio que contenga un archivo BlackOcean.bj)
		  %fc*%r bo %fmstart%r <carpeta que contenga un archivo BlackOcean.bj>

		  %cdNota:%r %fmstart%r puede ser iniciado en el CLI solo escribiendo %fmstart%r o %fm-s%r`.inf('Comando %bm%fwstart%r%bb%fy [alias: -s]'),
 };
