/*Variables*/
const errorFile = '%fmbuild%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj';

/*Funciones*/
const Exec = function( path ) {
	let file=api.GetParsePath(path||'.', 'BlackOcean.bj');

	if ( api.IsExist(file.path) ) {
		let   rop;
		const opc=new sOptions;

		opc.eve({
			onLoad:x=>{
				const fco=new sCompileFiles; fco.watchFiles=0, fco.viewLog=0;
				const frs=new sCompileFiles; frs.watchFiles=0, frs.viewLog=0;
				const fap=new sCompileFiles; fap.watchFiles=0, fap.viewLog=0;
				const fre=( path, cfiles )=>{
					cfiles.files.sForEach(v=>{
						let pat=api.GetParsePath(path, v.url).path;

						api.CreateDirectory(api.GetDirectory(pat));
						api.Write(pat, v.data);

						if ( api.IsFile(pat) )
							console.log(`%cdSave ['${(new Date).toLocaleTimeString("es-MX")}']:%r %fy${v.url}%r %cd${pat}%r`.cmd());
					});
				 };

				if ( x.Compiler .pathSave ) fco.eve({onLoad:y=>fre( api.GetParsePath(x.Compiler .pathSave).path, y )}).Build(x.Compiler );
				if ( x.Resources.pathSave ) frs.eve({onLoad:y=>fre( api.GetParsePath(x.Resources.pathSave).path, y )}).Build(x.Resources);
				if ( x.Api      .pathSave ) fap.eve({onLoad:y=>fre( api.GetParsePath(x.Api      .pathSave).path, y )}).Build(x.Api      );
			}
		});

		opc.Path(file.path);
	}
	else console.log(`%fr*%r ${errorFile}`.err('Error'));
 };

/*Definicion*/
module.exports.command = {
	command    : 'build <dir$>',
	description: 'Compila el proyecto BlackOcean',
	error      : '%fmbuild%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj',
	func       : Exec,
	help       :
		`%fmbuild%r Compila el proyecto BlackOcean

		%fcuso:%r
		  %fc*%r bo %fmbuild%r (sin argumentos en el directorio de trabajo)
		  %fc*%r bo %fmbuild%r <carpeta que contenga un archivo BlackOcean.bj>

		  %cdNota:%r %fmbuild%r puede ser iniciado en el CLI solo escribiendo %fmbuild%r`.inf('Comando %bm%fwbuild%r%bb%fy'),
 };
