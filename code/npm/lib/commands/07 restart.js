/*Funciones*/
const StopDesc = function( log         ) {
	return new sProm((run,err)=>{
		for ( let x=log.length; x--; ) {
			if ( api.IsFile(`${log[x]}/BlackOcean.json`) ) {
				let opt = require(`${log[x]}/BlackOcean.json`).SCompiler;

				(function dec( number ) {
					require('http')
					.get({ hostname:opt.host||'0.0.0.0', port:opt.port||8080, path:'/stop', method:'GET' }, res=>{
						const {statusCode} = res;

						if      ( statusCode==200 ) dec(0);
						else if ( number<5        ) dec(number+1);
					})
					.on('error', x=>{
						if ( number>5 ) run();
						else            setTimeout(x=>dec(number+1), 100);
					});
				 })(0);
			}
		}
	});
 }
const Stop     = function( log         ) {
	return new sProm((run,err)=>{
		api.Write(`${__dirname}/connections.log`, '');

		if ( !log.length || (log.length===1&&log[0]=='') ) run();
		else                                               StopDesc(log).run(run).err(err);
	});
 }
const IsAlive  = function( path, debug ) {
	return new sProm((run,err)=>{
		const file=api.GetParsePath(path||'.', 'BlackOcean.json');

		if ( api.IsExist(file.path) ) {
			require('child_process').exec(`node '${__dirname+'/../server.js'}' '${path}' ${debug?'debug':'production'}`, { cwd:path });

			const soptions=new sOptions(file.path, file.dir, true);

			(function con( number ){
				require('http')
				.get({
					hostname: soptions.SCompiler.host||'0.0.0.0',
					port    : soptions.SCompiler.port||8080,
					path    : '/alive',
					method  : 'GET',
				}, res=>{
					const {statusCode} = res;

					if ( statusCode!=200 ) err(statusCode);
					else {
						api.Write(__dirname+'/connections.log', (api.Read(__dirname+'/connections.log')||'')+`\n${path}`);
						run();
					}
				})
				.on('error', x=>{
					if ( number>50 ) err(x);
					else             setTimeout(x=>con(number+1), 100);
				});
			})(0);
		}
		else err(errorFile.cmd());
	});
 }
const Start    = function( log         ) {
	let num=0;

	return new sProm((run,err)=>{
		if ( log.length==1 && log[0]==='' ){run(); return}

		log.forEach((x,i)=>{
			if ( x!='' ) {
				let file=api.GetParsePath(x||'.', 'BlackOcean.json');

				if ( api.IsExist(file.path) ) {
					IsAlive(file.dir, true)
					.run(x=>num>=log.length && run())
					.err(err);
				}
				else console.log(`%fr*%r ${errorFile}`.err('Error'));
			}

			num++;
		});
	});
 }
const Exec     = function(             ) {
	(prompt||new sPrompt).PlaySpinner(`%bb%fy %spinner Reiniciando servicios... %r`);

	let log = api.Read(`${__dirname}/connections.log`).split('\n');

	Stop(log)
	.run(x=>{
		Start(log)
		.run(x=>prompt.StopSpinner('%fg✓%r Servicios %fgreiniciando%r correctamente\n'))
		.err(x=>prompt.StopSpinner(`%fr✗%r ${x}`));
	})
	.err(x=>prompt.StopSpinner(`%fr✗%r ${x}`));
 }

/*Definicio*/
// module.exports.command = {
// 	command    : '-r restart',
// 	description: 'Reinicia el servicio de BlackOcean',
// 	func       : Exec,
// 	help       :
// 		`%fmrestart%r reinicia cualquier servicio de BlackOcean

// 		%fcuso:%r
// 		  %fc*%r bo %fmrestart%r (sin argumentos)

// 		  %cdNota:%r %fmrestart%r puede ser iniciado en el CLI solo escribiendo %fmrestart%r o %fm-r%r`.inf('Comando %bm%fwrestart%r%bb%fy [alias: -r]'),
//  }
