/*Declaraciones*/
const childp = require('child_process');

/*Funciones*/
const GetTypeProject = function( type          ) {
	switch (type.toLowerCase()) {
		case 'com':
		case 'comp':
		case 'comps':
		case 'component': return VALUE_COM;
		case 'app':
		case 'application': return VALUE_APP;
	}

	return VALUE_SPA;
 };
const GetData        = function( path          ) {
	return new sProm(run=>{
		const res = {
			isPrompt     : /*prompt==null*/false,
			projectName  : api.GetParsePath(path).base,
			description  : 'proyecto BlackOcean',
			version      : '1.0.0',
			gitRepository: '',
			keywords     : 'black, ocean',
			license      : 'MIT',
			ipLocalhost  : 'No',
			typeProject  : VALUE_SPA,
			host         : 'http://localhost',
			appPort      : '8080',
			resPort      : '8081',
			apiPort      : '8082',
			author       : (function(){
				try {
					return (
						childp.execSync('git config --get user.name' ).toString().trim() +' <'+
						childp.execSync('git config --get user.email').toString().trim() +'>'
					 );
				} catch (e) {}

				return '';
			 })(),
		 };

		(new sPrompt)
		.Message(' Tipo de Proyecto: ', res.typeProject).run((ans,str)=>res.typeProject=GetTypeProject(ans))
		.Message(' Descripción: '     , res.description).run((ans,str)=>res.description=ans.toLowerCase())
		.Message(' Autor: '           , res.author     ).run((ans,str)=>res.author     =ans.toLowerCase())
		.Message(' Varsion: '         , res.version    ).run((ans,str)=>res.version    =ans.toLowerCase())
		.Message(' Host: '            , res.host       ).run((ans,str)=>res.host       =ans.toLowerCase())
		.Message(' Puerto de APP: '   , res.appPort    ).run((ans,str)=>res.appPort    =ans.toLowerCase())
		.Message(' Puerto de RES: '   , res.resPort    ).run((ans,str)=>res.resPort    =ans.toLowerCase())
		.Message(' Puerto de API: '   , res.apiPort    ).run((ans,str)=>res.apiPort    =ans.toLowerCase())
		.run(x=>run(res));
	});
 };
const GetBOJSON      = function( options       ) {
	let ipp = options.host;

	if ( options.ipLocalhost.indexOf('n')==-1 && options.ipLocalhost.indexOf('o')==-1 ) {
		let oss = require('os');
		let int = oss.networkInterfaces();

		for ( let key in int ) {
			let adr = int[key];

			for ( let i=adr.length; i--; )
				if ( adr[i].internal===false && adr[i].family==='IPv4' )
					ipp = adr[i].address;
		}
	}

	return `{
	    -Project  : {
	        type       : '${options.typeProject}',
	        author     : '${options.author}',
	        version    : '${options.version}',
	        description: '${options.description}',
	     },
	    -Variables: {
	        COM: '${options.host}:${options.appPort}',
	        APP: '${options.host}:${options.appPort}',
	        RES: '${options.host}:${options.resPort}',
	        API: '${options.host}:${options.apiPort}',
	     },
	    -Compiler : {
	        protocol  : 'http',
	        cert      : {
	            key : './cert/bo_ejemplo_key.pem',
	            cert: './cert/bo_ejemplo_crt.pem',
	         },
	        host      : '${options.host.replace(/http(s?):\/\//g, '')}',
	        port      : ${options.appPort},
	        pathSave  : './build',
	        pathPublic: './public',
	     },
	    -Resources: {
	        protocol  : 'http',
	        host      : '${options.host.replace(/http(s?):\/\//g, '')}',
	        port      : ${options.resPort},
	        pathPublic: './resources',
	        extensions: ['.png','.ico','.jpg']
	     },
	    -Api      : {
	        protocol  : 'http',
	        host      : '${options.host.replace(/http(s?):\/\//g, '')}',
	        port      : ${options.apiPort},
	        pathPublic: './api',
	        extensions: ['.json'],
	     },
	    production: {
	        -Variables: {
	            VAE: process.env.varEjemplo,
	            COM: 'https://BlackKraken.run',
	            APP: '.',
	            RES: '.',
	            API: '.',
	         },
	        -Compiler : {
	            watchFiles: false,
	            pathSave  : './build',
	            ignore    : {
	                ext: ['.DS_Store'],
	                dir: ['/example/path'],
	                fil: ['/example/path.not'],
	             },
	         },
	        -Resources: {
	            watchFiles: false,
	            pathSave  : './build/resources',
	         },
	     },
	 }`.replace(/[\t]/g, '');
 }
const CopyProto      = function( path, options ) {
	const pat=api.GetParsePath(path);
	const por=api.GetParsePath(__dirname+`/../../bin/template${options.typeProject===VALUE_COM?'-comp':''}`);
	const flo=vpath=>{
		if ( vpath=='' ) return;

		let sta=api.IsExist(vpath);

		if      ( !sta               ) return;
		else if (  sta.isDirectory() ) api.Travel(vpath).forEach(x=>flo(x.path));
		else if (  sta.isFile     () ) {
			let tem = api.GetParsePath(vpath.replace(por.path, pat.path));

			if ( tem.base[0]=='.' ) return;

			let con=api.Read(vpath, false);

			if ( vpath.toLowerCase().indexOf('blackocean.bj')>-1 )
				con=GetBOJSON(options);

			api.CreateDirectory(tem.dir);
			api.Write(tem.path, con);
		}
	 }

	flo(por.path);
 }
const Exec           = function( path          ) {
	if ( path==null ) path='.';

	path=api.GetParsePath(path).path;

	api.IsDirectory(path) &&
	GetData(path).run(res=>{
		CopyProto(path,res);

		console.log(
			`  %fy*%r Para iniciar el proyecto solo escribe: '%fmbo start%r' o solo '%fmstart%r' Estando en el CLI
			  %fy*%r Para parar el proyecto solo escribe: '%fmbo stop%r' o solo '%fmstop%r' Estando en el CLI
			  %fy*%r Puedes leer más sobre BlackOcean en https://BlackOcean.run/docs`
			.inf('BlackOcean se inicializo correctamente').replace(/\n$/g, ''));

		prompt.Close();
	 });
 }

/*Definicion*/
module.exports.command = {
	unique     : true,
	command    : '-i init <dir$>',
	description: 'Generador de proyectos según la plantilla general',
	error      : '%fminit%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo',
	func       : Exec,
	help       :
		`%fminit%r inicializa un proyecto según la plantilla de BlackOcean

		%fcuso:%r
		  %fc*%r bo %fminit%r (sin argumentos en el directorio de trabajo)
		  %fc*%r bo %fminit%r <carpeta donde se inicializara el proyecto BlackOcean>

		  %cdNota:%r %fminit%r puede ser iniciado en el CLI solo escribiendo %fminit%r o %fm-i%r`.inf('Comando %bm%fwinit%r%bb%fy [alias: -i]'),
 }
