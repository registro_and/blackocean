/*Funciones*/
const ExecDesc = function( log ) {
	return new sProm((run,err)=>{
		let opt=new sOptions;

		log.sForEach((v,k)=>{
			opt.Path(api.GetParsePath(k, 'BlackOcean.bj').path, v)
			.run(o=>{
				(function dec( number ) {
					require('http')
					.get({
						hostname: o.Compiler.host||'0.0.0.0',
						port    : o.Compiler.port||8080,
						path    : '/stop',
						method  : 'GET'
					}, res=>{
						const {statusCode} = res;

						if      ( statusCode==200 ) dec(0);
						else if ( number<5        ) dec(number+1);
					})
					.on('error', x=>{
						if ( number>5 ){prompt.StopSpinner('%fg✓%r Servicio %frterminado%r correctamente\n');run(log)}
						else            setTimeout(x=>dec(number+1), 100);
					});
				 })(0);
			});
		});
	});
 }
const Exec     = function(     ) {
	(new sPrompt).PlaySpinner(`%bb%fy %spinner Terminando servicio... %r`);

	let log;

	try       {log=JSON.parse( api.Read(`${__dirname}/connections.json`) );}
	catch (e) {log={};}

	ExecDesc(log)
	.run(x=>prompt.StopSpinner('%fg✓%r Servicios %frterminado%r correctamente\n'))
	.err(x=>prompt.StopSpinner(`%fr✗%r ${x}`));
 }

/*Definicion*/
module.exports.command = {
	command    : '-p stop',
	description: 'Termina el servicio de BlackOcean',
	func       : Exec,
	help       :
		`%fmstop%r termina cualquier servicio de BlackOcean

		%fcuso:%r
		  %fc*%r bo %fmstop%r (sin argumentos)

		  %cdNota:%r %fmstop%r puede ser iniciado en el CLI solo escribiendo %fmstop%r o %fm-p%r`.inf('Comando %bm%fwstop%r%bb%fy [alias: -p]'),
 };
