#! /usr/bin/env node
require('./main.js');

process.title = 'BlackOcean';

/*Variables*/
let   rop;
const opc=new sOptions, sec=new sServerBO, ser=new sServerBO, sea=new sServerBO;

opc.eve({
	onLoad:x=>{
		rop=x;
		sec.Connect(rop.Compiler);
	}
});
sec.eve({ onLoad:x=>ser.Connect(rop.Resources) });
ser.eve({ onLoad:x=>sea.Connect(rop.Api      ) });

opc.Path( api.GetParsePath(process.argv[2]||'.', 'BlackOcean.bj').path );
