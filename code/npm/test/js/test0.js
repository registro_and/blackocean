'use strict';

/*Variables*/
window.__ids__     = 0;
window.__orderbo__ = 0;
window.coms        = {};
window.gmain       = 0;
window.gcache      = 0;
const VALUE_FILE   = 'file',
	VALUE_FOLDER   = 'folder',
	VALUE_INSERT   = 'insert',
	VALUE_UPDATE   = 'update',
	VALUE_DELETE   = 'delete',
	VALUE_GETTER   = 'getter',
	VALUE_FUNCTION = 'function',
	VALUE_OBJECT   = 'object',
	VALUE_ARRAY    = 'array',
	VALUE_STRING   = 'string',
	VALUE_NUMBER   = 'number',
	VALUE_FLOAT    = 'float',
	VALUE_BOOL     = 'bool',
	VALUE_INT      = 'int',
	VALUE_PASSWORD = 'password',
	VALUE_SPECIAL  = 'special',
	VALUE_HTML     = 'html',
	VALUE_CSS      = 'css',
	VALUE_JS       = 'js',
	VALUE_TAXONOMY = 'taxonomy',
	VALUE_JSON     = 'json',
	VALUE_TEXT     = 'text',
	VALUE_BIG      = 'big',
	VALUE_MED      = 'med',
	VALUE_MIN      = 'min',
	VALUE_SYNC     = 'sync',
	VALUE_ASYNC    = 'async',
	VALUE_NULL     = 'null',
	VALUE_WARN     = 'warn',
	VALUE_ERROR    = 'error',
	VALUE_VALID    = 'valid',
	VALUE_QUERY    = 'query',
	VALUE_ACCEPT   = 'accept',
	VALUE_NOTHING  = 'nothing';
const CACHE_STRUCT = [
	{ name:'cache_ajax'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_exec'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}, order:{} } },
	{ name:'cache_json'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_taxonomy', cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_images'  , cams:{ requestID:{ unique:true }, data:{}                    } },
 ];

/*Declaraciones*/
const o=( select )=>new sObject(select),
	tipode      =( type             )=>Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase(),
	Hash        =( data             )=>{/* CREACION DE HASH hash */
		let hash=5381, num=data.length;

		while(num)hash=(hash*33)^data.charCodeAt(--num);

		return hash>>>0;
	 },
	Mime        =( type             )=>{/* REGRESA EL TIPO MIME DE UN TIPO DE ARCHIVO */
		switch (type.toLowerCase()) {
			case 'js'  : return 'application/javascript; charset=UTF-8';
			case 'bj'  : return 'application/javascript; charset=UTF-8';
			case 'css' : return 'text/css; charset=UTF-8';
			case 'ico' : return 'image/x-icon';
			case 'jpg' : return 'image/jpeg';
			case 'png' : return 'image/png';
			case 'svg' : return 'image/svg+xml';
			case 'woff': return 'font/woff';
			case 'html': return 'text/html; charset=UTF-8';
			case 'json': return 'application/javascript; charset=UTF-8';
			case 'txt' :
			default    : return 'text/plain';
		}
	 },
	ParseUrl    =( url              )=>{/* ENTREGAR PATH DE LA URL */
		if ( typeof url!=VALUE_STRING ) url = document.URL;

		window.__pathUrl__ = new URL(url);

		let eur=/\b([\w\n\.\-\%]+)\b/g;
		let par=window.__pathUrl__.search, tur=window.__pathUrl__.pathname, mat;

		/*URL*/
		let dat=[];
		while ( (mat=eur.exec(tur))!==null ) {
			mat.index===eur.lastIndex && eur.lastIndex++;
			mat.forEach((match, gindex)=>gindex===1 && dat.push(match));
		}
		window.__pathUrl__.path = dat;

		/*Params*/
		let tem='', pas=[];
		while ( (mat=eur.exec(par))!==null ) {
			mat.index===eur.lastIndex && eur.lastIndex++;
			tem=='' ? tem=decodeURI(mat[0]) : (pas.push({[tem]:decodeURI(mat[0])}), tem='');
		}
		window.__pathUrl__.params = pas;

		return window.__pathUrl__;
	 },
	Ajax        =( url, params      )=>{/* RECIBIR ARCHIVO DEL SERVIDOR CON AJAX */
		return new sProm((run,err)=>{
			params   =Object.assign({ id:url, method:'GET', body:{}, heads:{}, timeout:0, format:VALUE_TEXT, cache:false, mime:Mime('txt'), type:'txt'}, params);
			const aja=x=>{
				let xhr             = new XMLHttpRequest();
				let dat             = '';
				xhr.withCredentials = false;
				xhr.timeout         = params.timeout;
				xhr.responseType    = params.format;
				xhr.onloadend       = function() {
					if ( this.readyState>3 ) {
						if ( this.status===200 ) {
							if ( params.cache && gcache ) {
								let dat=this.response;
								if ( params.format==VALUE_JSON && typeof dat!=VALUE_STRING ) dat=JSON.stringify(dat);
								gcache.Add('cache_ajax',{ requestID:params.id, data:this.response, hash:Hash(dat) } );
							}

							run(this.response, this);
						}
						else err(this.response, this);
					}
				 };

				xhr.open(params.method, url, true);
				params.heads.sForEach((x,y)=>xhr.setRequestHeader(y,x));

				if      ( typeof params.body ==VALUE_STRING ) dat = params.body;
				else if ( tipode(params.body)==VALUE_OBJECT ) params.body.sForEach((x,y)=>dat+=y+'='+x+'&');

				xhr.send(dat);
			 };

			if ( gcache ){
				gcache.Get('cache_ajax', 'requestID', params.id)
				.run(x=>{
					if ( x ) {
						let nam = url.split('/');
						nam     = (nam[nam.length-1]||'').replace(/[\.-]/g,'_').toLowerCase();

						if ( CACHE_HASH[nam]==x.hash ) run(x.data);
						else                           aja();
					}
					else     aja();
				 })
				.err(e=>console.error(e));
			 }
			else aja();
		});
	 },
	Eval        =( params           )=>{/* EVALUAR SCRIPT */
		params = Object.assign({ id:'', type:VALUE_JS, data:'', src:'' }, params);

		if ( document.getElementById(params.id) ) return;

		if ( params.type==VALUE_CSS ) {
			let style = document.createElement('style');
			style.id  = params.id;

			style        .appendChild(document.createTextNode(params.data));
			document.head.appendChild(style);
		}
		else if ( params.type==VALUE_JS ) {
			let script  = document.createElement('script');
			script.id   = params.id;
			params.src  && (script.src =params.src);
			params.data && (script.text=params.data);

			document.head.appendChild(script);
		}
	 },
	Exec        =( clase, params    )=>{/* EJECUCION DE MODULOS */
		return new sProm(function(run,err) {
			params  = Object.assign({ parent:'body' }, params);
			let ord = window.__orderbo__++;
			let par = params.parent; delete params.parent;
			let exe = ()=>{
				if ( params.noExec===true ) return;

				params.css = Object.assign({ order:ord }, params.css);
				params.eve = Object.assign({ onExec:x=>{

					run(x.extra);
				 }}, params.eve);

				new (eval(clase))(par, params);
			 };
			let aja = ()=>{
				Ajax(typeof params.url==VALUE_STRING?`${params.url}/modules/${clase}.bj`:`[APP]/modules/${clase}.bj`, {cache:false}).run(x=>{
					if ( x[0]=='<' ){console.error('Error 405: Modulo no encontrado'); err('Error 405: Modulo no encontrado'); return}

					gcache&&gcache.Add('cache_exec', { requestID:clase, data:x, hash:Hash(x), order:ord }).err(err);
					Eval({ id:'exec_'+clase, data:x, type:VALUE_JS });
					exe();
				 }).err(e=>console.error(e) || err(e));
			 };

			if      ( typeof window[clase]==VALUE_FUNCTION ) exe();
			else if ( gcache                               ){
				gcache.Get('cache_exec', 'requestID', clase)
				.run(x=>{
					if ( x ) {
						let nam = (typeof params.url==VALUE_STRING?params.url:clase+'_bj').split('/');
						nam     = (nam[nam.length-1]||'').replace(/[\.-]/g,'_').toLowerCase();

						if ( CACHE_HASH[nam]==x.hash ) {
							Eval({ id:'exec_'+clase, data:x.data, type:VALUE_JS });
							exe();
						}
						else gcache.Del('cache_exec', x.id).run(aja).err(err);
					}
					else aja();
				 })
				.err(err);
			 }
			else aja();
		});
	 },
	ExecTaxonomy=( taxonomy, params )=>{/* CREACION DE COMPONENTES MEDIANTE TAXONOMIA */
		return new sProm(function(run,err) {
			if ( !params ) err('Error: no existe parent');

			let exe = arr=>{
				try {
					arr.forEach( (x,i)=>Exec(x.class, Object.assign({},params||{},x)).run(z=>run(z,i,arr.length)) );
				}
				catch(error) {
					console.log('Error al cargar la taxonomía:', error);
				}
			 };
			let aja = ( )=>{
				Ajax(`[APP]/taxonomy/${taxonomy}.bj`, {cache:false}).run(x=>{
					gcache&&gcache.Add('cache_taxonomy', { requestID:taxonomy, data:x, hash:Hash(x) }).err(e=>console.error(e));
					exe(window[taxonomy]=eval(x));
				 }).err(e=>console.error(e));
			 };

			if      ( tipode(taxonomy        )==VALUE_ARRAY ) exe(taxonomy);
			else if ( tipode(window[taxonomy])==VALUE_ARRAY ) exe(window[taxonomy]);
			else if ( gcache                                ) {
				gcache.Get('cache_taxonomy', 'requestID', taxonomy)
				.run(x=>{
					if ( x ) {
						let nam = taxonomy.toLowerCase() +'_bj';

						if ( CACHE_HASH[nam]==x.hash ) exe(window[taxonomy]=eval(x.data));
						else gcache.Del('cache_taxonomy', x.id).run(aja).err(x=>console.error(x));
					}
					else aja();
				 })
				.err(e=>console.error(e));
			 }
			else aja();
		});
	 };

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , { enumerable:false, value:function( func, result ){
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});
Object.defineProperty(Object.prototype, 'sForEach', { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) func.call(this, this[x], x, ind++);
 }});
Object.defineProperty(Number.prototype, 'sFormat' , { enumerable:false, value:function( format       ){
	if ( format=='$' || format==',' ) {
		let num         = Math.round(parseFloat(this) * Math.pow(10, 2)) / Math.pow(10, 2);
		let regx        = /(\d+)(\d{3})/;
		let numSeparado = num.toString().split('.');
		let enteros     = numSeparado[0];
		let decimales   = numSeparado.length>1 ? '.'+numSeparado[1] : '.00';

		decimales = decimales + '00';
		decimales = decimales.substr(0, 3);

		while ( regx.test(enteros) ) {
			enteros = enteros.replace(regx, '$1,$2');
		}

		return enteros + (format=='$'?decimales:'');
	}
	else {
		let num = String(this);

		while ( num.length<format.length ) {
			num = '_' + num;
		}

		for ( let i=0; i<format.length; i++ ) {
			if ( format[i]==='0' && num[i]==='_' ) {
				num = num.replace(/_/, 0);
			}
		}

		return num;
	}
 }});
Object.defineProperty( Array.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let res=null;

	for ( let x=this.length; x--; ) {
		res = func.call(this, this[x], x);

		if ( res ) return res;
	}
 }});

/*Clases*/
function sProm     ( parent, func  ) {
	/*Variables*/
	const gthis   = this;
	gthis.__sPromRun__ = [];
	gthis.__sPromErr__ = 0;
	gthis.__sPromAll__ = 0;
	gthis.__sPromTis__ = 0;
	gthis.fwait        = [];
	gthis.sClass       = 'sProm';

	/*Inicio*/
	if ( typeof parent==VALUE_FUNCTION ) {
		func   = parent;
		parent = undefined;
	}
	else for ( let x in parent ) {
		if ( typeof parent[x]==VALUE_FUNCTION )
			this[x]=(...params)=>ExecFunc(x, params);
	}

	/*Metodos*/
	gthis.run=( fnew )=>{gthis.__sPromRun__.push(fnew); return gthis; };
	gthis.err=( fnew )=>{gthis.__sPromErr__ = fnew;     return gthis; };
	gthis.all=( fnew )=>{gthis.__sPromAll__ = fnew;     return gthis; };
	gthis.tis=( para )=>{gthis.__sPromTis__ = para;     return gthis; };

	/*Funciones*/
	function Run( ...params ) {
		while ( gthis.__sPromRun__.length ) {
			if ( typeof gthis.__sPromRun__[0]==VALUE_FUNCTION ) {
				let res = gthis.__sPromRun__[0](...params);

				if ( res && res.sClass && res.sClass=='sProm' ) {
					res.err(gthis.__sPromErr__);
					gthis.__sPromRun__.splice(0, 1);
					for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
					break;
				}
			}
			else if ( tipode(gthis.__sPromRun__[0])==VALUE_OBJECT && parent ) {
				if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION || typeof parent[gthis.__sPromRun__[0].func]==VALUE_FUNCTION ) {
					let res;

					if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION ) res = gthis.__sPromRun__[0].func(gthis.__sPromRun__[0].params);
					else                                                     res = parent[gthis.__sPromRun__[0].func](...gthis.__sPromRun__[0].params);

					if ( res!=undefined ) {
						if ( res.sClass && res.sClass=='sProm' ) {
							res.err(gthis.__sPromErr__);
							gthis.__sPromRun__.splice(0, 1);
							for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
							break;
						}
						else {
							for ( let x=gthis.__sPromRun__.length; x--; ) {
								if      ( typeof gthis.__sPromRun__[x]     ==VALUE_FUNCTION ) gthis.__sPromRun__[x]        = { func:gthis.__sPromRun__[x], params:res };
								else if ( typeof gthis.__sPromRun__[x].func==VALUE_FUNCTION ) gthis.__sPromRun__[x].params = res;
							}
						}
					}
				}
			}

			gthis.__sPromRun__.splice(0, 1);
		}

		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }
	function Err( ...params ) {
		if ( typeof gthis.__sPromErr__==VALUE_FUNCTION ) gthis.__sPromErr__(...params);
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION ) gthis.__sPromAll__(...params);
	 }
	function All( ...params ) {
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }

	function ExecFunc( func, params ) {
		if ( params[0]==VALUE_ASYNC ){
			params.splice(0, 1);

			return parent[func](...params);
		}
		else {
			gthis.fwait.push(func);
			gthis.__sPromRun__.push({ func:func, params:params });
		}

		return gthis;
	 }

	/*Inicio*/
	setTimeout(func.bind(gthis, Run, Err, All), 10);
 }
function sObject   ( select        ) {
	/*Variables*/
	const gthis = this;

	/*Creacion*/
	gthis.constructor = function( _select, _props ) {/* CONSTRUCTOR */
		if ( select==undefined && _select==undefined ) return;

		Constructor.call(this, _select||select);

		typeof this.id   ==VALUE_STRING   &&
		typeof this.style==VALUE_FUNCTION &&
		typeof this.html ==VALUE_FUNCTION &&
		this.style(_props&&_props.style||'').html(_props&&_props.html||'').rem('#'+this.id) &&
		typeof this.idsView==VALUE_FUNCTION && this.idsView();

		if ( tipode(_props)==VALUE_OBJECT ) {
			delete _props.style;
			delete _props.html;

			if ( _props.eve ) {
				gthis.eve.call(this, _props.eve);

				delete _props.eve;
			}
		}

		this.tri.call(this, 'onExec'       , this);
		this.tri.call(this, 'onConstructor', this);

		if ( this.sClass!=gthis.sClass ) {
			if ( !window.coms[this.sClass] ) {
				window.coms[this.sClass] = this;
			}
			else {
				for ( var x=2; window.coms[this.sClass+'_'+x]; x++ );

				window.coms[this.sClass+'_'+x] = this;
			}
		}

		return this;
	 };
	gthis.destructor  = function(                 ) {/* DESTRUCTOR */
		if ( this.eve )
			this.eve(false);//.del();
	 };

	/*Metodos*/
	gthis.add = function( html, after      ) {/* AGREGAR ELEMENTO */
		if ( !this.length ) return this;

		let edit=( element, after )=>{
			for ( let x=0; x<this.length; x++ ) {
				if ( after==undefined || after!=false )
					while ( element.childNodes.length )
						this[x].appendChild(element.childNodes[0]);

				else while ( element.childNodes.length>0 )
					this[x].insertBefore(element.childNodes[0], this[x].childNodes[0]);
			}
		 };

		if ( typeof html==VALUE_STRING && html!='' ) {
			let
			div           = document.createElement('div');
			div.innerHTML = html;

			edit.call(this, div, after);
		}
		else if ( tipode(html)==VALUE_OBJECT && html.childNodes )
			edit.call(this, html, after);

		return this;
	 };
	gthis.att = function( attr, value      ) {/* ATRIBUTOS */
		if ( !this.length ) return this;

		let edit=( att, val )=>{
			if ( val===undefined ) return this[0].getAttribute(att);
			else {
				if ( val===false ) for(let x=this.length; x--;) this[x].removeAttribute(att);
				else               for(let x=this.length; x--;) this[x].setAttribute(att,val);
			}
		 };

		if ( typeof attr==VALUE_STRING ) {
			if ( value===undefined ) return edit.call(this, attr       );
			else                            edit.call(this, attr, value);
		}
		else if ( tipode(attr)==VALUE_OBJECT ) for(let x in attr) edit.call(this,x,attr[x]);

		return this;
	 };
	gthis.del = function( select           ) {/* BORRAR ELEMENTOS */
		if ( !this.length ) return this;

		if ( select==undefined ) {
			for ( let x=this.length; x--; ) {
				EditEvent(this[x], false);
				this[x].parentNode.removeChild(this[x]);
			}

			this.splice(0, this.length);
		}
		else if ( typeof select==VALUE_STRING ) this.fin(select).del();

		return this;
	 };
	gthis.fin = function( select, extra    ) {/* BUSCAR HIJOS */
		if ( !this.length ) return [];

		let nodl, elem=new sObject();

		select===undefined && (select='*');

		for ( let x=this.length; x--; ) {
			nodl = this[x].querySelectorAll(select);

			for( let y=nodl.length; y--; elem.unshift(nodl[y]) );
		}

		if ( tipode(extra)==VALUE_OBJECT ) {
			for ( let x in extra ) elem[x]=extra[x];
		}

		return elem;
	 };
	gthis.cla = function( sclass, sdelete  ) {/* EDITAR CLASE */
		if ( !this.length ) return this;

		if ( typeof sclass==VALUE_STRING && sclass!='' ) {
			if ( sdelete==false ) {
				sclass = new RegExp(sclass, 'g');

				for ( let x=this.length; x--; ) {
					if      ( typeof this[x].className ==VALUE_STRING ) this[x].className         = this[x].className        .replace(sclass, '').replace(/  /g, ' ');
					else if ( tipode(this[x].className)==VALUE_OBJECT ) this[x].className.baseVal = this[x].className.baseVal.replace(sclass, '').replace(/  /g, ' ');
				}
			}
			else {
				for ( let x=this.length; x--; ) {
					if ( typeof this[x].className==VALUE_STRING ) {
						if ( this[x].className.indexOf(sclass)==-1 )
							this[x].className+= ' ' + sclass;
					}
					else if ( tipode(this[x].className)==VALUE_OBJECT ) {
						if ( !this[x].className.baseVal.indexOf(sclass)==-1 )
							this[x].className.baseVal+= ' ' + sclass;
					}
				}
			}
		}
		else if ( sclass===false )  {
			for ( let x=this.length; x--; ) {
				if      ( typeof this[x].className ==VALUE_STRING ) this[x].className         = '';
				else if ( tipode(this[x].className)==VALUE_OBJECT ) this[x].className.baseVal = '';
			}
		}
		else if ( sclass===undefined ) return this[0].className;

		return this;
	 };
	gthis.css = function( property, value  ) {/* ESTILOS CSS */
		if ( !this.length ) return this;

		let edit=( property, value )=>{
			if (
				typeof value==VALUE_NUMBER &&
				property!='opacity' &&
				property!='zIndex'  &&
				property!='z-index' &&
				property!='order'
			) value+= 'px';

			for ( let x=this.length; x--; )
				this[x].style[property]!=undefined && (this[x].style[property]=value);
		 };

		if      ( typeof property==VALUE_STRING && value==undefined ) return window.getComputedStyle(this[0])[property];
		else if ( typeof property==VALUE_STRING && value!=undefined ) edit(property, value);
		else if ( tipode(property)==VALUE_OBJECT                    ) for(let x in property) edit(x, property[x]);

		return this;
	 };
	gthis.eve = function( events, idDelete ) {/* EVENTOS */
		if ( this.length==0 ) return this;

		if ( events===undefined ) {
			return this.map(x=>EditEvent(x));
		}
		else if ( typeof events==VALUE_STRING && idDelete===undefined )  {
			return this.map(x=>EditEvent(x, events));
		}
		else for(let x=this.length;x--;) EditEvent(this[x], events, idDelete);

		return this;
	 };
	gthis.htm = function( html             ) {/* HTML INTERNO */
		if ( this.length==0 ) return this;

		if ( html===undefined ) return this[0].innerHTML;
		else                    for(let x=this.length;x--;) this[x].innerHTML=html;

		return this;
	 };
	gthis.pos = function( size             ) {/* TAMAÑO Y POSICION */
		if ( !this.length ) return this;

		if ( size===undefined ) {
			let re1 = this[0].getBoundingClientRect();
			let re2 = this[0].offsetParent!=null ?
				this[0].offsetParent.getBoundingClientRect():
				{top:0, left:0, right:0, bottom:0, width:0, height:0};

			return {
				top   : re1.top    - re2.top,
				left  : re1.left   - re2.left,
				right : re1.right  - re2.right,
				bottom: re1.bottom - re2.bottom,
				width : re1.width,
				height: re1.height,
			};
		}
		else if ( size=='off' ) {
			let re1 = this[0].getBoundingClientRect();

			return {
				top   : re1.top,
				left  : re1.left,
				right : re1.right,
				bottom: re1.bottom,
				width : re1.width,
				height: re1.height,
			};
		}
		else if ( tipode(size)==VALUE_OBJECT ) {
			this.css(size);
		}

		return this;
	 };
	gthis.par = function(                  ) {/* OBTENER PADRE */
		if ( this.length==0 ) return this;

		let ele = new sObject();

		ele.push(this[0].parentNode);

		return ele;
	 };
	gthis.tri = function( event, extra     ) {/* GENERAR EVENTO */
		if ( this.length==0 ) return this;

		if ( typeof event==VALUE_STRING && event!='' ) {
			let evt = document.createEvent('HTMLEvents');

			evt.initEvent(event, false, true);

			evt.extra = extra;

			for ( let x=this.length; x--; ) this[x].dispatchEvent(evt);
		}

		return this;
	 };
	gthis.wra = function( html             ) {/* AGREGAR ELEMENTO ENVOLVENTE */
		if ( this.length==0 ) return this;

		if ( typeof html==VALUE_STRING && html!='' ) {
			for ( let x=this.length-1; x>=0; x-- ) {
				let div = document.createElement('div'); div.innerHTML = html;
				let hij = div.childNodes[0];

				while ( this[x].childNodes.length )
					hij.appendChild(this[x].childNodes[0]);

				this[x].appendChild(hij);

				div = null;
			}
		}

		return this;
	 };
	gthis.mov = function( select, after    ) {/* MUEVE UNA ESTRUCTURA A OTRO NODO */
		if ( !this.length ) return this;

		if ( typeof select==VALUE_STRING ) {
			let obj = document.querySelectorAll(select)[0];

			if ( after==undefined || after ) for(let x=this.length;x--;) obj.appendChild(this[x]);
			else                             for(let x=this.length;x--;) obj.insertBefore(this[x], obj.childNodes[0]);
		}
		else if ( typeof select.sClass==VALUE_STRING ) {
			if ( after==undefined || after ) for(let x=this.length;x--;) select[0].appendChild(this[x]);
			else                             for(let x=this.length;x--;) select[0].insertBefore(this[x], select[0].childNodes[0]);
		}

		return this;
	 };
	gthis.exe = function( method           ) {/* EJECUTAR FUNCION */
		if ( this.length==0 ) return this;

		if ( typeof method==VALUE_STRING ) {
			let tempor ;
			let result = [];

			[].splice.call(arguments, 0, 1);

			for ( let x=this.length; x--; ) {
				if ( typeof this[x][method]==VALUE_FUNCTION ) {
					tempor = this[x][method].apply(this[x], arguments);

					if ( tempor ) result.push(tempor);
				}
				else {
					if ( arguments.length>0 ) this[x][method] = arguments[0];
					else                      result.push(this[x][method]);
				}
			}

			if      ( result.length==1 )  return result[0];
			else if ( result.length> 1 )  return result;
		}
		else if ( tipode(method)==VALUE_OBJECT ) {
			for ( let x in method ) {
				if ( this[x]!=undefined ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(method[x])==VALUE_ARRAY ) this[x].apply(this, method[x]);
						else                                  this[x](method[x]);

						delete method[x];
					}
				}
				else this[x] = method[x];
			}
		}

		return this;
	 };
	gthis.rem = function( select, props    ) {/* REMPLAZAR HIJOS */
		if ( this.length==0 ) return this;

		Constructor.call(this, select, props);

		return this;
	 };
	gthis.run = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( this.length==0 || typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	gthis.err = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( this.length==0 || typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	gthis.all = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( this.length==0 || typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };

	/*Funciones*/
	function Constructor( select                    ) {/* SELECTOR - ARRAY */
		this.length>0 && this.splice(0, this.length);

		/*Inicio*/
		let type = tipode(select);

		if ( type==VALUE_STRING ) {
			let nodl = document.querySelectorAll(select);

			for( let x = nodl.length; x--; this.unshift(nodl[x]) );
		}
		else if ( type==VALUE_ARRAY ) {
			for ( let x=select.length; x--; )
				this.push(x);
		}
		else if ( type==VALUE_OBJECT ) {
			for ( let x in select )
				this.push(select[x]);
		}
		else if ( (/^(html)/g.test(type) && /(element)$/g.test(type)) || type=='htmldocument' ) {
			this.push(select);
		}
		else if ( (/^(svg)/g.test(type) && /(element)$/g.test(type)) || type=='svgsvgelement' ) {
			this.push(select);
		}
	 }
	function EditEvent  ( element, events, idDelete ) {/* EDICION DE EVENTOS */
		/*ELIMINAR TODOS LOS EVENTOS*/
		if ( events==false ) {
			if ( element.bcEvents ) {
				element.bcEvents.forEach(y=>element.removeEventListener(y.event, y.func));

				delete element.bcEvents;
			}
		 }

		/*VER LISTA DE EVENTOS*/
		else if ( events==undefined ) return element.bcEvents||[];

		/*LISTA DE EVENTOS CON ID*/
		else if ( typeof events==VALUE_STRING ) {
			/*ELIMINAR EVENTOS CON ID*/
			if ( idDelete==false ) {
				if ( element.bcEvents ) {
					for ( let y=element.bcEvents.length-1; y>=0; y-- ) {
						if ( element.bcEvents[y].id===events ) {
							element.removeEventListener(element.bcEvents[y].event, element.bcEvents[y].func);
							element.bcEvents.splice(y, 1);
						}
					}
				}
			 }

			/*VER LISTA DE EVENTOS CON ID*/
			else {
				let lis = [];
				if ( element.bcEvents ) element.bcEvents.forEach(x=>x.id===events && lis.push(x));
				return lis;
			 }
		 }

		/*MULTIPLES EVENTOS*/
		else if ( tipode(events)==VALUE_OBJECT ) {
			/*ELIMINAR EVENTO ESPESIFICO*/
			if ( idDelete==false ) {
				if ( element.bcEvents ) {
					events.forEach((x,y)=>{
						for ( let z=element.bcEvents.length-1; z>=0; z-- ) {
							if ( element.bcEvents[z].event===y && element.bcEvents[z].func===x ) {
								element.removeEventListener(element.bcEvents[z].event, element.bcEvents[z].func);
								element.bcEvents.splice(z, 1);
							}
						}
					});
				}
			 }

			/*AGREGAR EVENTOS*/
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';

				if ( !element.bcEvents ) element.bcEvents = [];

				events.sForEach((x,y)=>{
					if ( y!='id' ) {
						element.bcEvents.push({ id:events.id, event:y, func:x });
						element.addEventListener(y, x);
					}
				});
			 }
		 }
	 }

	/*Inicio*/
	this.sSuper = this;
	this.sClass = 'sObject';
	this.constructor(select);
 }sObject.prototype = new Array;
function sDomVisual( select, props ) {
	/*Variables*/
	const gthis = this;
	const va    = {sSuper:this.sSuper};
	gthis.id    = 'i'+(__ids__++);

	/*Creacion*/
	gthis.idsView     = function(                 ) {};
	gthis.html        = function( _html, _class   ) {
		this.add(`<${this.sClass} id="${this.id}" class="${_class||this.sClass}">${_html||''}</${this.sClass}>`);
		return this;
	 };
	gthis.style       = function( _style          ) {
		Eval({ id:'style_'+this.sClass, data:_style||'', type:VALUE_CSS });

		return this;
	 };
	gthis.constructor = function( _select, _props ) {
		va.sSuper.constructor.call(this, _select||select, (_props||props));
		va.sSuper.exe.call(this, (_props||props));
		return this;
	 };
	gthis.destructor  = function(                 ) {
		for(let x in window.coms)
			if ( window.coms[x].id===this.id )
				delete window.coms[x];

		gthis.tri.call(this, 'onDestructor', this);
		gthis.eve.call(this, false);
		gthis.del.call(this);
	 };

	/*Inicio*/
	this.sSuper = this;
	this.sClass = 'sDomVisual';
	this.constructor(select, props);
 }sDomVisual.prototype = new sObject;
function sNonVisual( props         ) {
	/*Variables*/
	const gthis = this;
	const va    = {events:[]};
	gthis.id    = 'i'+(__ids__++);

	/*Creacion*/
	gthis.constructor = function( _select, _props ) {
		if ( tipode(_props)==VALUE_OBJECT ) {
			if ( _props.eve ) {
				gthis.eve.call(this, _props.eve);

				delete _props.eve;
			}
		}

		this.tri.call(this, 'onExec'       , this);
		this.tri.call(this, 'onConstructor', this);

		if ( this.sClass!=gthis.sClass ) {
			if ( !window.coms[this.sClass] ) {
				window.coms[this.sClass] = this;
			}
			else {
				for ( var x=2; window.coms[this.sClass+'_'+x]; x++ );

				window.coms[this.sClass+'_'+x] = this;
			}
		}

		gthis.exe.call(this, (_props||props));

		return this;
	 };
	gthis.destructor  = function(                 ) {
		gthis.tri.call(this, 'onDestructor', this);
		gthis.eve.call(this, false);
	 };

	/*Metodos*/
	gthis.eve = function( events, idDelete ) {
		if      ( events===false              ) va.events=[];
		else if ( events===undefined          ) return va.events;
		else if ( typeof events==VALUE_STRING ) {
			if ( idDelete==false )
				for ( let y=va.events.length-1; y>=0; y-- )
					va.events[y].id===events&&va.events.splice(y, 1);

			else return va.events.filter(x.id===events);
		 }
		else if ( tipode(events)==VALUE_OBJECT ) {
			if ( idDelete==false ) {
				events.sForEach((x,y)=>{
					for ( let z=va.events.length-1; z>=0; z-- )
						if ( va.events[z].event===y && va.events[z].func===x )
							va.events.splice(z, 1);
				});
			 }
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';
				if ( !va.events                                   ) va.events = [];

				events.sForEach((x,y)=>y!='id'&&va.events.push({ id:events.id, event:y, func:x }));
			 }
		 }

		return this;
	 };
	gthis.tri = function( event , extra    ) {
		if ( typeof event==VALUE_STRING && event!='' )
			va.events.forEach(x=>x.event===event&&typeof(x.func)==VALUE_FUNCTION&&x.func.call(this,{extra}));

		return this;
	 };
	gthis.exe = function( method           ) {
		if ( tipode(method)==VALUE_OBJECT ) {
			for ( let x in method ) {
				if ( this[x]!=undefined ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(method[x])==VALUE_ARRAY ) this[x].apply(this, method[x]);
						else                                  this[x](method[x]);
					}
				}
				else this[x] = method[x];
			}
		}

		return this;
	 };
	gthis.run = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	gthis.err = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	gthis.all = function( func             ) {/* FINALIZACIÓN DE ANIMACIÓN */
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };

	/*Inicio*/
	this.sSuper = this;
	this.sClass = 'sNonVisual';
	this.constructor(props);
 }
function sCache    ( props         ) {
	/*Variables*/
	const gthis = this;
	const co    = {};

	/*Metodos*/
	gthis.Create = function(                            ) {
		try{CACHE_VERSION}catch(e){gthis.tri('onOpen');return;}

		if ( localStorage.CACHE_VERSION!=CACHE_VERSION ) {
			console.info('creacion de la cache');

			co.dbr          =indexedDB.deleteDatabase('BlackOcean.'+localStorage.CACHE_VERSION);
			co.dbr.onerror  =x=>gthis.tri('onError', 'Error al borrar la cache');
			co.dbr.onsuccess=x=>gthis.tri('onSuccess', 'la cache se ha borrado')&&CreateCache();

			localStorage.CACHE_VERSION = CACHE_VERSION;
		}
		else CreateCache();
	 };
	gthis.Clear  = function( cols                       ) {
		return new sProm(gthis,(run,err)=>{
			let res = co.dbp
			.transaction(cols, "readwrite")
			.objectStore(cols)
			.clear();

			res.onerror  =x=>err(x.target.result, x);
			res.onsuccess=x=>run(x.target.result, x);
		});
	 };
	gthis.Get    = function( cols, index, value         ) {
		return new sProm(gthis, (run, err)=>{
			if ( typeof index==VALUE_STRING ) {
				let res = co.dbp
				.transaction(cols, 'readonly')
				.objectStore(cols            )
				.index      (index           )
				.get        (value           );

				res.onerror  =x=>err(x.target.result, x);
				res.onsuccess=x=>run(x.target.result, x);
			}
			else {
				let arr=[], res=co.dbp
				.transaction(cols, 'readonly')
				.objectStore(cols            )
				.openCursor (                );

				res.onerror  =x=>err(x.target.result, x);
				res.onsuccess=x=>{
					if ( x.target.result ) {
						arr.push(x.target.result.value);
						x.target.result.continue();
					}
					else run(arr, res, x.target.result);
				 }
			}
		});
	 };
	gthis.Add    = function( cols,        value         ) {
		return new sProm(gthis, (run, err)=>{
			let res = co.dbp
			.transaction(cols, 'readwrite')
			.objectStore(cols             )
			.add        (value            );

			res.onsuccess=x=>run(x.target.result, x);
			res.onerror  =x=>{
				if ( !x.target.result ) run(x.target.result, x);
				else                    err(x.target.result, x);
			 }
		});
	 };
	gthis.Del    = function( cols, index, value         ) {
		return new sProm(gthis, (run, err)=>{
			let fde=(idd)=>{
				let res = co.dbp
				.transaction(cols, 'readwrite')
				.objectStore(cols             )
				.delete     (idd              );

				res.onsuccess=x=>run(x.target.result, x);
				res.onerror  =x=>{
					if ( !x.target.result ) run(x.target.result, x);
					else                    err(x.target.result, x);
				};
			 };

			if ( typeof index==VALUE_NUMBER ) fde(index);
			else {
				gthis
				.Get(cols, index, value)
				.run(x=>{
					if ( x.length ) run(x   );
					else            fde(x.id);
				})
				.err(err);
			}
		});
	 };
	gthis.Set    = function( cols, index, value, object ) {
		return new sProm(gthis, (run, err)=>{
			if ( typeof index ==VALUE_NUMBER && tipode(value)==VALUE_OBJECT ) object=value;
			if ( tipode(object)!=VALUE_OBJECT                               ) {
				err('Se requiere de un objeto que editar.');
				return this;
			}

			let isn = typeof index==VALUE_NUMBER;
			let res = co.dbp
			.transaction(cols, 'readwrite')
			.objectStore(cols             )
			.openCursor (                 );

			res.onerror  =x=>err(x.target.result, x);
			res.onsuccess=x=>{
				if ( x.target.result ) {
					if ( (x.target.result.value[isn?'id':index])===(isn?index:value) ) {
						object.id = x.target.result.value.id;

						res = x.target.result.update(object);

						res.onerror  =x=>err(x.target.result, x);
						res.onsuccess=x=>run(x.target.result, x);

						return;
					}

					x.target.result.continue();
				}
			 }
		});
	 };
	gthis.Mov    = function( cols, index, newindex      ) {
		return new sProm(gthis,(run,err)=>{
			gthis
			.Get.call(this, cols)
			.err(err)
			.run(rows=>{
				if ( index>=rows.length ) index = rows.length-1;
				if ( index<0            ) index = 0;

				if ( newindex>rows.length+1 ) newindex = rows.length;
				if ( newindex<0             ) newindex = 0;

				rows.splice(newindex, 0, rows.splice(index,1)[0]);

				gthis
				.Clear.call(this, cols)
				.err(err)
				.run(x=>{
					let ind=0;
					let fes=()=>{
						if ( ++ind>=rows.length )
						run(rows);
					 };

					rows.forEach(z=>{
						delete z.id;

						gthis.Add(cols, z).run(fes);
					});
				});
			});
		});
	 };

	/*Fuente*/
	function CreateCache() {
		co.dbr                =indexedDB.open('BlackOcean.'+CACHE_VERSION, 1);
		co.dbr.onerror        =x=>gthis.tri('onError', 'Error al abrir la cache');
		co.dbr.onsuccess      =x=>{
			co.dbp = x.target.result;

			LoadModules().run(x=>gthis.tri('onOpen', 'La cache se abrió correctamente'));
		 };
		co.dbr.onupgradeneeded=x=>{
			co.dbp = x.target.result;

			let ind = 0;
			let run =()=>{
				if ( ind<CACHE_STRUCT.length ) {
					let sre = co.dbp.createObjectStore(CACHE_STRUCT[ind].name, {keyPath:'id', autoIncrement:true});

					CACHE_STRUCT[ind].cams.sForEach((val,key)=>sre.createIndex(key, key, val));

					ind++;

					run();
				}
			 };

			run();
			gthis.tri('onUpgradeneeded', 'La cache se actualizo correctamente');
		 };
	 }
	function LoadModules() {
		return new sProm(run=>{
			gthis.Get('cache_exec').run(x=>{
				x
				.sort((a,b)=>b.order-a.order)
				.sSome(y=>{
					if ( CACHE_HASH[(y.requestID+'_bj').toLowerCase()]==y.hash )
						Eval({ id:'exec_'+y.requestID, data:y.data, type:VALUE_JS });

					else return true;
				});

				run();
			}).err(run);
		});
	 }

	/*Inicio*/
	this.sSuper = this;
	this.sClass = 'sCache';
	this.exe(props);
 }sCache.prototype = new sNonVisual;
