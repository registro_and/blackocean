[![BlackOcean.js](https://reources-blackkraken.firebaseapp.com/title-readme-md.png)](https://bitbucket.org/jesus-aldrete/blackkraken/src/master/)

BlackKraken es el servicio de distribución masivo de componentes de [Nova Solutions Systems](http://novasolutionsystems.com/).


## Inicio

En este apartado se explica cómo se puede comenzar a trabajar con el servicio de BlackKraken y las herramientas de las que depende.

Para una mayor información, por favor consulta: https://BlackKraken.run/docs

###Requisitos Previos

Este Servicio está desarrollado en Javascript ES6+ con [Node.js](https://nodejs.org/es/) [v8.11.2](https://nodejs.org/dist/v8.11.2/node-v8.11.2.pkg), por lo que se requerirá tener instalada una versión de Node.js igual o superior.

Este servicio depende expresamente del Framework BlackOcean.js, por lo que se requiere instalar tal como se indica [aqui](https://www.npmjs.com/package/black-ocean).

### Instalación/Ejecución 

...


## Autores

* Nova Solutions - Jesús E. Aldréte Hernández<br> <jaldrete@novasolutionsystems.com>


## License

...