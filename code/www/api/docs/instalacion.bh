h1 Introducción

p \
MkDocs includes a couple \
[a(href='http://www.mkdocs.org/user-guide/styling-your-docs/#built-in-themes') built-in themes] \
as well as various third party themes, all of which can easily be customized with extra \
[a(href='http://www.mkdocs.org/user-guide/styling-your-docs/#using-the-docs_dir') CSS or JavaScript] \
or overridden from the \[theme directory\]\[theme_dir\]. You can also create your own \
[a(href='http://www.mkdocs.org/user-guide/custom-themes/') custom theme] \
from the ground up for your documentation.

p To use a theme that is included in MkDocs, simply add this to your [span mkdocs.yml] config file.

code(type='keyv') theme: readthedocs

p \
Replace [span(style='color:var(--COLOR_HIGH);') readthedocs] with any of the \
[a(href='http://www.mkdocs.org/user-guide/styling-your-docs/#built-in-themes') built-in themes] \
listed below.

p \
To create a new custom theme see the \
[a(href='http://www.mkdocs.org/user-guide/custom-themes/') Custom Themes] \
page, or to more heavily customize an existing theme, see the \
[a(href='http://www.mkdocs.org/user-guide/styling-your-docs/#customizing-a-theme') Customizing a Theme] \
section below.

h2 Built-in themes

h3 mkdocs

p \
The default theme, which was built as a custom \
[a(href='http://getbootstrap.com/') Bootstrap] \
theme, supports most every feature of MkDocs. It only supports the default \
[a(href='http://www.mkdocs.org/user-guide/configuration/#theme') theme configuration options].

img(src='http://www.mkdocs.org/img/mkdocs.png' alt=mkdocs)/

h3 readthedocs

p \
A clone of the default theme used by the Read the Docs service. \
This theme only supports features in its parent theme and does not support any MkDocs theme configuration \
options in addition to the defaults.

img(src='http://docs.readthedocs.io/en/latest/_images/screen_mobile.png' alt=ReadTheDocs)/

h3 Third Party Themes

p \
A list of third party themes can be found in the MkDocs \
[a(href='https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes') community wiki]. \
If you have created your own, please feel free to add it to the list.

h2 Customizing a Theme

p \
If you would like to make a few tweaks to an existing theme, there is no need to create your own theme from scratch. \
For minor tweaks which only require some CSS and/or JavaScript, you can use the \
[a(href='http://www.mkdocs.org/user-guide/styling-your-docs/#using-the-docs_dir') docs_dir]. \
However, for more complex customizations, including overriding templates, you will need to use the theme \
[a(href='http://www.mkdocs.org/user-guide/configuration/#custom_dir') custom_dir] \
setting.

h3 Using the docs_dir

p \
The \
[a(href='http://www.mkdocs.org/user-guide/configuration/#extra_css') extra_css] \
and \
[a(href='http://www.mkdocs.org/user-guide/configuration/#extra_javascript') extra_javascript] \
configuration options can be used to make tweaks and customizations to existing themes. \
To use these, you simply need to include either CSS or JavaScript files within your \
[a(href='http://www.mkdocs.org/user-guide/configuration/#docs_dir') documentation directory].

p \
For example, to change the colour of the headers in your documentation, create a file called [span extra.css] and place \
it next to the documentation Markdown. In that file add the following CSS.

code(type='css') \
h1 \{\
  color: red\;\
}

.note
	h5 Note

	p \
	If you are deploying your documentation with \
	[a(href='http://www.mkdocs.org/user-guide/deploying-your-docs/#readthedocs') ReadTheDocs]. \
	You will need to explicitly list the CSS and JavaScript files you want to include in your config. \
	To do this, add the following to your mkdocs.yml.

	code(type='keyv') \
	extra_css: \[extra.css]

p \
After making these changes, they should be visible when you run [span mkdocs serve] - if you already had this running, \
you should see that the CSS changes were automatically picked up and the documentation will be updated.

.note
	h5 Note

	p \
	Any extra CSS or [span JavaScript] files will be added to the generated HTML document after the page content. \
	If you desire to include a JavaScript library, you may have better success including the library by using the theme \
	[a(href='http://www.mkdocs.org/user-guide/configuration/#custom_dir') custom_dir].