function GetMap( permits ) {
	let res=[];
	let rec=ppath=>{
		if ( ppath=='' ) return;

		let sta=api.IsExist(ppath);

		if      ( !sta               );
		else if (  sta.isDirectory() ) api.Travel(ppath).forEach( p=>rec(p.path) );
		else if (  sta.isFile     () ){
			let par = api.GetParsePath(ppath);
			par.ext = par.ext.toLowerCase();

			if ( !/\b((boStart|onOpen|onClose)\.js)$/gi.test(par.base) && /\.js$/gi.test(par.ext) && permits.some(v=>v.toLowerCase()===par.name.toLowerCase()) )
				res.push(par.name);
		}
	 };

	let pat=api.GetParsePath(__dirname);
	pat.base==='sock' && api.IsDirectory(pat.path) && rec(pat.path);

	return res;
 };

module.exports.exec = function( user, pass ) {
	if      ( typeof user!=VALUE_STRING || user=='' ) return { code:400, data:'faltó proporcionar el usuario.'    };
	else if ( typeof pass!=VALUE_STRING || pass=='' ) return { code:400, data:'faltó proporcionar ca contraseña.' };

	return new sProm((run,err)=>{
		setTimeout(x=>{
			mongodb.Get('users', {user})
			.run(v=>{
				if (
					v.length===1 &&
					v[0].user===user &&
					v[0].password===api.SHA256(`[${user}-BO-${pass}]`)
				) {
					mongodb.Get('roles', {id:v[0].roleid})
					.run(vv=>{
						if ( vv.length===1 ) {
							clearTimeout(this.timer);

							this.user=v [0];
							this.role=vv[0];

							run({ code:200, map:GetMap(this.role.permits) });
						}
						else err({ code:401, message:'rol no asignado.' });
					})
					.err(e=>console.error(e) || err({ code:500 }));
				}
				else err({ code:401, message:'error al autenticar las credenciales proporcionadas.' });
			})
			.err( e=>console.error(e) || err({ code:500 }) );
		}, 1000);
	});
 };