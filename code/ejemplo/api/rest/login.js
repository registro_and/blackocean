module.exports.exec=function( headers ) {
	if ( typeof headers.json.u!=VALUE_STRING || headers.json.u=='' ) headers.Response({ code:400, data:'faltó proporcionar el usuario' });
	if ( typeof headers.json.p!=VALUE_STRING || headers.json.p=='' ) headers.Response({ code:400, data:'faltó proporcionar ca contraseña' });
	else {
		setTimeout(x=>{
			mongodb.Get('users', {user:headers.json.u})
			.run(v=>{
				if (
					v.length===1 &&
					v[0].user===headers.json.u &&
					v[0].password===api.SHA256(`[${headers.json.u}-BO-${headers.json.p}]`)
				) {
					mongodb.Get('roles', {id:v[0].roleid})
					.run(vv=>{
						if ( vv.length===1 ) {
							headers.Response({
								code: 202,
								data: JSON.stringify(
									api.EncodeJWT({
										'-date'  : Date(),
										'-roleid': vv[0].id,
										'-userid': v[0].userid,
										rolename : vv[0].title,
										permits  : vv[0].permits,
									})
								)
							});
						}
						else headers.Response({ code:401, data:'Error:401:rol no asignado' });
					})
					.err(e=>console.error(e) || headers.Response({ code:500 }));
				}
				else headers.Response({ code:401 })
			})
			.err( e=>console.error(e) || headers.Response({ code:500 }) );
		}, 500);
	}
 };
